#ifndef _KAZOOIE_H
#define _KAZOOIE_H

#include "ScriptedGossip.h"
#include "Player.h"

typedef QueryResult Kazooie_QueryResult;

enum KazooieBoolean {
	KAZOOIE_BOOL_TRIPLE_SPE,
	KAZOOIE_BOOL_BOTBG_TAG,
	KAZOOIE_BOOL_BOTARENA_TAG,
	KAZOOIE_BOOL_FULL_METIER,
	KAZOOIE_BOOL_MAX,
};
enum KazooieEquilibrage {
	KAZOOIE_EQUILIBRAGE_HEAL,
	KAZOOIE_EQUILIBRAGE_ABSORB,
	KAZOOIE_EQUILIBRAGE_DAMAGE,
};
enum KazooieFloat {
	KAZOOIE_FLOAT_GHOST_SPEED,
	KAZOOIE_FLOAT_RATE_HONOR_KILL,
	KAZOOIE_FLOAT_RATE_HONOR_WIN,
	KAZOOIE_FLOAT_RATE_CORPSE_DELAY,
	KAZOOIE_FLOAT_MAX,
};
enum CauseMarque {
	CAUSE_ERROR,
	CAUSE_TRANSFERT,
	CAUSE_BOUTIQUE,
};
class ClientGhost;

class VendorExtension {
	public:
		uint32 GetEntry() { return m_entry; }
		std::string GetTextFR() { return m_textFR; }
		std::string GetTextUS() { return m_textUS; }
		uint32 GetNpcVendor() { return m_vendorID; }
		void SetEntry(uint32 entry) { m_entry=entry; }
		void SetTextFR(std::string text) { m_textFR=text; }
		void SetTextUS(std::string text) { m_textUS=text; }
		void SetNpcVendor(uint32 entry) { m_vendorID=entry; }	
		void SetCacheClass(uint32 entry) { m_cacheClass=entry; }	
		uint32 GetCacheClass() { return m_cacheClass; }
	private:
		uint32 m_cacheClass;
		uint32 m_entry;
		std::string m_textFR;
		std::string m_textUS;
		uint32 m_vendorID;
};
class Teleportation {
	public:
		void SetMap(uint32 map) { m_map=map;}
		void SetTextUS(std::string text) { m_textUS=text;}
		void SetTextFR(std::string text) { m_textFR=text;}
		void SetX_Horde(float x) { m_x_horde=x;}
		void SetY_Horde(float y) { m_y_horde=y;}
		void SetZ_Horde(float z) { m_z_horde=z;}
		void SetO_Horde(float o) { m_o_horde=o;}
		void SetX_Alliance(float x) { m_x_alliance=x;}
		void SetY_Alliance(float y) { m_y_alliance=y;}
		void SetZ_Alliance(float z) { m_z_alliance=z;}
		void SetO_Alliance(float o) { m_o_alliance=o;}
		void SetCategory(uint32 category) { m_category=category;}
		void SetLevel(uint16 lev) { m_level=lev;}
		void SetAction(uint32 action) { m_action=action;}
	
		uint32 GetMap() { return m_map; }
		std::string GetTextUS() { return m_textUS; }
		std::string GetTextFR() { return m_textFR; }
		float GetX_Horde() { return m_x_horde; }
		float GetY_Horde() { return m_y_horde; }
		float GetZ_Horde() { return m_z_horde; }
		float GetO_Horde() { return m_o_horde; }
		float GetX_Alliance() { return m_x_alliance; }
		float GetY_Alliance() { return m_y_alliance; }
		float GetZ_Alliance() { return m_z_alliance; }
		float GetO_Alliance() { return m_o_alliance; }
		uint32 GetCategory() { return m_category; }
		uint16 GetLevel() { return m_level; }
		uint32 GetAction() { return m_action; }
		
    private:
		uint32 m_map;
		float m_x_horde;
		float m_y_horde;
		float m_z_horde;
		float m_o_horde;
		float m_x_alliance;
		float m_y_alliance;
		float m_z_alliance;
		float m_o_alliance;
		uint32 m_category;
		std::string m_textFR;
		std::string m_textUS;
		uint16 m_level;
		uint32 m_action;
};
class Portal {
	public:
		void SetMap(uint32 map) { m_map=map;}
		void SetX(float x) { m_x=x;}
		void SetY(float y) { m_y=y;}
		void SetZ(float z) { m_z=z;}
		void SetO(float o) { m_o=o;}
		void SetEntry(uint32 entry) { m_entry=entry;}
		void SetType(uint32 type) { m_type=type;}
	
		uint32 GetMap() { return m_map; }
		float GetX() { return m_x; }
		float GetY() { return m_y; }
		float GetZ() { return m_z; }
		float GetO() { return m_o; }
		uint32 GetEntry() { return m_entry; }
		uint8 GetType() { return m_type; }
		
		void Teleport(Player* player) { player->TeleportTo(GetMap(),GetX(),GetY(),GetZ(),GetO()); }
    private:
		uint32 m_map;
		float m_x;
		float m_y;
		float m_z;
		float m_o;
		uint8 m_type;
		uint32 m_entry;
};
class Equilibrage {
	public:
		void SetSpellID(uint32 spellID) { m_spellID=spellID;}
		void SetRatio(float ratio) { m_ratio=ratio;}
		void SetHaste(float ratio) { m_haste=ratio;}
		uint32 GetSpellID() { return m_spellID; }
		float GetRatio() { return m_ratio; }
		float GetHaste() { return m_haste; }
    private:
		uint32 m_spellID;
		float m_ratio;
		float m_haste;
};
class EquilibrageClass {
	public: 
		void SetClassID(uint32 classID) { m_classID=classID;}
		void SetMeleeRatio(float ratio) { m_melee_ratio=ratio;}
		void SetDamageRatio(float ratio) { m_damage_ratio=ratio;}
		void SetHealRatio(float ratio) { m_heal_ratio=ratio;}
		void SetLifeRatio(float ratio) { m_life_ratio=ratio;}
		void SetGlobalLifeRatio(float ratio) { m_global_life_ratio=ratio;}
		void SetManaRatio(float ratio) { m_mana_ratio=ratio;}
		void SetRegenRatio(float ratio) { m_regen_ratio=ratio;}
		void SetResilienceRatio(float ratio) { m_resilience_ratio=ratio;}
		
		uint32 GetClassID() { return m_classID;}
		float GetMeleeRatio() { return m_melee_ratio;}
		float GetDamageRatio() { return m_damage_ratio;}
		float GetHealRatio() { return m_heal_ratio;}
		float GetLifeRatio() { return m_life_ratio;}
		float GetGlobalLifeRatio() { return m_global_life_ratio;}
		float GetManaRatio() { return m_mana_ratio;}
		float GetRegenRatio() { return m_regen_ratio;}
		float GetResilienceRatio() { return m_resilience_ratio;}
		
    private:
		uint32 m_classID;
		float m_melee_ratio;
		float m_damage_ratio;
		float m_heal_ratio;
		float m_life_ratio;
		float m_global_life_ratio;
		float m_mana_ratio;
		float m_regen_ratio;
		float m_resilience_ratio;
};
typedef std::vector <Equilibrage *> EquilibrageMap;
typedef std::vector <EquilibrageClass *> EquilibrageClassMap;
typedef std::map<uint32,Teleportation *> TeleportationMap;
typedef std::vector <Portal *> PortalMap;
class TeleporteurCategorie {
		public:
			TeleporteurCategorie() {}
			TeleporteurCategorie(std::string _nom,uint32 _cat,uint32 _action,TeleportationMap* map_tele);
			~TeleporteurCategorie() {}
			std::string nom;
			uint32 action;
			uint32 categorie;
			std::vector<Teleportation*> teleportations;
};
		
class Kazooie {
	public:
		Kazooie();
		~Kazooie();
	private:
		EquilibrageMap      mEquilibrageMap;
		EquilibrageClassMap mEquilibrageClassMap;
		PortalMap   		mPortalMap;
		uint64 _xp_amount_guild[50];
		std::map<uint32,TeleporteurCategorie> teleporteur_cat;
		TeleportationMap* teleporteur_map;
		bool _bool_config[KAZOOIE_BOOL_MAX];
		float _float_config[KAZOOIE_FLOAT_MAX];
	public:
		/* SpellExtension */
			float Kazooie_GetEquilibrageRatioBySpellID(uint32 spellid);
			float Kazooie_GetEquilibrageHasteBySpellID(uint32 spellid);
			EquilibrageClass* Kazooie_GetEquilibrageClassByClassID(uint32 classid);
			void Kazooie_ManageValue(KazooieEquilibrage e,Unit* caster,Unit* victim,SpellInfo const* spellInfo,uint32& addHealth,bool critical);
			void Kazooie_ManageValue(KazooieEquilibrage e,Unit* caster,Unit* victim,SpellInfo const* spellInfo,int32& addHealth,bool critical);
		/* Teleporteur */
			Portal* Kazooie_GetPortalDestination(uint32 entry);
			std::map<uint32,TeleporteurCategorie> Kazooie_GetTeleporteurCategories() { return teleporteur_cat; }
			std::map <uint32,Teleportation *>* Kazooie_GetTeleportateurMap() { return teleporteur_map; }
			void Kazooie_AjouterTeleportation(uint32 action,Teleportation* t) { teleporteur_map->insert(std::pair<uint32,Teleportation*>(action,t)); }
		/* Loads */
			void Kazooie_LoadTeleporteur();
			void Kazooie_LoadEquilibrage();
			void Kazooie_LoadEquilibrageClass();
			void Kazooie_LoadConfigs();
			void Kazooie_LoadAll();
		/* Config */
			bool GetFloatConfig(uint8 i) { return _float_config[i];  }
			bool GetBoolConfig(uint8 i) { return _bool_config[i]; }
			void SetFloatConfig(uint8 i,float v) { _float_config[i]=v; }
			void SetBoolConfig(uint8 i,bool v) { _bool_config[i]=v; }
};
#define sKazooie ACE_Singleton<Kazooie, ACE_Null_Mutex>::instance()
#endif