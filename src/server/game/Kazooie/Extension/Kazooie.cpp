#include "Kazooie.h"
#include "Config.h"
TeleporteurCategorie::TeleporteurCategorie(std::string _nom,uint32 _cat,uint32 _action ,TeleportationMap* map_tele) {
	nom=_nom;
	categorie=_cat;
	action=_action;
	Kazooie_QueryResult result = WorldDatabase.PQuery("SELECT textFR,textUS,category,map,pos_x_alliance,pos_y_alliance,pos_z_alliance,pos_o_alliance,pos_x_horde,pos_y_horde,pos_z_horde,pos_o_horde,level FROM teleporteur WHERE category='%u' ORDER BY textFR ASC",_cat);
	if (!result) return;
	uint32 count=0;
	do	{
						Field *fields = result->Fetch();
						
						Teleportation* teleportation = new Teleportation();
						
						std::string textFR = fields[0].GetString();
						std::string textUS = fields[1].GetString();
						uint32 category = fields[2].GetUInt32();
						uint32 map = fields[3].GetUInt32();
						float pos_x_alliance = fields[4].GetFloat();
						float pos_y_alliance = fields[5].GetFloat();
						float pos_z_alliance = fields[6].GetFloat();
						float pos_o_alliance = fields[7].GetFloat();
						float pos_x_horde = fields[8].GetFloat();
						float pos_y_horde = fields[9].GetFloat();
						float pos_z_horde = fields[10].GetFloat();
						float pos_o_horde = fields[11].GetFloat();
						uint16 level = fields[12].GetUInt16();
						
						teleportation->SetTextFR(textFR);
						teleportation->SetTextUS(textUS);
						teleportation->SetCategory(category);
						teleportation->SetMap(map);
						teleportation->SetX_Alliance(pos_x_alliance);
						teleportation->SetY_Alliance(pos_y_alliance);
						teleportation->SetZ_Alliance(pos_z_alliance);
						teleportation->SetO_Alliance(pos_o_alliance);
						teleportation->SetX_Horde(pos_x_horde);
						teleportation->SetY_Horde(pos_y_horde);
						teleportation->SetZ_Horde(pos_z_horde);
						teleportation->SetO_Horde(pos_o_horde);
						teleportation->SetLevel(level);
						teleportation->SetAction(10000+categorie*1000+count);
						
						teleportations.push_back(teleportation);
						sKazooie->Kazooie_AjouterTeleportation(teleportation->GetAction(),teleportation);
						++count;
	}
	while (result->NextRow());
}
void Kazooie::Kazooie_LoadTeleporteur() {
    uint32 oldMSTime = getMSTime();
	
	if(!teleporteur_map->empty())
		for (std::map <uint32,Teleportation *>::const_iterator itr = teleporteur_map->begin(); itr != teleporteur_map->end(); ++itr)
			delete itr->second;
	teleporteur_map->clear();
	teleporteur_cat.clear();
	teleporteur_cat.insert(std::pair<uint32,TeleporteurCategorie>(1011,TeleporteurCategorie("|cff510073Zones Shop"					,0,1011,teleporteur_map)));
	teleporteur_cat.insert(std::pair<uint32,TeleporteurCategorie>(1012,TeleporteurCategorie("|cff510073Zones PVP"					,1,1012,teleporteur_map)));
	teleporteur_cat.insert(std::pair<uint32,TeleporteurCategorie>(1021,TeleporteurCategorie("|cff510073Capitales"					,11,1021,teleporteur_map)));
	teleporteur_cat.insert(std::pair<uint32,TeleporteurCategorie>(1013,TeleporteurCategorie("|cff510073Zones Quetes - Niveau 79"	,2,1013,teleporteur_map)));
	teleporteur_cat.insert(std::pair<uint32,TeleporteurCategorie>(1014,TeleporteurCategorie("|cff510073Zones Quetes - Niveau 80"	,3,1014,teleporteur_map)));
	teleporteur_cat.insert(std::pair<uint32,TeleporteurCategorie>(1015,TeleporteurCategorie("|cff510073Zones Quetes - Niveau 81"	,4,1015,teleporteur_map)));
	teleporteur_cat.insert(std::pair<uint32,TeleporteurCategorie>(1016,TeleporteurCategorie("|cff510073Zones Quetes - Niveau 82"	,5,1016,teleporteur_map)));
	teleporteur_cat.insert(std::pair<uint32,TeleporteurCategorie>(1017,TeleporteurCategorie("|cff510073Zones Quetes - Niveau 83"	,6,1017,teleporteur_map)));
	teleporteur_cat.insert(std::pair<uint32,TeleporteurCategorie>(1018,TeleporteurCategorie("|cff510073Zones Quetes - Niveau 84"	,7,1018,teleporteur_map)));
	teleporteur_cat.insert(std::pair<uint32,TeleporteurCategorie>(1019,TeleporteurCategorie("|cff510073Instances T11"				,8,1019,teleporteur_map)));
	teleporteur_cat.insert(std::pair<uint32,TeleporteurCategorie>(1020,TeleporteurCategorie("|cff510073Raids T12"					,9,1020,teleporteur_map)));
	//teleporteur_cat.insert(std::pair<uint32,TeleporteurCategorie>(1021,TeleporteurCategorie("|cff510073Anciennes Instances"			,10,1021,teleporteur_map)));
	
	if(!mPortalMap.empty())
		for (PortalMap::const_iterator itr = mPortalMap.begin(); itr != mPortalMap.end(); ++itr)
			delete *itr;

	mPortalMap.clear();
    Kazooie_QueryResult result2 = WorldDatabase.Query("SELECT entry,map,pos_x,pos_y,pos_z,orientation,type FROM portal ORDER BY map DESC");

    if (!result2)
    {
        TC_LOG_INFO(LOG_FILTER_GENERAL,">> Loaded 0 portal definitions. DB table `teleporteur` is empty!");
        
        return;
    }

    uint32 count = 0;

    do
    {
        Field *fields = result2->Fetch();
		
		Portal* portal = new Portal();
		
		uint32 entry = fields[0].GetUInt32();
		uint32 map = fields[1].GetUInt32();
		float pos_x = fields[2].GetFloat();
		float pos_y = fields[3].GetFloat();
		float pos_z = fields[4].GetFloat();
		float pos_o = fields[5].GetFloat();
		uint8 type = fields[6].GetUInt32();
		
		portal->SetEntry(entry);
		portal->SetMap(map);
		portal->SetX(pos_x);
		portal->SetY(pos_y);
		portal->SetZ(pos_z);
		portal->SetO(pos_o);
		portal->SetType(type);

		mPortalMap.push_back(portal);
		
        ++count;
    }
    while (result2->NextRow());

    TC_LOG_INFO(LOG_FILTER_GENERAL,">> Loaded %u Portal definitions in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
}
void Kazooie::Kazooie_LoadEquilibrage() {

    uint32 oldMSTime = getMSTime();
	if(!mEquilibrageMap.empty())
		for (EquilibrageMap::const_iterator itr = mEquilibrageMap.begin(); itr != mEquilibrageMap.end(); ++itr)
			delete *itr;

	mEquilibrageMap.clear();
    Kazooie_QueryResult result = WorldDatabase.Query("SELECT spellid,ratio,haste FROM equilibrage");

    if (!result)
    {
        TC_LOG_INFO(LOG_FILTER_GENERAL,">> Loaded 0 equilibrage definitions. DB table `equilibrage` is empty!");
        return;
    }

    uint32 count = 0;

    do
    {
        Field *fields = result->Fetch();
		
		Equilibrage* equilibrage = new Equilibrage();
		
        uint32 spellid = fields[0].GetUInt32();
		float ratio = fields[1].GetFloat();
		float haste = fields[2].GetFloat();
		
        equilibrage->SetSpellID(spellid);
        equilibrage->SetRatio(ratio);
        equilibrage->SetHaste(haste);

		mEquilibrageMap.push_back(equilibrage); 
		
        ++count;
    }
    while (result->NextRow());

    TC_LOG_INFO(LOG_FILTER_GENERAL,">> Loaded %u Equilibrages definitions in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
    
}
void Kazooie::Kazooie_LoadEquilibrageClass() {
    uint32 oldMSTime = getMSTime();
	if(!mEquilibrageClassMap.empty())
		for (EquilibrageClassMap::const_iterator itr = mEquilibrageClassMap.begin(); itr != mEquilibrageClassMap.end(); ++itr)
			delete *itr;

	mEquilibrageClassMap.clear();		//				0 	  1		2     3		4	5	  6		7
    Kazooie_QueryResult result = WorldDatabase.Query("SELECT class,melee_blanc,damage,heal,life,life_global,mana,regen,resilience FROM equilibrage_class");

    if (!result)
    {
        TC_LOG_INFO(LOG_FILTER_GENERAL,">> Loaded 0 equilibrage class definitions. DB table `equilibrage class` is empty!");
        
        return;
    }

    uint32 count = 0;

    do
    {
        Field *fields = result->Fetch();
		
		EquilibrageClass* equilibrageclass = new EquilibrageClass();
		
        uint32 classid = fields[0].GetUInt32();
		float melee = fields[1].GetFloat();
		float damage = fields[2].GetFloat();
		float heal = fields[3].GetFloat();
		float life = fields[4].GetFloat();
		float life_global = fields[5].GetFloat();
		float mana = fields[6].GetFloat();
		float regen = fields[7].GetFloat();
		float resilience= fields[8].GetFloat();
		
        equilibrageclass->SetClassID(classid);
        equilibrageclass->SetMeleeRatio(melee);
        equilibrageclass->SetDamageRatio(damage);
        equilibrageclass->SetHealRatio(heal);
        equilibrageclass->SetLifeRatio(life);
        equilibrageclass->SetGlobalLifeRatio(life_global);
        equilibrageclass->SetManaRatio(mana);
        equilibrageclass->SetRegenRatio(regen);
        equilibrageclass->SetResilienceRatio(resilience);

		mEquilibrageClassMap.push_back(equilibrageclass); 
		
        ++count;
    }
    while (result->NextRow());

    TC_LOG_INFO(LOG_FILTER_GENERAL,">> Loaded %u Equilibrages Class definitions in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
    
}
void Kazooie::Kazooie_LoadConfigs() {
    TC_LOG_INFO(LOG_FILTER_SERVER_LOADING, "Loading Kazooie Variables...");
    SetBoolConfig(KAZOOIE_BOOL_TRIPLE_SPE,sConfigMgr->GetBoolDefault("Kazooie.TripleSpe", false));
    SetBoolConfig(KAZOOIE_BOOL_BOTBG_TAG,sConfigMgr->GetBoolDefault("Kazooie.BGTag", true));
    SetBoolConfig(KAZOOIE_BOOL_BOTARENA_TAG,sConfigMgr->GetBoolDefault("Kazooie.ArenaTag", true));
    SetBoolConfig(KAZOOIE_BOOL_FULL_METIER,sConfigMgr->GetBoolDefault("Kazooie.FullMetier", false));
    SetFloatConfig(KAZOOIE_FLOAT_GHOST_SPEED,sConfigMgr->GetFloatDefault("Kazooie.GhostSpeed", 1.0f));
    SetFloatConfig(KAZOOIE_FLOAT_RATE_HONOR_KILL,sConfigMgr->GetFloatDefault("Kazooie.RateHonorKill", 1.0f));
    SetFloatConfig(KAZOOIE_FLOAT_RATE_HONOR_WIN,sConfigMgr->GetFloatDefault("Kazooie.RateHonorWin", 1.0f));
    SetFloatConfig(KAZOOIE_FLOAT_RATE_CORPSE_DELAY,sConfigMgr->GetFloatDefault("Kazooie.CorpseReclaimDelay", 1.0f));
}
void Kazooie::Kazooie_LoadAll() {
    TC_LOG_INFO(LOG_FILTER_SERVER_LOADING, "Loading Teleporteur...");
	Kazooie_LoadTeleporteur();
    TC_LOG_INFO(LOG_FILTER_SERVER_LOADING, "Loading Equilibrage...");
	Kazooie_LoadEquilibrage();
    TC_LOG_INFO(LOG_FILTER_SERVER_LOADING, "Loading Equilibrage Class...");
	Kazooie_LoadEquilibrageClass();
}