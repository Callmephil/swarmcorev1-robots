#ifndef _EXTENSION_H
#define _EXTENSION_H
#include "Player.h"
#include "Unit.h"
#include "ScriptMgr.h"
#include "Cell.h"
#include "CellImpl.h"
#include "GameEventMgr.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "GameObject.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "InstanceScript.h"
#include "CombatAI.h"
#include "PassiveAI.h"
#include "Chat.h"
#include "DBCStructure.h"
#include "DBCStores.h"
#include "ObjectMgr.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"

class Statistiques;
class DuelStat;
class FFAStat;
class ArenaStat;
class BGStat;
class GeneralStat;
class Kazooie_UnitExtension {
	public:
		Kazooie_UnitExtension(Unit* unit);
		~Kazooie_UnitExtension();
		/* Auras */
			uint8 Kazooie_GetStackAmount(uint32 spellid);
			uint8 Kazooie_GetCharges(uint32 spellid);
			void Kazooie_AddCharges(int8 incr,uint32 spellid);
			void Kazooie_AddStack(int8 incr,uint32 spellid);
		/* Status */
			Unit* Kazooie_GetMaster();
		/* SpellExtension */
			void Kazooie_TriggerAbsorb(Unit* victim, SpellInfo const* spellProto, uint32 &absorb);
			void Kazooie_TriggerHeal(Unit* victim, SpellInfo const* spellProto, uint32 &heal);
			void Kazooie_TriggerDamage(Unit* victim, SpellInfo const* spellProto, uint32 &damage);
			void Kazooie_TriggerDispellAura(Aura* aura,uint32 spellId, uint64 casterGUID, Unit* dispeller, uint8 chargesRemoved);
			float Kazooie_GetEquilibrage(uint32 spellid,Unit* victim,bool heal);
			void Kazooie_Interruption(Unit* unitTarget);
			float Kazooie_NegativeAuraModifier(AuraType aura) const;
			void Kazooie_EventMeleeAttack(Unit* victim,uint32 dmg);
			void Kazooie_GestionKill(Unit* victim,SpellInfo const* spellProto);
			void Kazooie_DeclenchementDegatsSpell(SpellNonMeleeDamage* damageInfo,SpellInfo const* spellInfo);
			void Kazooie_CriticSpell(Unit* victim,SpellInfo const* spellInfo);
			uint8 Kazooie_StackedAuraCure(DispelType type);
	private:
		Unit* _unit;
};
class Kazooie_PlayerExtension : public Kazooie_UnitExtension {
	public:
		Kazooie_PlayerExtension(Player* plr);
		~Kazooie_PlayerExtension();
		/* PVP */
			void Kazooie_AjouterMarque(uint32 marque,uint32 count,uint8 cause,bool mail);
			uint32 Kazooie_GetFFAStack();
			uint32 Kazooie_GetFFAScore();
			void Kazooie_AddFFAScore(Player* victim);
			void Kazooie_ResetFFAScore();
			bool Kazooie_IsInFFAZone() { return m_isinffazone; };
			void Kazooie_SetInFFAZone(bool isffa);
			void Kazooie_RepopAtFFAGraveyard();
		/* Database */
			void Kazooie_Load(SQLQueryHolder* result);
			void Kazooie_Save(SQLTransaction& trans);
			uint32 Kazooie_GetVotePoints();
			void Kazooie_DeleteVotePoints(uint32 amount);
		/* Divers */
			void Kazooie_Begin();
			void Kazooie_ManageArea(uint32 old_zone,uint32 new_zone);
	private:
		uint32 m_FFAscore;
		bool m_isinffazone;
		bool m_TriggerStarting;
		uint32 m_timernozari;
		uint8 m_nozariphase;
		Player* _player;
};
#endif