#include "Extension.h"
#include "Kazooie.h"
#include "GhostSpells.h"
#include "Group.h"
#include "QuestDef.h"
#include "Guild.h"
#include "Unit.h"
#include "SpellAuras.h"
#include "Battleground.h"
#include "BattlegroundQueue.h"
#include "BattlegroundWS.h"
#include "BattlegroundEY.h"
#include "Player.h"
#include "Item.h"
#include "World.h"
#include "Spell.h"
Kazooie::Kazooie() {
	teleporteur_map=new TeleportationMap();
}
Kazooie::~Kazooie() {
	delete teleporteur_map;
}
Kazooie_PlayerExtension::Kazooie_PlayerExtension(Player* plr):Kazooie_UnitExtension(plr),_player(plr) {
	m_timernozari=0;
    m_FFAscore=0;
    m_isinffazone=false;
}
Kazooie_PlayerExtension::~Kazooie_PlayerExtension() {

}
void Kazooie_PlayerExtension::Kazooie_Load(SQLQueryHolder* holder) {


}
void Kazooie_PlayerExtension::Kazooie_Save(SQLTransaction& trans) {


}
Kazooie_UnitExtension::Kazooie_UnitExtension(Unit* unit):_unit(unit) {

}
Kazooie_UnitExtension::~Kazooie_UnitExtension() {

}
/* EXTENSION UNIT */
uint8 Kazooie_UnitExtension::Kazooie_StackedAuraCure(DispelType type) {
	/*uint8 stack=0;
    // Create dispel mask by dispel type
        uint32 dispelMask = SpellInfo::GetDispelMask(type);
        // Dispel all existing auras vs current dispel type
        AuraApplicationMap& auras = GetAppliedAuras();
        for (AuraApplicationMap::iterator itr = auras.begin(); itr != auras.end();)
        {
            SpellInfo const* spell = itr->second->GetBase()->GetSpellInfo();
            if (spell->GetDispelMask() & dispelMask)
            {
                // Dispel aura
				stack+=itr->second->GetBase()->GetStackAmount();
                RemoveAura(itr);
            }
            else
                ++itr;
    }
	return stack;*/ return 0;
}

float WorldObject::GetDistance(WorldSafeLocsEntry const* loc)
{
	return sqrt(pow(loc->x-m_positionX,2)+
				pow(loc->y-m_positionY,2)+
				pow(loc->z-m_positionZ,2));
}
GameObject* WorldObject::CreateChest(float x,float y, float z, float o,uint32 lootid)
{
    if (GameObject* chest = SummonGameObject(lootid,x,y,z,o, 0, 0, 0, 0, 0)) {
        chest->SetRespawnTime(chest->GetRespawnDelay());
		return chest;
	}
	return NULL;
}
Unit* Kazooie_UnitExtension::Kazooie_GetMaster() {
	if(TempSummon* sum=_unit->ToTempSummon())
		return sum->GetOwner();
	return NULL;
}
/* PLAYER EXTENSION */
uint32 Kazooie_PlayerExtension::Kazooie_GetFFAScore()
{
	return m_FFAscore;
}
uint32 Kazooie_PlayerExtension::Kazooie_GetFFAStack()
{
	if(m_FFAscore>=25) return 5;
	return floor((float)m_FFAscore/5);
}
void Kazooie_PlayerExtension::Kazooie_AddFFAScore(Player* Victim) {
	if(!Victim) return;
	uint8 group_A_value=1,group_E_value=1;
	
	if(Group* group_A=Victim->GetGroup()) 
		group_A_value+=group_A->GroupSizeInZone(_player->GetZoneId());
	if(Group* group_E=Victim->GetGroup()) 
		group_E_value+=group_E->GroupSizeInZone(_player->GetZoneId());
	
	uint32 score=50*group_E_value/group_A_value;
	m_FFAscore+=score;
	//Kazooie_SendMsg(String("[%uv%u] Your score increase by %u (%u points)",NewEntierMap(group_A_value,group_E_value,score,m_FFAscore)));
	uint8 FFAstade=m_FFAscore/500;
	switch(FFAstade) {
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			if(m_FFAscore>(FFAstade+1)*500) {
				_player->CastSpell(_player,21174,true);
				_player->SetFloatValue(OBJECT_FIELD_SCALE_X,1.0f+0.05f*(float)(1+FFAstade));
			}
		break;
	}
	_player->UpdateMaxHealth();
}
void Kazooie_PlayerExtension::Kazooie_SetInFFAZone(bool isffa) { 
	m_isinffazone=isffa;
}
void Kazooie_PlayerExtension::Kazooie_ResetFFAScore()
{
	if(m_FFAscore>0) {
		_player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1.0f);
		//Kazooie_AjouterMarque(CHAMPION_SEAL,floor((float)m_FFAscore/50),CAUSE_ERROR,false);
	}
	//Kazooie_getFFAStat()->Event(EVENT_FFA_SCORE,m_FFAscore);
	m_FFAscore=0;
	m_isinffazone=false;
	if(_player->HasAura(21174)) 
		_player->RemoveAura(21174);
	_player->UpdatePvPState(true);
	_player->UpdateMaxHealth();
	if(_player->GetGroup()) 
		if(_player->GetGroup()->GetLeaderGUID()==_player->GetGUID())
			_player->GetGroup()->SetGroupFFASpot(0);
}
void Kazooie_PlayerExtension::Kazooie_RepopAtFFAGraveyard()
{
    Kazooie_ResetFFAScore();
    _player->ResurrectPlayer(1.0f);
    _player->SpawnCorpseBones();
    // stop countdown until repop
    if(_player->GetZoneId()==796)
    _player->TeleportTo(189, 168.1f,-270.66f,18.76f,0.88f);
    
    if(_player->GetZoneId()==2100||_player->GetZoneId()==405)
    _player->TeleportTo(1,-1452.7f,2937.2f,94.893f,5.5f);
}
void Kazooie_PlayerExtension::Kazooie_AjouterMarque(uint32 marque,uint32 count,uint8 cause,bool mail) {
	/*if(!mail)
		if(AddItem(marque,count)) return;
	std::string subject;
	std::string body;
	
	switch(cause) {
		case CAUSE_ERROR:
			subject="Recompenses";
			body="Vous avez remportes des marques echangeables mais votre inventaire etait a ce moment vide. Nous vous reversons donc ce qui vous etait due.";
		break;
		case CAUSE_TRANSFERT:
			subject="Transfert";
			 body="Vous avez utilise le transfert de ruban sur ce personnage. L'operation vous aura coute 70 rubans.";
		break;
		case CAUSE_BOUTIQUE:
			subject="Boutique";
			body="Vous avez effectue un achat en boutique, nous vous remercions de votre participation a notre serveur.";
		break;
	}
    CharacterDatabase.BeginTransaction();
	MailDraft draft(subject, body);
	if(marque==9999) {
		draft.AddMoney(count);
	}
	else if(marque>0) {
		if(Item* item = Item::CreateItem(marque,count,NULL))  {
			item->SaveToDB();
			draft.AddItem(item);
		}
	}
	draft.SendMailTo(MailReceiver(this, GUID_LOPART(this->GetGUID())), MailSender(this),MAIL_CHECK_MASK_HAS_BODY, 0);
    CharacterDatabase.CommitTransaction();*/
}

void Kazooie_PlayerExtension::Kazooie_Begin() {
	Kazooie_ResetFFAScore();
}
uint32 Kazooie_PlayerExtension::Kazooie_GetVotePoints() {
	Kazooie_QueryResult result = LoginDatabase.PQuery("SELECT points_vote FROM account WHERE id = '%u'",_player->GetSession()->GetAccountId());
	if(result) {
		Field* fields = result->Fetch();
		return fields[0].GetUInt32();
	}
	return 0;
}
void Kazooie_PlayerExtension::Kazooie_DeleteVotePoints(uint32 amount) {
	LoginDatabase.PExecute("UPDATE account SET points_vote=points_vote-'%u' WHERE id='%u'",amount,_player->GetSession()->GetAccountId());
}
/* Spells */
void SpellMgr::Kazooie_SpellMgr(SpellInfo* spellInfo) {
	if(!spellInfo) return;
	switch(spellInfo->Id) {
		case 87195:
		case 87192:
			spellInfo->Effects[0].TriggerSpell=0;
		break;
		case 19184: // Entrapment, prevent from crash  :: TODO
		case 19387:
			spellInfo->Effects[0].TriggerSpell=0;
		break;
		case 14910:
		case 33371:
			spellInfo->Effects[1].TriggerSpell=0;
		break;
		case 88688:
			spellInfo->StackAmount=2;
		break;
		case 46924: //bladestorm
			spellInfo->MaxAffectedTargets=0;
		break;
	}
}
void SpellMgr::Kazooie_ModAura(SpellInfo* spellInfo) {
}
bool AuraEffect::Kazooie_ApplicationStackAura(AuraApplication const* aurApp, uint8 mode, bool apply) const {
    Unit* caster = GetCaster();
    Unit* target = aurApp->GetTarget();
	if(!target) return true;
	if(!caster) return true;
	
	if(target->IsAlive()) {
		switch(GetId()) {
		
		}
	}
	switch(GetSpellInfo()->SpellFamilyName) {
	
	}
	return true;
}
void AuraEffect::Kazooie_TriggerAuraTick(Unit* target, Unit* caster) const {
	if(!caster) return;

	if(!target) return;
	
}
void Kazooie_UnitExtension::Kazooie_TriggerDispellAura(Aura* aura,uint32 spellId, uint64 casterGUID, Unit* dispeller, uint8 chargesRemoved) {
	if(!aura) return;
	if(!dispeller) return;
}
bool Spell::Kazooie_TriggerSpell_END(Unit* victim) {
	if(!m_caster) return true;
	
	switch(m_spellInfo->SpellFamilyName) {
		case SPELLFAMILY_PRIEST:
			switch(m_spellInfo->Id) {
				/* GESTION DU CHAKRA */
				case CHAKRA:
					if(m_caster->HasAura(81208 /*Chakra: Serenity*/))
						m_caster->RemoveAura(81208);
					else if(m_caster->HasAura(81206 /*Chakra: Sanctuary*/))
						m_caster->RemoveAura(81206);
					else if(m_caster->HasAura(81209 /*Chakra: Chastise*/))
						m_caster->RemoveAura(81209);
				break;
			}
		break;
	}
	
	switch(m_spellInfo->Id) {
	
	}
	if(!victim) return true;
	switch(m_spellInfo->SpellFamilyName) {
		case SPELLFAMILY_PRIEST:
			switch(m_spellInfo->Id) {
				case SOIN_DE_LIEN:
				case SOIN_RAPIDE:
				case SOIN_SUPERIEUR:
				case PRIERE_DE_SOIN:
					if(m_caster->HasAura(INNER_FOCUS))
						m_caster->RemoveAura(INNER_FOCUS);
				break;
			}
			switch(m_spellInfo->Id) {
				case INNER_FOCUS:
					// Strength of soul
					if(m_caster->HasAura(89488)) m_caster->CastSpell(m_caster,96266,true);
					else if(m_caster->HasAura(89489)) m_caster->CastSpell(m_caster,96267,true);
				break;
				case POINTE_MENTALE:
					// Mind Melt
					if(m_caster->HasAura(14910))
						m_caster->CastSpell(m_caster,81292,true);
					else if(m_caster->HasAura(33371))
						m_caster->CastSpell(m_caster,87160,true);
				break;
				case ATTAQUE_MENTALE:
					victim->RemoveAurasDueToSpell(87178);
					m_caster->RemoveAurasDueToSpell(87160);
					m_caster->RemoveAurasDueToSpell(81292);
				break;
				case DISPELL:
					if(m_caster->IsFriendlyTo(victim)) {
						if(m_caster->HasAura(33167)) {
							victim->RemoveAurasDueToSpellByDispel(15090,15090,m_caster->GetGUID(),m_caster,2);
						}
						else victim->RemoveAurasDueToSpellByDispel(15090,15090,m_caster->GetGUID(),m_caster,1);
					}
					else {
						victim->RemoveAurasDueToSpellByDispel(15090,15090,m_caster->GetGUID(),m_caster,1);
					}
				break;
			}
		break;
		case SPELLFAMILY_PALADIN:
			switch(m_spellInfo->Id) {
			
			}
		break;
		case SPELLFAMILY_WARRIOR:
			switch(m_spellInfo->Id) {
			
			}
		break;
		default:
			switch(m_spellInfo->Id) {
			
			}
		break;
	}
	switch (m_spellInfo->Id)                     // better way to check unknown
    {
	
	}
	return true;
}

bool Spell::Kazooie_TriggerSpell_BEGIN(Unit* victim) {
	if(!m_caster) return true;
	
    TC_LOG_ERROR(LOG_FILTER_NETWORKIO, "Spell launched %u",m_spellInfo->Id);
	switch(m_spellInfo->SpellFamilyName) {
		case SPELLFAMILY_PRIEST:
			switch(m_spellInfo->Id) {
				case SOIN_RAPIDE:
				case SOIN:
				case SOIN_DE_LIEN:
				case SOIN_SUPERIEUR:
					if(m_caster->HasAura(CHAKRA)) // Chakra
					{
						m_caster->RemoveAurasDueToSpell(CHAKRA);
						m_caster->CastSpell(m_caster,81208,true);
					}
				break;
				case CHATIMENT:
				case POINTE_MENTALE:
					if(m_caster->HasAura(CHAKRA)) // Chakra
					{
						m_caster->RemoveAurasDueToSpell(CHAKRA);
						m_caster->CastSpell(m_caster,81209,true);
					}
				break;
				case PRIERE_DE_GUERISON:
				case PRIERE_DE_SOIN:
					if(m_caster->HasAura(CHAKRA)) // Chakra
					{
						m_caster->RemoveAurasDueToSpell(CHAKRA);
						m_caster->CastSpell(m_caster,81206,true);
					}
				break;
			}
		break;
	}
	
	switch(m_spellInfo->Id) {
	
	}
	if(!victim) return true;
	switch(m_spellInfo->SpellFamilyName) {
		case SPELLFAMILY_PRIEST:
			switch(m_spellInfo->Id) {
			
			}
		break;
		case SPELLFAMILY_PALADIN:
			switch(m_spellInfo->Id) {
			
			}
		break;
		case SPELLFAMILY_WARRIOR:
			switch(m_spellInfo->Id) {
			
			}
		break;
		default:
			switch(m_spellInfo->Id) {
			
			}
		break;
	}
	switch (m_spellInfo->Id)                     // better way to check unknown
    {
	
	}
	return true;
}
void Kazooie_UnitExtension::Kazooie_CriticSpell(Unit* victim,SpellInfo const* spellInfo) {
	if(!spellInfo) return;
	if(!victim) return;
	switch(spellInfo->SpellFamilyName) {
		case SPELLFAMILY_PRIEST:
			switch(spellInfo->Id) {
				case ATTAQUE_MENTALE:
					// Paralysis
					if(_unit->HasAura(87192))
						_unit->CastSpell(victim,87193,true);
					else if(_unit->HasAura(87195))
						_unit->CastSpell(victim,87194,true);
				break;
			}
		break;
	}
}

void Kazooie_UnitExtension::Kazooie_TriggerAbsorb(Unit* victim, SpellInfo const* spellProto, uint32 &absorb) {

}
void Kazooie_UnitExtension::Kazooie_TriggerHeal(Unit* victim, SpellInfo const* spellProto, uint32 &heal) {
	if(!spellProto) return;
	if(!victim) return;
	
	switch(spellProto->SpellFamilyName) {
		case SPELLFAMILY_PRIEST:
			switch(spellProto->Id) {
				case 101062:
					if(_unit->HasAura(88688)) {
						Kazooie_AddStack(-1,88688);
					}
				break;
			}
		break;
		case SPELLFAMILY_WARRIOR:
			switch(spellProto->Id) {
			
			}
		break;
	
		case SPELLFAMILY_SHAMAN:
			switch(spellProto->Id) {
			
			}
		break;
		case SPELLFAMILY_DRUID:
			switch(spellProto->Id) {
			
			}
		break;
	}
}
void Kazooie_UnitExtension::Kazooie_Interruption(Unit* unitTarget) {
	if(Player* plr=_unit->ToPlayer()) {
		switch(plr->getClass()) {
			case CLASS_WARRIOR:
		
			break;
		}
	}
}
void Kazooie_UnitExtension::Kazooie_TriggerDamage(Unit* victim, SpellInfo const* spellProto,uint32 &damage) {
	if(!victim) return;
	
	if(!spellProto) return;
	switch(spellProto->SpellFamilyName) {
		case SPELLFAMILY_PRIEST:
			switch(spellProto->Id) {
				case POINTE_MENTALE:
					if(Aura* aura=victim->GetAura(MOT_DE_DOULEUR,_unit->GetGUID()))
						aura->Remove();
					if(Aura* aura=victim->GetAura(PESTE_DEVORANTE,_unit->GetGUID()))
						aura->Remove();
					if(Aura* aura=victim->GetAura(TOUCHER_VAMPIRIQUE,_unit->GetGUID()))
						aura->Remove();
				break;
			}
		break;
		case SPELLFAMILY_MAGE:
			switch(spellProto->Id) {
			
			}
		break;
		case SPELLFAMILY_WARLOCK:
			switch(spellProto->Id) {
			
			}
		break;
		case SPELLFAMILY_DRUID:
			switch(spellProto->Id) {
			
			}
		break;
		case SPELLFAMILY_PALADIN:
			switch(spellProto->Id) {
			
			}
		break;
		case SPELLFAMILY_SHAMAN:
			switch(spellProto->Id) {
			
			}
		break;
		case SPELLFAMILY_ROGUE:
			switch(spellProto->Id) {
			
			}
		break;
		case SPELLFAMILY_WARRIOR:
			switch(spellProto->Id) {
			
			}
		break;
		default:
			switch(spellProto->Id) {
			
			}
		break;
	}
	
	switch(spellProto->Id) {
	
	}
}
float Kazooie_UnitExtension::Kazooie_NegativeAuraModifier(AuraType aura) const {
	float ratio=1.0f;
	
	return ratio;
}
uint8 Kazooie_UnitExtension::Kazooie_GetStackAmount(uint32 spellid)
{
    uint8 stack=0;
    for (Unit::AuraMap::const_iterator itr = _unit->m_ownedAuras.begin(); itr != _unit->m_ownedAuras.end() ; ++itr)
    {
		if(itr->second->GetId()==spellid) stack+=itr->second->GetStackAmount();
    }
	return stack;
}
uint8 Kazooie_UnitExtension::Kazooie_GetCharges(uint32 spellid) {
    uint8 stack=0;
    for (Unit::AuraMap::const_iterator itr = _unit->m_ownedAuras.begin(); itr != _unit->m_ownedAuras.end() ; ++itr)
    {
		if(itr->second->GetId()==spellid) stack+=itr->second->GetCharges();
    }
	return stack;
}
void Kazooie_UnitExtension::Kazooie_AddCharges(int8 incr,uint32 spellid) {
    for (Unit::AuraMap::const_iterator itr = _unit->m_ownedAuras.begin(); itr != _unit->m_ownedAuras.end() ; ++itr)
    {
		if(itr->second->GetId()==spellid) {
			itr->second->ModCharges(incr);
			return;
		}
    }
}
void Kazooie_UnitExtension::Kazooie_AddStack(int8 incr,uint32 spellid) {
    for (Unit::AuraMap::const_iterator itr = _unit->m_ownedAuras.begin(); itr != _unit->m_ownedAuras.end() ; ++itr)
    {
		if(itr->second->GetId()==spellid) {
			if(itr->second->GetStackAmount()+incr==0) _unit->RemoveAura(spellid);
			else itr->second->ModStackAmount(incr);
			return;
		}
    }
}
void Kazooie_UnitExtension::Kazooie_EventMeleeAttack(Unit* victim,uint32 dmg) {
	// Gestion combat
	if (Player* plr=_unit->ToPlayer()) {
		switch(plr->getClass()) {
	
		}
	}
}
void Kazooie_UnitExtension::Kazooie_GestionKill(Unit* victim,SpellInfo const* spellProto) {
	if(!spellProto) return;
	if(!victim) return;
}
EquilibrageClass* Kazooie::Kazooie_GetEquilibrageClassByClassID(uint32 classid) {
    for (EquilibrageClassMap::const_iterator itr = mEquilibrageClassMap.begin(); itr != mEquilibrageClassMap.end(); ++itr)
		if ((*itr)->GetClassID() == classid) return (*itr);
    return NULL;
}
float Kazooie::Kazooie_GetEquilibrageRatioBySpellID(uint32 spellid)
{
    for (EquilibrageMap::const_iterator itr = mEquilibrageMap.begin(); itr != mEquilibrageMap.end(); ++itr)
		if ((*itr)->GetSpellID() == spellid) return (*itr)->GetRatio();
    return 1.0f;
}
void Kazooie::Kazooie_ManageValue(KazooieEquilibrage e,Unit* caster,Unit* victim,SpellInfo const* spellInfo,int32& addHealth,bool critical) {
	uint32 v=abs(addHealth);
	Kazooie_ManageValue(e,caster,victim,spellInfo,v,critical);
	addHealth=v;
}
void Kazooie::Kazooie_ManageValue(KazooieEquilibrage e,Unit* caster,Unit* victim,SpellInfo const* spellInfo,uint32& addHealth,bool critical) {
	switch(e) {
		case KAZOOIE_EQUILIBRAGE_HEAL:
			if (Player* plr=caster->ToPlayer()) {
				addHealth*=sKazooie->Kazooie_GetEquilibrageClassByClassID(plr->getClass())->GetHealRatio();
			}
			if(victim) 
				caster->getExtension()->Kazooie_TriggerHeal(victim,spellInfo,addHealth);
		break;
		case KAZOOIE_EQUILIBRAGE_ABSORB:
			if (Player* plr=caster->ToPlayer()) {
				addHealth*=sKazooie->Kazooie_GetEquilibrageClassByClassID(plr->getClass())->GetHealRatio();
			}
			if(victim) {
				caster->getExtension()->Kazooie_TriggerAbsorb(victim,spellInfo,addHealth);
				}
		break;
		case KAZOOIE_EQUILIBRAGE_DAMAGE:
			if(victim) 
				if (Player* plr=victim->ToPlayer()) {
					addHealth*=sKazooie->Kazooie_GetEquilibrageClassByClassID(plr->getClass())->GetResilienceRatio();
				}
			if (Player* plr=caster->ToPlayer()) {
				addHealth*=sKazooie->Kazooie_GetEquilibrageClassByClassID(plr->getClass())->GetDamageRatio();
			}
			if(Unit* master=caster->getExtension()->Kazooie_GetMaster()) {
				if(Player* plr=master->ToPlayer()) {
					Kazooie_ManageValue(KAZOOIE_EQUILIBRAGE_DAMAGE,master,victim,spellInfo,addHealth,critical);
				}
			}
			caster->getExtension()->Kazooie_TriggerDamage(victim,spellInfo,addHealth);
		break;
	}
	addHealth*=sKazooie->Kazooie_GetEquilibrageRatioBySpellID(spellInfo->Id);
}
uint32 Battleground::GetGhostSlotsForTeam(uint32 Team) const
{
	return 0;
}
bool Battleground::HasBGFreeSlots(uint32 Team) const
{
	uint16 count_ally=0;
	uint16 count_enemy=0;
	for (Battleground::BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
	{
		Player *plr = sObjectMgr->GetPlayer(itr->first);
		if(!plr) continue;
		if(plr->GetTeam()==Team) count_ally++;
		else count_enemy++;
	}
	if(count_ally>=GetMaxPlayersPerTeam()||count_ally>count_enemy) return false;
	return true;
}
void Battleground::DisengageGhostFromBG(uint8 count,Team team) {

}
void Battleground::GestionGhostDeconnexion(Player* player_deconnexion)
{

}
bool BattlegroundQueue::EmptyWithoutGhost(Team team) {
    for (QueuedPlayersMap::const_iterator itr = m_QueuedPlayers.begin(); itr != m_QueuedPlayers.end(); ++itr) 
	{
		Player* player = ObjectAccessor::FindPlayer(itr->first);
		if (!player) continue;
		if(player->GetTeam()!=team) continue;
	}
	return true;
}
uint8 BattlegroundQueue::GetRealSize(Team team) {
	uint8 count=0;
    for (QueuedPlayersMap::const_iterator itr = m_QueuedPlayers.begin(); itr != m_QueuedPlayers.end(); ++itr) 
	{
		Player* player = ObjectAccessor::FindPlayer(itr->first);
		if (!player) continue;
		if(player->GetTeam()!=team) continue;
	}
	return count;
}

/* DIVERS */

uint8 Group::GroupSizeInZone(uint32 zoneId) {
	uint8 count=0;
	for (member_citerator citr = m_memberSlots.begin(); citr != m_memberSlots.end(); ++citr)
		if (Player* player = ObjectAccessor::FindPlayer(citr->guid)) if(player->GetZoneId()==zoneId) count++;
	return count;
}
/* Maps */
GameObject* Map::AddObject(uint32 type, uint32 entry, float x, float y, float z, float o, float rotation0, float rotation1, float rotation2, float rotation3, uint32 respawnTime)
{
    GameObject* go = new GameObject;
    if (!go->Create(sObjectMgr->GenerateLowGuid(HIGHGUID_GAMEOBJECT), entry, this,
        PHASEMASK_NORMAL, x, y, z, o, rotation0, rotation1, rotation2, rotation3, respawnTime, GO_STATE_READY))
    {
        delete go;
        return NULL;
    }

    // Add to world, so it can be later looked up from HashMapHolder
    if (!AddToMap(go))
    {
        delete go;
        return NULL;
    }
    return go;
}

void Kazooie_PlayerExtension::Kazooie_ManageArea(uint32 old_zone,uint32 new_zone) {
	if(realmID==1) { // Abysse
	
	}
	if(realmID==2) { // Moonlight
	
	}
	if(realmID==3) { // Pandora
	
	}
}