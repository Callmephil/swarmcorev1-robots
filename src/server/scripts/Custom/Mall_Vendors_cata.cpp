/*
Mall_Vendors By : Philippe
Required Multi-Vendor By rochet2
*/

//====================COLOR DEFINITION====================\/
#define MSG_COLOR_LIGHTRED     "|cffff6060"
#define MSG_COLOR_LIGHTBLUE    "|cff00ccff"
#define MSG_COLOR_ANN_GREEN    "|c1f40af20"
#define MSG_COLOR_RED          "|cffff0000"
#define MSG_COLOR_GOLD         "|cffffcc00"
#define MSG_COLOR_SUBWHITE     "|cffbbbbbb"
#define MSG_COLOR_MAGENTA      "|cffff00ff"
#define MSG_COLOR_YELLOW       "|cffffff00"
#define MSG_COLOR_CYAN         "|cff00ffff"
#define MSG_COLOR_DARKBLUE     "|cff0000ff"

#define MSG_COLOR_GREY         "|cff9d9d9d"
#define MSG_COLOR_WHITE        "|cffffffff"
#define MSG_COLOR_GREEN        "|cff1eff00"
#define MSG_COLOR_BLUE         "|cff0080ff"
#define MSG_COLOR_PURPLE       "|cffb048f8"
#define MSG_COLOR_ORANGE       "|cffff8000"

#define MSG_COLOR_DRUID        "|cffff7d0a"
#define MSG_COLOR_HUNTER       "|cffabd473"
#define MSG_COLOR_MAGE         "|cff69ccf0"
#define MSG_COLOR_PALADIN      "|cfff58cba"
#define MSG_COLOR_PRIEST       "|cffffffff"
#define MSG_COLOR_ROGUE        "|cfffff569"
#define MSG_COLOR_SHAMAN       "|cff0070de"
#define MSG_COLOR_WARLOCK      "|cff9482c9"
#define MSG_COLOR_WARRIOR      "|cffc79c6e"
#define MSG_COLOR_DEATH_KNIGHT "|cffc41f3b"
#define MSG_COLOR_MONK         "|cff00ff96"
//====================ARENA_PERSONAL_RATING_RESTRICTION====================\/
#define MSG_ERROR_ACCES  "!! You need 1500+ personal rating to acces to this menu !!"
#define MSG_ULTIMATE_PVP "(===============Ultimate PvP Repack===============)"
#define MSG_CONSTUL_INFO "Consult information to know how tu grunt your Personal Rating :"
#define MSG_INFO_FINDER  "Information can be found on every npc from The Ultimate PvP Repack"
//====================GM_ALERT====================\/
#define MSG_GM_ALERT  "!!ALERT!! FREE GM-ACCES !!ALERT!!"

#include "Extension.h"
#include "Chat.h"
#include "Creature.h"

enum eIcons
{
	CHAT_ICON               = 0,
	VENDOR_ICON             = 1,
	FLIGHT_ICON             = 2,
	TRAINER_ICON            = 3,
	GEAR_ICON               = 4,
	GEAR_ICON_2             = 5,
	BANK_ICON               = 6,
	CHAT_DOTS_ICON          = 7,
	TABARD_ICON             = 8,
	SWORDS_ICON             = 9,
	GOLD_DOT_ICON           = 10
};

#define GLOBAL_MENU_PVP "PvP Gear"
#define GLOBAL_MENU_PVE "PvE Gear"


//==========MAIN_MENU==========\/
enum Conquest_RATING_REQ
{
	Conquest_RATING_REQ = 1500,
};

//=========OPTION-MENU=========\/
enum  OPTION_MENU
{
	OPTION_MENU_BACK = 1501,
	OPTION_MENU_NEVERMIND = 1502,
};
//=========MISCELLIOUS=========\/
enum  MAIN_MISCELLIOUS_MENU
{
	MAIN_MISCELLIOUS_MENU_NECKS = 1,
	MAIN_MISCELLIOUS_MENU_RINGS = 2,
	MAIN_MISCELLIOUS_MENU_CLOAK = 3,
	MAIN_MISCELLIOUS_MENU_TRINKET = 4,
	MAIN_MISCELLIOUS_MENU_RELIC = 5,
};
//===========SUB_MENU_PVP===========\/
enum  SUB_MENU_PVP
{
	SUB_MENU_PVP_NECKS	 = 10,
	SUB_MENU_PVP_RINGS	 = 11,
	SUB_MENU_PVP_CLOAK	 = 12,
	SUB_MENU_PVP_TRINKET = 13,
	SUB_MENU_PVP_RELIC	 = 14,
};
//===========SUB_MENU_PVE===========\/
enum  SUB_MENU_PVE
{
	SUB_MENU_PVE_NECKS	 = 15,
	SUB_MENU_PVE_RINGS	 = 16,
	SUB_MENU_PVE_CLOAK	 = 17,
	SUB_MENU_PVE_TRINKET = 18,
	SUB_MENU_PVE_RELIC	 = 19,
};
//===========SUB_MENU===========\/
enum  SUB_MISCELLIOUS_MENU_PVP
{
	SUB_MISCELLIOUS_PVP_NECKS_1 = 100, //384
	SUB_MISCELLIOUS_PVP_NECKS_2 = 101, //390
	SUB_MISCELLIOUS_PVP_NECKS_3 = 102, //403
	//================================\/
	SUB_MISCELLIOUS_PVP_RINGS_1 = 103, //384
	SUB_MISCELLIOUS_PVP_RINGS_2 = 104, //390
	SUB_MISCELLIOUS_PVP_RINGS_3 = 105, //403
	//================================\/
	SUB_MISCELLIOUS_PVP_CLOAK_1 = 106, //384
	SUB_MISCELLIOUS_PVP_CLOAK_2 = 107, //390
	SUB_MISCELLIOUS_PVP_CLOAK_3 = 108, //403
	//================================\/
	SUB_MISCELLIOUS_PVP_TRINKET_1 = 109, //384
	SUB_MISCELLIOUS_PVP_TRINKET_2 = 110, //390
	SUB_MISCELLIOUS_PVP_TRINKET_3 = 111, //403
	//================================\/
	SUB_MISCELLIOUS_PVP_RELIC_1 = 112, //384
	SUB_MISCELLIOUS_PVP_RELIC_2 = 113, //390
	SUB_MISCELLIOUS_PVP_RELIC_3 = 114, //403
	//================================\/
};

enum  SUB_MISCELLIOUS_MENU_PVE
{
	SUB_MISCELLIOUS_PVE_NECKS_1 = 115, //384
	SUB_MISCELLIOUS_PVE_NECKS_2 = 116, //391
	SUB_MISCELLIOUS_PVE_NECKS_3 = 117, //403
	//================================\/
	SUB_MISCELLIOUS_PVE_RINGS_1 = 118, //384
	SUB_MISCELLIOUS_PVE_RINGS_2 = 119, //391
	SUB_MISCELLIOUS_PVE_RINGS_3 = 120, //403
	//================================\/
	SUB_MISCELLIOUS_PVE_CLOAK_1 = 121, //384
	SUB_MISCELLIOUS_PVE_CLOAK_2 = 122, //391
	SUB_MISCELLIOUS_PVE_CLOAK_3 = 123, //403
	//================================\/
	SUB_MISCELLIOUS_PVE_TRINKET_1 = 124, //384
	SUB_MISCELLIOUS_PVE_TRINKET_2 = 125, //391
	SUB_MISCELLIOUS_PVE_TRINKET_3 = 126, //403
	//================================\/
	SUB_MISCELLIOUS_PVE_RELIC_1 = 127, //384
	SUB_MISCELLIOUS_PVE_RELIC_2 = 128, //391
	SUB_MISCELLIOUS_PVE_RELIC_3 = 129, //403
	//================================\/
};
//===========NPC_ENTRY===========\/
enum MISCELLIOUS_NPC_ID
{
	MISCELLIOUS_NPC_PVP_RINGS_1   = 400000, //384
	MISCELLIOUS_NPC_PVP_RINGS_2   = 400001, //390
	MISCELLIOUS_NPC_PVP_RINGS_3   = 400002, //403
	//================================\/
	MISCELLIOUS_NPC_PVP_TRINKET_1 = 400053, //384
	MISCELLIOUS_NPC_PVP_TRINKET_2 = 400054, //390
	MISCELLIOUS_NPC_PVP_TRINKET_3 = 400055, //403
	//================================\/
	MISCELLIOUS_NPC_PVP_NECKS_1   = 400008, //384
	MISCELLIOUS_NPC_PVP_NECKS_2   = 400009, //390
	MISCELLIOUS_NPC_PVP_NECKS_3   = 400010, //403
	//================================\/
	MISCELLIOUS_NPC_PVP_CLOAK_1	  = 400016, //384
	MISCELLIOUS_NPC_PVP_CLOAK_2   = 400017, //390
	MISCELLIOUS_NPC_PVP_CLOAK_3   = 400018, //403
	//================================\/
	MISCELLIOUS_NPC_PVP_RELIC_1   = 400065, //384
	MISCELLIOUS_NPC_PVP_RELIC_2   = 400066, //390
	MISCELLIOUS_NPC_PVP_RELIC_3   = 400067, //403
	//================================\/
			//PVE//PVE//PVE//
	//================================\/
	MISCELLIOUS_NPC_PVE_RINGS_1   = 400003, //384
	MISCELLIOUS_NPC_PVE_RINGS_2   = 400004, //390
	MISCELLIOUS_NPC_PVE_RINGS_3   = 400005, //403
	//================================\/
	MISCELLIOUS_NPC_PVE_TRINKET_1 = 400012, //384
	MISCELLIOUS_NPC_PVE_TRINKET_2 = 400013, //390
	MISCELLIOUS_NPC_PVE_TRINKET_3 = 400015, //403
	//================================\/
	MISCELLIOUS_NPC_PVE_NECKS_1   = 400011, //384
	MISCELLIOUS_NPC_PVE_NECKS_2   = 400012, //390
	MISCELLIOUS_NPC_PVE_NECKS_3   = 400013, //403
	//================================\/
	MISCELLIOUS_NPC_PVE_CLOAK_1	  = 400019, //384
	MISCELLIOUS_NPC_PVE_CLOAK_2   = 400020, //390
	MISCELLIOUS_NPC_PVE_CLOAK_3   = 400007, //403
	//================================\/
	MISCELLIOUS_NPC_PVE_RELIC_1   = 400065, //384
	MISCELLIOUS_NPC_PVE_RELIC_2   = 400066, //390
	MISCELLIOUS_NPC_PVE_RELIC_3   = 400067, //403
	//================================\/
};
//=====Miscellious_Vendor_Main_Option======\/
//=========================================\/
#define NECK_MENU "Necks ->"
#define RINGS_MENU "Rings ->"
#define	CLOAK_MENU "Cloaks ->"
#define TRINKET_MENU "Trinkets ->"
#define RELIC_MENU "Relics ->"//DK //Druid  //SHAMAN //Paladin
//=====Miscellious_Vendor_Sub_Option======\/
//========================================\/
//=====NECK_PVP_Option=====\/
#define NECK_PVP_384 "Neck 384+ (Free)"
#define NECK_PVP_390 "Neck 390+ (Honor)"
#define NECK_PVP_403 "Neck 403+ (Conquest)"
//=====RINGS_PVP_Option=====\/
#define RINGS_PVP_384 "Ring 384+ (Free)"
#define RINGS_PVP_390 "Ring 390+ (Honor)"
#define RINGS_PVP_403 "Ring 403+ (Conquest)"
//=====CLOAK_PVP_Option=====\/
#define CLOAK_PVP_384 "Cloak 384+ (Free)"
#define CLOAK_PVP_390 "Cloak 390+ (Honor)"
#define CLOAK_PVP_403 "Cloak 403+ (Conquest)"
//=====TRINKET_PVP_Option=====\/
#define TRINKET_PVP_384 "Trinket 384+ (Free)"
#define TRINKET_PVP_390 "Trinket 390+ (Honor)"
#define TRINKET_PVP_403 "Trinket 403+ (Conquest)"
//=====RELIC_PVP_Option=====\/   //DK //Druid  //SHAMAN //Paladin
#define RELIC_PVP_384 "Relic 384+ (Free)"
#define RELIC_PVP_390 "Relic 390+ (Honor)"
#define RELIC_PVP_403 "Relic 403+ (Conquest)"
//=========================================\/
//=====NECK_PVE_Option=====\/
#define NECK_PVE_384 "Neck Tiers 13 [Raid Finder] (Free)"
#define NECK_PVE_390 "Neck Tiers 12 [Heroic] (Honor)"
#define NECK_PVE_403 "Neck Tiers 13 [Normal] (Conquest)"
//=====RINGS_PVE_Option=====\/
#define RINGS_PVE_384 "Ring Tiers 13 [Raid Finder] (Free)"
#define RINGS_PVE_390 "Ring Tiers 12 [Heroic] (Honor)"
#define RINGS_PVE_403 "Ring Tiers 13 [Normal] (Conquest)"
//=====CLOAK_PVE_Option=====\/
#define CLOAK_PVE_384 "Cloak Tiers 13 [Raid Finder] (Free)"
#define CLOAK_PVE_390 "Cloak Tiers 12 [Heroic] (Honor)"
#define CLOAK_PVE_403 "Cloak Tiers 13 [Normal] (Conquest)"
//=====TRINKET_PVE_Option=====\/
#define TRINKET_PVE_384 "Trinket Tiers 13 [Raid Finder] (Free)"
#define TRINKET_PVE_390 "Trinket Tiers 12 [Heroic] (Honor)"
#define TRINKET_PVE_403 "Trinket Tiers 13 [Normal] (Conquest)"
//=====RELIC_PVE_Option=====\/   //DK //Druid  //SHAMAN //Paladin
#define RELIC_PVE_384 "Relic Tiers 13 [Raid Finder] (Free)"
#define RELIC_PVE_390 "Relic Tiers 12 [Heroic] (Honor)"
#define RELIC_PVE_403 "Relic Tiers 13 [Normal] (Conquest)"
//=========================================\/
#define OPTION_BACK "[Back]"
class Mall_Vendor_Miscellious : public CreatureScript
{
public:
	Mall_Vendor_Miscellious() : CreatureScript("Mall_Vendor_Miscellious") { }

	bool OnGossipHello(Player* player, Creature* creature)
	{
		WorldSession * PGS = player->GetSession();
		{
			player->PrepareQuestMenu(creature->GetGUID());
			player->SendPreparedQuest(creature->GetGUID());
		}
		if (player->getLevel() >= 80)
		{
			player->ADD_GOSSIP_ITEM(TABARD_ICON, NECK_MENU, GOSSIP_SENDER_MAIN, MAIN_MISCELLIOUS_MENU_NECKS);
			player->ADD_GOSSIP_ITEM(TABARD_ICON, RINGS_MENU, GOSSIP_SENDER_MAIN, MAIN_MISCELLIOUS_MENU_RINGS);
			player->ADD_GOSSIP_ITEM(TABARD_ICON, CLOAK_MENU, GOSSIP_SENDER_MAIN, MAIN_MISCELLIOUS_MENU_CLOAK);
			player->ADD_GOSSIP_ITEM(TABARD_ICON, TRINKET_MENU, GOSSIP_SENDER_MAIN, MAIN_MISCELLIOUS_MENU_TRINKET);
				if (PGS->GetSecurity() >= 1)
			{
				player->ADD_GOSSIP_ITEM(SWORDS_ICON, RELIC_MENU, GOSSIP_SENDER_MAIN, MAIN_MISCELLIOUS_MENU_RELIC);
			}
			if (player->getClass() == CLASS_DEATH_KNIGHT && CLASS_DRUID && CLASS_SHAMAN && CLASS_PALADIN)
			{
				player->ADD_GOSSIP_ITEM(SWORDS_ICON, RELIC_MENU, GOSSIP_SENDER_MAIN, MAIN_MISCELLIOUS_MENU_RELIC);
			}
		}
		player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());

		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
	{
		player->PlayerTalkClass->ClearMenus();
		{
			WorldSession * PGS = player->GetSession();
			switch(action)
			{
			case OPTION_MENU_BACK:
				{
				player->PrepareQuestMenu(creature->GetGUID());
				player->SendPreparedQuest(creature->GetGUID());
				}
				if (player->getLevel() >= 80)
				{
			player->ADD_GOSSIP_ITEM(TABARD_ICON, NECK_MENU, GOSSIP_SENDER_MAIN, MAIN_MISCELLIOUS_MENU_NECKS);
			player->ADD_GOSSIP_ITEM(TABARD_ICON, RINGS_MENU, GOSSIP_SENDER_MAIN, MAIN_MISCELLIOUS_MENU_RINGS);
			player->ADD_GOSSIP_ITEM(TABARD_ICON, CLOAK_MENU, GOSSIP_SENDER_MAIN, MAIN_MISCELLIOUS_MENU_CLOAK);
			player->ADD_GOSSIP_ITEM(TABARD_ICON, TRINKET_MENU, GOSSIP_SENDER_MAIN, MAIN_MISCELLIOUS_MENU_TRINKET);
				if (PGS->GetSecurity() >= 1)
			{
				player->ADD_GOSSIP_ITEM(SWORDS_ICON, RELIC_MENU, GOSSIP_SENDER_MAIN, MAIN_MISCELLIOUS_MENU_RELIC);
			}
			if (player->getClass() == CLASS_DEATH_KNIGHT && CLASS_DRUID && CLASS_SHAMAN && CLASS_PALADIN)
			{
				player->ADD_GOSSIP_ITEM(SWORDS_ICON, RELIC_MENU, GOSSIP_SENDER_MAIN, MAIN_MISCELLIOUS_MENU_RELIC);
			}
				}
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;
				//=========================================\/
			case MAIN_MISCELLIOUS_MENU_NECKS:
				player->ADD_GOSSIP_ITEM(CHAT_ICON, OPTION_BACK, GOSSIP_SENDER_MAIN, OPTION_MENU_BACK);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, GLOBAL_MENU_PVP, GOSSIP_SENDER_MAIN, SUB_MENU_PVP_NECKS);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, GLOBAL_MENU_PVE, GOSSIP_SENDER_MAIN, SUB_MENU_PVE_NECKS);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;
			case SUB_MENU_PVP_NECKS:
				player->ADD_GOSSIP_ITEM(CHAT_ICON, OPTION_BACK, GOSSIP_SENDER_MAIN, OPTION_MENU_BACK);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, NECK_PVP_384, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVP_NECKS_1);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, NECK_PVP_390, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVP_NECKS_2);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, NECK_PVP_403, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVP_NECKS_3);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
			break;
			
			case SUB_MENU_PVE_NECKS:
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, NECK_PVE_384, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVE_NECKS_1);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, NECK_PVE_390, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVE_NECKS_2);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, NECK_PVE_403, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVE_NECKS_3);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
			break;
				//=========================================\/
			case MAIN_MISCELLIOUS_MENU_RINGS:
				player->ADD_GOSSIP_ITEM(CHAT_ICON, OPTION_BACK, GOSSIP_SENDER_MAIN, OPTION_MENU_BACK);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, GLOBAL_MENU_PVP, GOSSIP_SENDER_MAIN, SUB_MENU_PVP_RINGS);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, GLOBAL_MENU_PVE, GOSSIP_SENDER_MAIN, SUB_MENU_PVE_RINGS);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
			break;
			case SUB_MENU_PVP_RINGS:
				player->ADD_GOSSIP_ITEM(CHAT_ICON, OPTION_BACK, GOSSIP_SENDER_MAIN, OPTION_MENU_BACK);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, RINGS_PVP_384, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVP_RINGS_1);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, RINGS_PVP_390, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVP_RINGS_2);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, RINGS_PVP_403, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVP_RINGS_3);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;
			case SUB_MENU_PVE_RINGS:
				player->ADD_GOSSIP_ITEM(CHAT_ICON, OPTION_BACK, GOSSIP_SENDER_MAIN, OPTION_MENU_BACK);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, RINGS_PVE_384, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVE_RINGS_1);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, RINGS_PVE_390, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVE_RINGS_2);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, RINGS_PVE_403, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVE_RINGS_3);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;
				//=========================================\/
			case MAIN_MISCELLIOUS_MENU_CLOAK:
				player->ADD_GOSSIP_ITEM(CHAT_ICON, OPTION_BACK, GOSSIP_SENDER_MAIN, OPTION_MENU_BACK);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, GLOBAL_MENU_PVP, GOSSIP_SENDER_MAIN, SUB_MENU_PVP_CLOAK);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, GLOBAL_MENU_PVE, GOSSIP_SENDER_MAIN, SUB_MENU_PVE_CLOAK);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;
			case SUB_MENU_PVP_CLOAK:
				player->ADD_GOSSIP_ITEM(CHAT_ICON, OPTION_BACK, GOSSIP_SENDER_MAIN, OPTION_MENU_BACK);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, CLOAK_PVP_384, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVP_CLOAK_1);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, CLOAK_PVP_390, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVP_CLOAK_2);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, CLOAK_PVP_403, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVP_CLOAK_3);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
			break;
			case SUB_MENU_PVE_CLOAK:
				player->ADD_GOSSIP_ITEM(CHAT_ICON, OPTION_BACK, GOSSIP_SENDER_MAIN, OPTION_MENU_BACK);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, CLOAK_PVE_384, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVE_CLOAK_1);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, CLOAK_PVE_390, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVE_CLOAK_2);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, CLOAK_PVE_403, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVE_CLOAK_3);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
			break;
				//=========================================\/
			case MAIN_MISCELLIOUS_MENU_TRINKET:
				player->ADD_GOSSIP_ITEM(CHAT_ICON, OPTION_BACK, GOSSIP_SENDER_MAIN, OPTION_MENU_BACK);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, GLOBAL_MENU_PVP, GOSSIP_SENDER_MAIN, SUB_MENU_PVP_TRINKET);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, GLOBAL_MENU_PVE, GOSSIP_SENDER_MAIN, SUB_MENU_PVE_TRINKET);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;
			case SUB_MENU_PVP_TRINKET:
				player->ADD_GOSSIP_ITEM(CHAT_ICON, OPTION_BACK, GOSSIP_SENDER_MAIN, OPTION_MENU_BACK);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, TRINKET_PVP_384, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVP_TRINKET_1);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, TRINKET_PVP_390, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVP_TRINKET_2);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, TRINKET_PVP_403, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVP_TRINKET_3);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
			break;
			case SUB_MENU_PVE_TRINKET:
				player->ADD_GOSSIP_ITEM(CHAT_ICON, OPTION_BACK, GOSSIP_SENDER_MAIN, OPTION_MENU_BACK);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, TRINKET_PVE_384, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVE_TRINKET_1);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, TRINKET_PVE_390, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVE_TRINKET_2);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, TRINKET_PVE_403, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVE_TRINKET_3);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
			break;
				//=========================================\/
			case MAIN_MISCELLIOUS_MENU_RELIC:
				player->ADD_GOSSIP_ITEM(CHAT_ICON, OPTION_BACK, GOSSIP_SENDER_MAIN, OPTION_MENU_BACK);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, GLOBAL_MENU_PVP, GOSSIP_SENDER_MAIN, SUB_MENU_PVP_RELIC);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, GLOBAL_MENU_PVE, GOSSIP_SENDER_MAIN, SUB_MENU_PVE_RELIC);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;
			case SUB_MENU_PVP_RELIC:
				player->ADD_GOSSIP_ITEM(CHAT_ICON, OPTION_BACK, GOSSIP_SENDER_MAIN, OPTION_MENU_BACK);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, RELIC_PVP_384, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVP_RELIC_1);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, RELIC_PVP_390, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVP_RELIC_2);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, RELIC_PVP_403, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVP_RELIC_3);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
			break;
			case SUB_MENU_PVE_RELIC:
				player->ADD_GOSSIP_ITEM(CHAT_ICON, OPTION_BACK, GOSSIP_SENDER_MAIN, OPTION_MENU_BACK);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, RELIC_PVE_384, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVE_RELIC_1);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, RELIC_PVE_390, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVE_RELIC_2);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, RELIC_PVE_403, GOSSIP_SENDER_MAIN, SUB_MISCELLIOUS_PVE_RELIC_3);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
			break;
				//=========================================FINAL VENDOR GOSSIP=========================================\/
				//=================NECK====================\/
			case SUB_MISCELLIOUS_PVP_NECKS_1: //384
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVP_NECKS_1);
				break;
			case SUB_MISCELLIOUS_PVP_NECKS_2: //390
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVP_NECKS_2);
				break;
			case SUB_MISCELLIOUS_PVP_NECKS_3: //403
			/*	if(PGS->GetSecurity() >= 1)
				{
				PGS->SendNotification(""MSG_COLOR_RED"" MSG_GM_ALERT);
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVP_NECKS_3);
				}
				else
				if(player->GetArenaPersonalRating(player->GetGUID() >= Conquest_RATING_REQ)) //Will be used soon
				{*/
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVP_NECKS_3);
				/*} 
				else
				{
					PGS->SendNotification(""MSG_COLOR_RED"" MSG_ERROR_ACCES);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_WHITE"" MSG_ULTIMATE_PVP);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_ROGUE"" MSG_CONSTUL_INFO);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_WHITE"" MSG_INFO_FINDER);
				player->CLOSE_GOSSIP_MENU();
				}*/
				break;
				
			case SUB_MISCELLIOUS_PVE_NECKS_1: //384
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVE_NECKS_1);
				break;
			case SUB_MISCELLIOUS_PVE_NECKS_2: //390
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVE_NECKS_2);
				break;
			case SUB_MISCELLIOUS_PVE_NECKS_3: //403
			/*	if(PGS->GetSecurity() >= 1)
				{
				PGS->SendNotification(""MSG_COLOR_RED"" MSG_GM_ALERT);
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVE_NECKS_3);
				}
				else
				if(player->GetArenaPersonalRating(player->GetGUID() >= Conquest_RATING_REQ)) //Will be used soon
				{*/
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVE_NECKS_3);
				/*} 
				else
				{
					PGS->SendNotification(""MSG_COLOR_RED"" MSG_ERROR_ACCES);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_WHITE"" MSG_ULTIMATE_PVP);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_ROGUE"" MSG_CONSTUL_INFO);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_WHITE"" MSG_INFO_FINDER);
				player->CLOSE_GOSSIP_MENU();
				}*/
				break;
				//=================RINGS====================\/
			case SUB_MISCELLIOUS_PVP_RINGS_1: //384
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVP_RINGS_1);
				break;
			case SUB_MISCELLIOUS_PVP_RINGS_2: //390
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVP_RINGS_2);
				break;
			case SUB_MISCELLIOUS_PVP_RINGS_3: //403
				/*if(PGS->GetSecurity() >= 1)
				{
				PGS->SendNotification(""MSG_COLOR_RED"" MSG_GM_ALERT);
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVP_RINGS_3);
				}
				else
				if(player->GetArenaPersonalRating(player->GetGUID() >= Conquest_RATING_REQ)) //Will be used soon
				{*/
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVP_RINGS_3);
			/*	} 
				else
				{
					PGS->SendNotification(""MSG_COLOR_RED"" MSG_ERROR_ACCES);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_WHITE"" MSG_ULTIMATE_PVP);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_ROGUE"" MSG_CONSTUL_INFO);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_WHITE"" MSG_INFO_FINDER);
				player->CLOSE_GOSSIP_MENU();
				}*/
				break;
			case SUB_MISCELLIOUS_PVE_RINGS_1: //384
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVE_RINGS_1);
				break;
			case SUB_MISCELLIOUS_PVE_RINGS_2: //390
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVE_RINGS_2);
				break;
			case SUB_MISCELLIOUS_PVE_RINGS_3: //403
			/*	if(PGS->GetSecurity() >= 1)
				{
				PGS->SendNotification(""MSG_COLOR_RED"" MSG_GM_ALERT);
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVE_RINGS_3);
				}
				else
				if(player->GetArenaPersonalRating(player->GetGUID() >= Conquest_RATING_REQ)) //Will be used soon
				{*/
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVE_RINGS_3);
			/*	} 
				else
				{
					PGS->SendNotification(""MSG_COLOR_RED"" MSG_ERROR_ACCES);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_WHITE"" MSG_ULTIMATE_PVP);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_ROGUE"" MSG_CONSTUL_INFO);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_WHITE"" MSG_INFO_FINDER);
				player->CLOSE_GOSSIP_MENU();
				}*/
				break;
				//=================CLOAKS====================\/
			case SUB_MISCELLIOUS_PVP_CLOAK_1: //384
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVP_CLOAK_1);
				break;
			case SUB_MISCELLIOUS_PVP_CLOAK_2: //390
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVP_CLOAK_2);
				break;
			case SUB_MISCELLIOUS_PVP_CLOAK_3: //403
			/*	if(PGS->GetSecurity() >= 1)
				{
				PGS->SendNotification(""MSG_COLOR_RED"" MSG_GM_ALERT);
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVP_CLOAK_3);
				}
				else
				if(player->GetArenaPersonalRating(player->GetGUID() >= Conquest_RATING_REQ)) //Will be used soon
				{*/
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVP_CLOAK_3);
			/*	} 
				else
				{
					PGS->SendNotification(""MSG_COLOR_RED"" MSG_ERROR_ACCES);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_WHITE"" MSG_ULTIMATE_PVP);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_ROGUE"" MSG_CONSTUL_INFO);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_WHITE"" MSG_INFO_FINDER);
				player->CLOSE_GOSSIP_MENU();
				}*/
				break;
			case SUB_MISCELLIOUS_PVE_CLOAK_1: //384
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVE_CLOAK_1);
				break;
			case SUB_MISCELLIOUS_PVE_CLOAK_2: //390
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVE_CLOAK_2);
				break;
			case SUB_MISCELLIOUS_PVE_CLOAK_3: //403
				/*if(PGS->GetSecurity() >= 1)
				{
				PGS->SendNotification(""MSG_COLOR_RED"" MSG_GM_ALERT);
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVE_CLOAK_3);
				}
				else
				if(player->GetArenaPersonalRating(player->GetGUID() >= Conquest_RATING_REQ)) //Will be used soon
				{*/
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVE_CLOAK_3);
			/*	} 
				else
				{
					PGS->SendNotification(""MSG_COLOR_RED"" MSG_ERROR_ACCES);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_WHITE"" MSG_ULTIMATE_PVP);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_ROGUE"" MSG_CONSTUL_INFO);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_WHITE"" MSG_INFO_FINDER);
				player->CLOSE_GOSSIP_MENU();
				}*/
				break;
				//=================TRINKET====================\/
			case SUB_MISCELLIOUS_PVP_TRINKET_1: //384
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVP_TRINKET_1);
				break;
			case SUB_MISCELLIOUS_PVP_TRINKET_2: //390
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVP_TRINKET_2);
				break;
			case SUB_MISCELLIOUS_PVP_TRINKET_3: //403
				/*if(PGS->GetSecurity() >= 1)
				{
				PGS->SendNotification(""MSG_COLOR_RED"" MSG_GM_ALERT);
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVP_TRINKET_3);
				}
				else
				if(player->GetArenaPersonalRating(player->GetGUID() >= Conquest_RATING_REQ)) //Will be used soon
				{*/
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVP_TRINKET_3);
				/*} 
				else
				{
					PGS->SendNotification(""MSG_COLOR_RED"" MSG_ERROR_ACCES);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_WHITE"" MSG_ULTIMATE_PVP);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_ROGUE"" MSG_CONSTUL_INFO);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_WHITE"" MSG_INFO_FINDER);
				player->CLOSE_GOSSIP_MENU();
				}*/
				break;
			case SUB_MISCELLIOUS_PVE_TRINKET_1: //384
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVE_TRINKET_1);
				break;
			case SUB_MISCELLIOUS_PVE_TRINKET_2: //390
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVE_TRINKET_2);
				break;
			case SUB_MISCELLIOUS_PVE_TRINKET_3: //403
			/*	if(PGS->GetSecurity() >= 1)
				{
				PGS->SendNotification(""MSG_COLOR_RED"" MSG_GM_ALERT);
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVE_TRINKET_3);
				}
				else
				if(player->GetArenaPersonalRating(player->GetGUID() >= Conquest_RATING_REQ)) //Will be used soon
				{*/
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVE_TRINKET_3);
			/*	} 
				else
				{
					PGS->SendNotification(""MSG_COLOR_RED"" MSG_ERROR_ACCES);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_WHITE"" MSG_ULTIMATE_PVP);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_ROGUE"" MSG_CONSTUL_INFO);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_WHITE"" MSG_INFO_FINDER);
				player->CLOSE_GOSSIP_MENU();
				}*/
				break;
				//=================RELIC====================\/
			case SUB_MISCELLIOUS_PVP_RELIC_1: //384
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVP_RELIC_1);
				break;
			case SUB_MISCELLIOUS_PVP_RELIC_2: //390
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVP_RELIC_2);
				break;
			case SUB_MISCELLIOUS_PVP_RELIC_3: //403
			/*	if(PGS->GetSecurity() >= 1)
				{
				PGS->SendNotification(""MSG_COLOR_RED"" MSG_GM_ALERT);
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVP_RELIC_3);
				}
				else
				if(player->GetArenaPersonalRating(player->GetGUID() >= Conquest_RATING_REQ)) //Will be used soon
				{*/
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVP_RELIC_3);
			/*	} 
				else
				{
					PGS->SendNotification(""MSG_COLOR_RED"" MSG_ERROR_ACCES);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_WHITE"" MSG_ULTIMATE_PVP);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_ROGUE"" MSG_CONSTUL_INFO);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_WHITE"" MSG_INFO_FINDER);
				player->CLOSE_GOSSIP_MENU();
				}*/
				break;
			case SUB_MISCELLIOUS_PVE_RELIC_1: //384
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVE_RELIC_1);
				break;
			case SUB_MISCELLIOUS_PVE_RELIC_2: //390
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVE_RELIC_2);
				break;
			case SUB_MISCELLIOUS_PVE_RELIC_3: //403
			/*	if(PGS->GetSecurity() >= 1)
				{
				//PGS->SendNotification(""MSG_COLOR_RED"" MSG_GM_ALERT);
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVE_RELIC_3);
				}
				else
				if(player->GetArenaPersonalRating(player->GetGUID() >= Conquest_RATING_REQ)) //Will be used soon
				{*/
				PGS->SendListInventory(creature->GetGUID(), MISCELLIOUS_NPC_PVP_RELIC_3);
				/*} 
				else
				{
					PGS->SendNotification(""MSG_COLOR_RED"" MSG_ERROR_ACCES);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_WHITE"" MSG_ULTIMATE_PVP);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_ROGUE"" MSG_CONSTUL_INFO);
					ChatHandler(PGS).PSendSysMessage(""MSG_COLOR_WHITE"" MSG_INFO_FINDER);
				player->CLOSE_GOSSIP_MENU();
				}*/
				break;
			}
		}
		return true;
	}
};

//=========ARMOR-SET=========\/
enum  MAIN_ARMOR_SET_MENU
{
	MAIN_ARMOR_SET_MENU_PVP_1 = 1,
	MAIN_ARMOR_SET_MENU_PVP_2 = 2,
	MAIN_ARMOR_SET_MENU_PVP_3 = 3,
	MAIN_ARMOR_SET_MENU_PVE_4 = 4,
	MAIN_ARMOR_SET_MENU_PVE_5 = 5,
	MAIN_ARMOR_SET_MENU_PVE_6 = 6,
};

enum MAIN_SUB_MENU_SET
{
	MAIN_SUB_MENU_SET_PVP = 7,
	MAIN_SUB_MENU_SET_PVE = 8,
};

enum SUB_ARMOR_SET_MENU
{
	//======PVP
	SUB_PVP_SET_1 = 9,
	SUB_PVP_SET_2 = 10,
	SUB_PVP_SET_3 = 11,
	//======PVE
	SUB_PVE_SET_1 = 12,
	SUB_PVE_SET_2 = 13,
	SUB_PVE_SET_3 = 14,
};

enum NPC_SET_VENDOR_ID
{
//===============================\/
	NPC_SET_VENDOR_ID_PVP_1 = 400024, //384
	NPC_SET_VENDOR_ID_PVP_2 = 400025, //390
	NPC_SET_VENDOR_ID_PVP_3 = 400026, //403
//===============================\/
	NPC_SET_VENDOR_ID_PVE_1 = 400021, //384
	NPC_SET_VENDOR_ID_PVE_2 = 400022, //391
	NPC_SET_VENDOR_ID_PVE_3 = 400023, //397
};

#define MENU_PVP_SET_1 "Season 10 (Free)" //384
#define MENU_PVP_SET_2 "Season 11 (Honor)" //390
#define MENU_PVP_SET_3 "Season 11 (Conquest)" //403
//================================================\/
#define MENU_PVE_SET_1 "Tiers 13 (Raid Finder) (Free)" //384
#define MENU_PVE_SET_2 "Tiers 12 (Heroic) (Honor)" //390
#define MENU_PVE_SET_3 "Tiers 13 (Normal) (Conquest)" //403

class Mall_Vendor_Set : public CreatureScript
{
public:
	Mall_Vendor_Set() : CreatureScript("Mall_Vendor_Set") { }

	bool OnGossipHello(Player* player, Creature* creature)
	{
		{
			player->PrepareQuestMenu(creature->GetGUID());
			player->SendPreparedQuest(creature->GetGUID());
		}
		player->ADD_GOSSIP_ITEM(TABARD_ICON, GLOBAL_MENU_PVP, GOSSIP_SENDER_MAIN, MAIN_SUB_MENU_SET_PVP);
		player->ADD_GOSSIP_ITEM(TABARD_ICON, GLOBAL_MENU_PVE, GOSSIP_SENDER_MAIN, MAIN_SUB_MENU_SET_PVE);
		player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
		return true;
	}
	
	bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
	{
		WorldSession * PGS = player->GetSession();

		player->PlayerTalkClass->ClearMenus();
		{
			switch(action)
			{
			case OPTION_MENU_BACK:
			{
			player->PrepareQuestMenu(creature->GetGUID());
			player->SendPreparedQuest(creature->GetGUID());
			}
			player->ADD_GOSSIP_ITEM(TABARD_ICON, GLOBAL_MENU_PVP, GOSSIP_SENDER_MAIN, MAIN_SUB_MENU_SET_PVP);
			player->ADD_GOSSIP_ITEM(TABARD_ICON, GLOBAL_MENU_PVE, GOSSIP_SENDER_MAIN, MAIN_SUB_MENU_SET_PVE);
			player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
			break;
			
			case MAIN_SUB_MENU_SET_PVP:
			player->ADD_GOSSIP_ITEM(CHAT_ICON, OPTION_BACK, GOSSIP_SENDER_MAIN, OPTION_MENU_BACK);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, MENU_PVP_SET_1, GOSSIP_SENDER_MAIN, SUB_PVP_SET_1);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, MENU_PVP_SET_2, GOSSIP_SENDER_MAIN, SUB_PVP_SET_2);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, MENU_PVP_SET_3, GOSSIP_SENDER_MAIN, SUB_PVP_SET_3);
			player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
			break;
			
			case MAIN_SUB_MENU_SET_PVE:
			player->ADD_GOSSIP_ITEM(CHAT_ICON, OPTION_BACK, GOSSIP_SENDER_MAIN, OPTION_MENU_BACK);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, MENU_PVE_SET_1, GOSSIP_SENDER_MAIN, SUB_PVE_SET_1);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, MENU_PVE_SET_2, GOSSIP_SENDER_MAIN, SUB_PVE_SET_2);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, MENU_PVE_SET_3, GOSSIP_SENDER_MAIN, SUB_PVE_SET_3);
			player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
			break;
			//===================PVP-VENDOR===================\/
			case SUB_PVP_SET_1:
				PGS->SendListInventory(creature->GetGUID(), NPC_SET_VENDOR_ID_PVP_1);
			break;
			case SUB_PVP_SET_2:
				PGS->SendListInventory(creature->GetGUID(), NPC_SET_VENDOR_ID_PVP_2);
			break;
			case SUB_PVP_SET_3:
				PGS->SendListInventory(creature->GetGUID(), NPC_SET_VENDOR_ID_PVP_3);
			break;
			//===================PVP-VENDOR===================\/
			case SUB_PVE_SET_1:
				PGS->SendListInventory(creature->GetGUID(), NPC_SET_VENDOR_ID_PVE_1);
			break;
			case SUB_PVE_SET_2:
				PGS->SendListInventory(creature->GetGUID(), NPC_SET_VENDOR_ID_PVE_2);
			break;
			case SUB_PVE_SET_3:
				PGS->SendListInventory(creature->GetGUID(), NPC_SET_VENDOR_ID_PVE_3);
			break;
			}
		}
		return true;
	}
};
#define RARE_GEM "Rare Gems (Free)"
#define EPIC_GEM "Epic Gems (Honor)"

#define RED_GEM "Red"
#define BLUE_GEM "Blue"
#define GREEN_GEM "Green"
#define PURPLE_GEM "Purple"
#define YELLOW_GEM "Yellow"
#define ORANGE_GEM "Orange"
#define PRISMA_GEM "Prismatic"
#define META_GEM "Meta"

enum MAIN_MENU_GEMS
{
	MAIN_MENU_GEMS_RARE = 1,
	MAIN_MENU_GEMS_EPIC = 2,
};

enum SUB_MENU_GEMS
{
	SUB_MENU_GEM_RED_RARE = 3,
	SUB_MENU_GEM_BLUE_RARE = 4,
	SUB_MENU_GEM_GREEN_RARE = 5,
	SUB_MENU_GEM_PURPLE_RARE = 6,
	SUB_MENU_GEM_YELLOW_RARE = 7,
	SUB_MENU_GEM_ORANGE_RARE = 8,
	SUB_MENU_GEM_PRISMA_RARE = 9,
	SUB_MENU_GEM_META_RARE = 10,
	//===========================
	SUB_MENU_GEM_RED_EPIC = 11,
	SUB_MENU_GEM_BLUE_EPIC = 12,
	SUB_MENU_GEM_GREEN_EPIC = 13,
	SUB_MENU_GEM_PURPLE_EPIC = 14,
	SUB_MENU_GEM_YELLOW_EPIC = 15,
	SUB_MENU_GEM_ORANGE_EPIC = 16,
};

enum NPC_VENDOR_GEM_ID
{
	NPC_VENDOR_GEM_ID_1 = 400027, //Red
	NPC_VENDOR_GEM_ID_2 = 400029, //Blue
	NPC_VENDOR_GEM_ID_3 = 400031, //Green
	NPC_VENDOR_GEM_ID_4 = 400032, //Purple
	NPC_VENDOR_GEM_ID_5 = 400028, //Yellow
	NPC_VENDOR_GEM_ID_6 = 400030, //Orange
	NPC_VENDOR_GEM_ID_7 = 400033, //Prismatic
	NPC_VENDOR_GEM_ID_8 = 400034, //Meta
	NPC_VENDOR_GEM_ID_9 = 400035, //Red Epic
	NPC_VENDOR_GEM_ID_10 = 400037, //blue Epic
	NPC_VENDOR_GEM_ID_11 = 400039, //Green Epic
	NPC_VENDOR_GEM_ID_12 = 400040, //Purple Epic
	NPC_VENDOR_GEM_ID_13 = 400036, //Yellow Epic
	NPC_VENDOR_GEM_ID_14 = 400038, //Orange Epic
};

class Mall_Vendor_Gems : public CreatureScript
{
public:
	Mall_Vendor_Gems() : CreatureScript("Mall_Vendor_Gems") { }

	bool OnGossipHello(Player* player, Creature* creature)
	{
		{
			player->PrepareQuestMenu(creature->GetGUID());
			player->SendPreparedQuest(creature->GetGUID());
		}
		player->ADD_GOSSIP_ITEM(GEAR_ICON, RARE_GEM, GOSSIP_SENDER_MAIN, MAIN_MENU_GEMS_RARE);
		player->ADD_GOSSIP_ITEM(GEAR_ICON, EPIC_GEM, GOSSIP_SENDER_MAIN, MAIN_MENU_GEMS_EPIC);
		player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
		return true;
	}
	
	bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
	{
		WorldSession * PGS = player->GetSession();

		player->PlayerTalkClass->ClearMenus();
		{
			switch(action)
			{
			
			case OPTION_MENU_BACK:
			{
			player->PrepareQuestMenu(creature->GetGUID());
			player->SendPreparedQuest(creature->GetGUID());
			}
			player->ADD_GOSSIP_ITEM(GEAR_ICON, RARE_GEM, GOSSIP_SENDER_MAIN, MAIN_MENU_GEMS_RARE);
			player->ADD_GOSSIP_ITEM(GEAR_ICON, EPIC_GEM, GOSSIP_SENDER_MAIN, MAIN_MENU_GEMS_EPIC);
			player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
			break;
			
			case MAIN_MENU_GEMS_RARE:
			player->ADD_GOSSIP_ITEM(CHAT_ICON, OPTION_BACK, GOSSIP_SENDER_MAIN, OPTION_MENU_BACK);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, RED_GEM, GOSSIP_SENDER_MAIN, SUB_MENU_GEM_RED_RARE);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, BLUE_GEM, GOSSIP_SENDER_MAIN, SUB_MENU_GEM_BLUE_RARE);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, GREEN_GEM, GOSSIP_SENDER_MAIN, SUB_MENU_GEM_GREEN_RARE);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, PURPLE_GEM, GOSSIP_SENDER_MAIN, SUB_MENU_GEM_PURPLE_RARE);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, YELLOW_GEM, GOSSIP_SENDER_MAIN, SUB_MENU_GEM_YELLOW_RARE);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, ORANGE_GEM, GOSSIP_SENDER_MAIN, SUB_MENU_GEM_ORANGE_RARE);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, PRISMA_GEM, GOSSIP_SENDER_MAIN, SUB_MENU_GEM_PRISMA_RARE);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, META_GEM, GOSSIP_SENDER_MAIN, SUB_MENU_GEM_META_RARE);
			player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
			break;
			
			case MAIN_MENU_GEMS_EPIC:
			player->ADD_GOSSIP_ITEM(CHAT_ICON, OPTION_BACK, GOSSIP_SENDER_MAIN, OPTION_MENU_BACK);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, RED_GEM, GOSSIP_SENDER_MAIN, SUB_MENU_GEM_RED_EPIC);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, BLUE_GEM, GOSSIP_SENDER_MAIN, SUB_MENU_GEM_BLUE_EPIC);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, GREEN_GEM, GOSSIP_SENDER_MAIN, SUB_MENU_GEM_GREEN_EPIC);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, PURPLE_GEM, GOSSIP_SENDER_MAIN, SUB_MENU_GEM_PURPLE_EPIC);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, YELLOW_GEM, GOSSIP_SENDER_MAIN, SUB_MENU_GEM_YELLOW_EPIC);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, ORANGE_GEM, GOSSIP_SENDER_MAIN, SUB_MENU_GEM_ORANGE_EPIC);
			player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
			break;
			
			case SUB_MENU_GEM_RED_RARE:
				PGS->SendListInventory(creature->GetGUID(), NPC_VENDOR_GEM_ID_1);
			break;
			case SUB_MENU_GEM_BLUE_RARE:
				PGS->SendListInventory(creature->GetGUID(), NPC_VENDOR_GEM_ID_2);
			break;
			case SUB_MENU_GEM_GREEN_RARE:
				PGS->SendListInventory(creature->GetGUID(), NPC_VENDOR_GEM_ID_3);
			break;
			case SUB_MENU_GEM_PURPLE_RARE:
				PGS->SendListInventory(creature->GetGUID(), NPC_VENDOR_GEM_ID_4);
			break;
			case SUB_MENU_GEM_YELLOW_RARE:
				PGS->SendListInventory(creature->GetGUID(), NPC_VENDOR_GEM_ID_5);
			break;
			case SUB_MENU_GEM_ORANGE_RARE:
				PGS->SendListInventory(creature->GetGUID(), NPC_VENDOR_GEM_ID_6);
			break;
			case SUB_MENU_GEM_PRISMA_RARE:
				PGS->SendListInventory(creature->GetGUID(), NPC_VENDOR_GEM_ID_7);
			break;
			case SUB_MENU_GEM_META_RARE:
				PGS->SendListInventory(creature->GetGUID(), NPC_VENDOR_GEM_ID_8);
			break;
			//========================\/
			case SUB_MENU_GEM_RED_EPIC:
				PGS->SendListInventory(creature->GetGUID(), NPC_VENDOR_GEM_ID_9);
			break;
			case SUB_MENU_GEM_BLUE_EPIC:
				PGS->SendListInventory(creature->GetGUID(), NPC_VENDOR_GEM_ID_10);
			break;
			case SUB_MENU_GEM_GREEN_EPIC:
				PGS->SendListInventory(creature->GetGUID(), NPC_VENDOR_GEM_ID_11);
			break;
			case SUB_MENU_GEM_PURPLE_EPIC:
				PGS->SendListInventory(creature->GetGUID(), NPC_VENDOR_GEM_ID_12);
			break;
			case SUB_MENU_GEM_YELLOW_EPIC:
				PGS->SendListInventory(creature->GetGUID(), NPC_VENDOR_GEM_ID_13);
			break;
			case SUB_MENU_GEM_ORANGE_EPIC:
				PGS->SendListInventory(creature->GetGUID(), NPC_VENDOR_GEM_ID_14);
			break;
			}
		}
		return true;
	}
};


enum GLYPH_MENU
{
	GLYPH_MENU = 1,
};

enum GLYPH_NPC_ID
{
	GLYPH_NPC_ID_WARRIOR = 400068,
	GLYPH_NPC_ID_PALADIN = 400069,
	GLYPH_NPC_ID_HUNTER  = 400070,
	GLYPH_NPC_ID_ROGUE   = 400071,
	GLYPH_NPC_ID_PRIEST  = 400072,
	GLYPH_NPC_ID_DK      = 400073,
	GLYPH_NPC_ID_SHAMAN  = 400074,
	GLYPH_NPC_ID_MAGE 	 = 400075,
	GLYPH_NPC_ID_WARLOCK = 400076,
	GLYPH_NPC_ID_DRUID   = 400077,
};

class Mall_Vendor_Glyphs : public CreatureScript
{
public:
	Mall_Vendor_Glyphs() : CreatureScript("Mall_Vendor_Glyphs") { }

	bool OnGossipHello(Player* player, Creature* creature)
	{
		{
			player->PrepareQuestMenu(creature->GetGUID());
			player->SendPreparedQuest(creature->GetGUID());
		}
		
		player->ADD_GOSSIP_ITEM(SWORDS_ICON, ">>", GOSSIP_SENDER_MAIN, GLYPH_MENU);
		player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
		return true;
	}
	
	bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
	{
		WorldSession * PGS = player->GetSession();

		player->PlayerTalkClass->ClearMenus();
		{
			switch(action)
			{
			case GLYPH_MENU:
			if (player->getClass() == CLASS_WARRIOR)
				{
					PGS->SendListInventory(creature->GetGUID(),GLYPH_NPC_ID_WARRIOR);
					return false;
				}
				if (player->getClass() == CLASS_PALADIN)
				{
					PGS->SendListInventory(creature->GetGUID(),GLYPH_NPC_ID_PALADIN);
					return false;
				}
				if (player->getClass()== CLASS_HUNTER)
				{
					PGS->SendListInventory(creature->GetGUID(),GLYPH_NPC_ID_HUNTER);
					return false;
				}
				if (player->getClass() == CLASS_ROGUE)
				{
					PGS->SendListInventory(creature->GetGUID(),GLYPH_NPC_ID_ROGUE);
					return false;
				}
				if (player->getClass() == CLASS_PRIEST)
				{
					PGS->SendListInventory(creature->GetGUID(),GLYPH_NPC_ID_PRIEST);
					return false;
				}
				if (player->getClass() == CLASS_DEATH_KNIGHT)
				{
					PGS->SendListInventory(creature->GetGUID(),GLYPH_NPC_ID_DK);
					return false;
				}
				if (player->getClass() == CLASS_SHAMAN)
				{
					PGS->SendListInventory(creature->GetGUID(),GLYPH_NPC_ID_SHAMAN);
					return false;
				}
				if (player->getClass() == CLASS_MAGE)
				{
					PGS->SendListInventory(creature->GetGUID(),GLYPH_NPC_ID_MAGE);
					return false;
				}
				if (player->getClass() == CLASS_WARLOCK)
				{
					PGS->SendListInventory(creature->GetGUID(),GLYPH_NPC_ID_WARLOCK);
					return false;
				}
				if (player->getClass() == CLASS_DRUID)
				{
					PGS->SendListInventory(creature->GetGUID(),GLYPH_NPC_ID_DRUID);
					return false;
				}
			break;
}
		}
		return true;
	}
};

#define Ground_Mount_1 "Ground Mount (Honor)"
#define Ground_Mount_2 "Ground Mount (Conquest)"
#define Ground_Mount_3 "Ground Mount (Rated)"
#define FLYING_Mount_1 "Flying Mount (Honor)"
#define FLYING_Mount_2 "Flying Mount (Conquest)"
#define FLYING_Mount_3 "Flying Mount (Rated)"

#define Menu_Mount_1	"Ground Mount"
#define Menu_Mount_2	"Flying Mount"

enum MENU_MOUNT
{
	MENU_MOUNT_1 = 1, //Ground
	MENU_MOUNT_2 = 2, //Flying
};

enum SUB_MENU_MOUNT
{
	SUB_MENU_MOUNT_GROUND_1 = 3,
	SUB_MENU_MOUNT_GROUND_2 = 4,
	SUB_MENU_MOUNT_GROUND_3 = 5,
	SUB_MENU_MOUNT_FLYING_1 = 6,
	SUB_MENU_MOUNT_FLYING_2 = 7,
	SUB_MENU_MOUNT_FLYING_3 = 8,
};
//A FAIRE
enum NPC_MOUNT_ID
{
	NPC_MOUNT_ID_1 = 400097,
	NPC_MOUNT_ID_2 = 400098,
	NPC_MOUNT_ID_3 = 400099,
	NPC_MOUNT_ID_4 = 400100,
	NPC_MOUNT_ID_5 = 400101,
	NPC_MOUNT_ID_6 = 400102,
};
class Mall_Vendor_Mount : public CreatureScript
{
public:
	Mall_Vendor_Mount() : CreatureScript("Mall_Vendor_Mount") { }

	bool OnGossipHello(Player* player, Creature* creature)
	{
		{
			player->PrepareQuestMenu(creature->GetGUID());
			player->SendPreparedQuest(creature->GetGUID());
		}
		
		player->ADD_GOSSIP_ITEM(VENDOR_ICON, Menu_Mount_1, GOSSIP_SENDER_MAIN, MENU_MOUNT_1);
		player->ADD_GOSSIP_ITEM(VENDOR_ICON, Menu_Mount_2, GOSSIP_SENDER_MAIN, MENU_MOUNT_2);
		player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
		return true;
	}
	
	bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
	{
		WorldSession * PGS = player->GetSession();

		player->PlayerTalkClass->ClearMenus();
		{
			switch(action)
			{
		case MENU_MOUNT_1:
		player->ADD_GOSSIP_ITEM(VENDOR_ICON, Ground_Mount_1, GOSSIP_SENDER_MAIN, SUB_MENU_MOUNT_GROUND_1);
		player->ADD_GOSSIP_ITEM(VENDOR_ICON, Ground_Mount_2, GOSSIP_SENDER_MAIN, SUB_MENU_MOUNT_GROUND_2);
		player->ADD_GOSSIP_ITEM(VENDOR_ICON, Ground_Mount_3, GOSSIP_SENDER_MAIN, SUB_MENU_MOUNT_GROUND_3);
		player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
			break;
		case MENU_MOUNT_2:
		player->ADD_GOSSIP_ITEM(VENDOR_ICON, FLYING_Mount_1, GOSSIP_SENDER_MAIN, SUB_MENU_MOUNT_FLYING_1);
		player->ADD_GOSSIP_ITEM(VENDOR_ICON, FLYING_Mount_2, GOSSIP_SENDER_MAIN, SUB_MENU_MOUNT_FLYING_2);
		player->ADD_GOSSIP_ITEM(VENDOR_ICON, FLYING_Mount_3, GOSSIP_SENDER_MAIN, SUB_MENU_MOUNT_FLYING_3);
		player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
			break;
			
			case SUB_MENU_MOUNT_GROUND_1:
					PGS->SendListInventory(creature->GetGUID(),NPC_MOUNT_ID_1);
			break;
			case SUB_MENU_MOUNT_GROUND_2:
					PGS->SendListInventory(creature->GetGUID(),NPC_MOUNT_ID_2);
			break;
			case SUB_MENU_MOUNT_GROUND_3:
					PGS->SendListInventory(creature->GetGUID(),NPC_MOUNT_ID_3);
			break;
			case SUB_MENU_MOUNT_FLYING_1:
					PGS->SendListInventory(creature->GetGUID(),NPC_MOUNT_ID_4);
			break;
			case SUB_MENU_MOUNT_FLYING_2:
					PGS->SendListInventory(creature->GetGUID(),NPC_MOUNT_ID_5);
			break;
			case SUB_MENU_MOUNT_FLYING_3:
					PGS->SendListInventory(creature->GetGUID(),NPC_MOUNT_ID_6);
			break;
			
			}
		}
		return true;
	}
};

//=================WEAPON_1_H===================\/
#define DEFINE_WEAPON_ONE_HAND_1  "1 Handed Weapons (Free)"
#define DEFINE_WEAPON_ONE_HAND_2  "1 Handed Weapons (Honor)"
#define DEFINE_WEAPON_ONE_HAND_3  "1 Handed Weapons (Conquest)"


enum SUB_WEAPON_ONE_HAND
{
	//========ONE_HAND============\/
	SUB_MENU_WEAPON_ONE_HAND_1  = 1, //384
	SUB_MENU_WEAPON_ONE_HAND_2  = 2, //390
	SUB_MENU_WEAPON_ONE_HAND_3  = 3, //403
};

enum WEAPON_ONE_HAND_NPC_ID
{
	//========ONE_HAND============\/
	WEAPON_ONE_HAND_NPC_1  = 400042, //384
	WEAPON_ONE_HAND_NPC_2  = 400046, //390
	WEAPON_ONE_HAND_NPC_3  = 400050, //403
};

class Mall_Vendor_Weapons_1H : public CreatureScript
{
public:
	Mall_Vendor_Weapons_1H() : CreatureScript("Mall_Vendor_Weapons_1H") { }

	bool OnGossipHello(Player* player, Creature* creature)
	{
		{
			player->PrepareQuestMenu(creature->GetGUID());
			player->SendPreparedQuest(creature->GetGUID());
		}
		player->ADD_GOSSIP_ITEM(SWORDS_ICON, DEFINE_WEAPON_ONE_HAND_1, GOSSIP_SENDER_MAIN, SUB_MENU_WEAPON_ONE_HAND_1);
		player->ADD_GOSSIP_ITEM(SWORDS_ICON, DEFINE_WEAPON_ONE_HAND_2, GOSSIP_SENDER_MAIN, SUB_MENU_WEAPON_ONE_HAND_2);
		player->ADD_GOSSIP_ITEM(SWORDS_ICON, DEFINE_WEAPON_ONE_HAND_3, GOSSIP_SENDER_MAIN, SUB_MENU_WEAPON_ONE_HAND_3);
		player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
		return true;
	}
bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
	{
		WorldSession * PGS = player->GetSession();

		player->PlayerTalkClass->ClearMenus();
		{
			switch(action)
			{
		case SUB_MENU_WEAPON_ONE_HAND_1:
				PGS->SendListInventory(creature->GetGUID(), WEAPON_ONE_HAND_NPC_1);
		break;
		case SUB_MENU_WEAPON_ONE_HAND_2:
				PGS->SendListInventory(creature->GetGUID(), WEAPON_ONE_HAND_NPC_2);
		break;
		case SUB_MENU_WEAPON_ONE_HAND_3:
				PGS->SendListInventory(creature->GetGUID(), WEAPON_ONE_HAND_NPC_3);
		break;
		
			}
		}
		return true;
	}
};

//=================WEAPON_2_H===================\/
#define MENU_WEAPON_TWO_HAND_1  "2 Handed Weapons (Free)"
#define MENU_WEAPON_TWO_HAND_2  "2 Handed Weapons (Honor)"
#define MENU_WEAPON_TWO_HAND_3  "2 Handed Weapons (Conquest)"

enum SUB_WEAPON_TWO_HAND
{
	//========TWO_HAND============\/
	SUB_MENU_WEAPON_TWO_HAND_1  = 1, //384
	SUB_MENU_WEAPON_TWO_HAND_2  = 2, //390
	SUB_MENU_WEAPON_TWO_HAND_3  = 3, //403
};

enum WEAPON_TWO_HAND_NPC_ID
{
	//========TWO_HAND============\/
	WEAPON_TWO_HAND_NPC_1  = 400041, //384
	WEAPON_TWO_HAND_NPC_2  = 400045, //390
	WEAPON_TWO_HAND_NPC_3  = 400049, //403
};

class Mall_Vendor_Weapons_2H : public CreatureScript
{
public:
	Mall_Vendor_Weapons_2H() : CreatureScript("Mall_Vendor_Weapons_2H") { }

	bool OnGossipHello(Player* player, Creature* creature)
	{
		{
			player->PrepareQuestMenu(creature->GetGUID());
			player->SendPreparedQuest(creature->GetGUID());
		}
		player->ADD_GOSSIP_ITEM(SWORDS_ICON, MENU_WEAPON_TWO_HAND_1, GOSSIP_SENDER_MAIN, SUB_MENU_WEAPON_TWO_HAND_1);
		player->ADD_GOSSIP_ITEM(SWORDS_ICON, MENU_WEAPON_TWO_HAND_2, GOSSIP_SENDER_MAIN, SUB_MENU_WEAPON_TWO_HAND_2);
		player->ADD_GOSSIP_ITEM(SWORDS_ICON, MENU_WEAPON_TWO_HAND_3, GOSSIP_SENDER_MAIN, SUB_MENU_WEAPON_TWO_HAND_3);
		player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
		return true;
	}
bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
	{
		WorldSession * PGS = player->GetSession();

		player->PlayerTalkClass->ClearMenus();
		{
			switch(action)
			{
		case SUB_MENU_WEAPON_TWO_HAND_1:
				PGS->SendListInventory(creature->GetGUID(), WEAPON_TWO_HAND_NPC_1);
		break;
		case SUB_MENU_WEAPON_TWO_HAND_2:
				PGS->SendListInventory(creature->GetGUID(), WEAPON_TWO_HAND_NPC_2);
		break;
		case SUB_MENU_WEAPON_TWO_HAND_3:
				PGS->SendListInventory(creature->GetGUID(), WEAPON_TWO_HAND_NPC_3);
		break;
		
			}
		}
		return true;
	}
};


//==================OFF-HAND===================\/
#define MENU_WEAPON_OFF_HAND_1  "Off-Hand (Free)"
#define MENU_WEAPON_OFF_HAND_2  "Off-Hand (Honor)"
#define MENU_WEAPON_OFF_HAND_3  "Off-Hand (Conquest)"

enum SUB_WEAPON_OFF_HAND
{
	//========OFF_HAND============\/
	SUB_MENU_WEAPON_OFF_HAND_1  = 1, //384
	SUB_MENU_WEAPON_OFF_HAND_2  = 2, //390
	SUB_MENU_WEAPON_OFF_HAND_3  = 3, //403
};

enum WEAPON_OFF_HAND_NPC_ID
{
	//========OFF_HAND============\/
	WEAPON_OFF_HAND_NPC_1  = 400043, //384
	WEAPON_OFF_HAND_NPC_2  = 400047, //390
	WEAPON_OFF_HAND_NPC_3  = 400051, //403
};

class Mall_Vendor_OFF_HAND : public CreatureScript
{
public:
	Mall_Vendor_OFF_HAND() : CreatureScript("Mall_Vendor_OFF_HAND") { }

	bool OnGossipHello(Player* player, Creature* creature)
	{
		{
			player->PrepareQuestMenu(creature->GetGUID());
			player->SendPreparedQuest(creature->GetGUID());
		}
		player->ADD_GOSSIP_ITEM(SWORDS_ICON, MENU_WEAPON_OFF_HAND_1, GOSSIP_SENDER_MAIN, SUB_MENU_WEAPON_OFF_HAND_1);
		player->ADD_GOSSIP_ITEM(SWORDS_ICON, MENU_WEAPON_OFF_HAND_2, GOSSIP_SENDER_MAIN, SUB_MENU_WEAPON_OFF_HAND_2);
		player->ADD_GOSSIP_ITEM(SWORDS_ICON, MENU_WEAPON_OFF_HAND_3, GOSSIP_SENDER_MAIN, SUB_MENU_WEAPON_OFF_HAND_3);
		player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
		return true;
	}
bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
	{
		WorldSession * PGS = player->GetSession();

		player->PlayerTalkClass->ClearMenus();
		{
			switch(action)
			{
		case SUB_MENU_WEAPON_OFF_HAND_1:
				PGS->SendListInventory(creature->GetGUID(), WEAPON_OFF_HAND_NPC_1);
		break;
		case SUB_MENU_WEAPON_OFF_HAND_2:
				PGS->SendListInventory(creature->GetGUID(), WEAPON_OFF_HAND_NPC_2);
		break;
		case SUB_MENU_WEAPON_OFF_HAND_3:
				PGS->SendListInventory(creature->GetGUID(), WEAPON_OFF_HAND_NPC_3);
		break;
		
			}
		}
		return true;
	}
};

//=================RANGED======================\/
#define MENU_WEAPON_RANGED_1 "Ranged (Free)"
#define MENU_WEAPON_RANGED_2 "Ranged (Honor)"
#define MENU_WEAPON_RANGED_3 "Ranged (Conquest)"

enum SUB_WEAPON_RANGED
{
	//========RANGED============\/
	SUB_MENU_WEAPON_RANGED_1  = 1, //384
	SUB_MENU_WEAPON_RANGED_2  = 2, //390
	SUB_MENU_WEAPON_RANGED_3  = 3, //403
};

enum WEAPON_RANGED_NPC_ID
{
	//========RANGED============\/
	WEAPON_RANGED_NPC_1  = 400044, //384
	WEAPON_RANGED_NPC_2  = 400048, //390
	WEAPON_RANGED_NPC_3  = 400052, //403
};

class Mall_Vendor_RANGED : public CreatureScript
{
public:
	Mall_Vendor_RANGED() : CreatureScript("Mall_Vendor_RANGED") { }

	bool OnGossipHello(Player* player, Creature* creature)
	{
		{
			player->PrepareQuestMenu(creature->GetGUID());
			player->SendPreparedQuest(creature->GetGUID());
		}
		player->ADD_GOSSIP_ITEM(SWORDS_ICON, MENU_WEAPON_RANGED_1, GOSSIP_SENDER_MAIN, SUB_MENU_WEAPON_RANGED_1);
		player->ADD_GOSSIP_ITEM(SWORDS_ICON, MENU_WEAPON_RANGED_2, GOSSIP_SENDER_MAIN, SUB_MENU_WEAPON_RANGED_2);
		player->ADD_GOSSIP_ITEM(SWORDS_ICON, MENU_WEAPON_RANGED_3, GOSSIP_SENDER_MAIN, SUB_MENU_WEAPON_RANGED_3);
		player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
		return true;
	}
bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
	{
		WorldSession * PGS = player->GetSession();

		player->PlayerTalkClass->ClearMenus();
		{
			switch(action)
			{
		case SUB_MENU_WEAPON_RANGED_1:
				PGS->SendListInventory(creature->GetGUID(), WEAPON_RANGED_NPC_1);
		break;
		case SUB_MENU_WEAPON_RANGED_2:
				PGS->SendListInventory(creature->GetGUID(), WEAPON_RANGED_NPC_2);
		break;
		case SUB_MENU_WEAPON_RANGED_3:
				PGS->SendListInventory(creature->GetGUID(), WEAPON_RANGED_NPC_3);
		break;
		
			}
		}
		return true;
	}
};

#define OFF_SET_MENU_1 "Waist"
#define OFF_SET_MENU_2 "Wrist"
#define OFF_SET_MENU_3 "Boots"

#define Waist_Menu_1 "Waist (Free)"
#define Waist_Menu_2 "Waist (Honor)"
#define Waist_Menu_3 "Waist (Conquest)"

#define Wrist_Menu_1 "Wrist (Free)"
#define Wrist_Menu_2 "Wrist (Honor)"
#define Wrist_Menu_3 "Wrist (Conquest)"

#define Boots_Menu_1 "Boots (Free)" 
#define Boots_Menu_2 "Boots (Honor)" 
#define Boots_Menu_3 "Boots (Conquest)" 

enum OFF_SET_MAIN
{
	OFF_SET_WAIST = 1,
	OFF_SET_WRIST = 2,
	OFF_SET_BOOTS = 3,
};

enum OFF_SET_SUB
{
	//====================\/
	SUB_OFF_SET_WAIST_1 = 4,
	SUB_OFF_SET_WAIST_2 = 5,
	SUB_OFF_SET_WAIST_3 = 6,
	//====================\/
	SUB_OFF_SET_WRIST_1 = 7,
	SUB_OFF_SET_WRIST_2 = 8,
	SUB_OFF_SET_WRIST_3 = 9,
	//====================\/
	SUB_OFF_SET_BOOTS_1 = 10,
	SUB_OFF_SET_BOOTS_2 = 11,
	SUB_OFF_SET_BOOTS_3 = 12,
};

enum NPC_OFFSET_VENDOR_ID
{
	//====================\/
	NPC_ID_WAIST_1 = 400059,
	NPC_ID_WAIST_2 = 400060,
	NPC_ID_WAIST_3 = 400061,
	//====================\/
	NPC_ID_WRIST_1 = 400062,
	NPC_ID_WRIST_2 = 400063,
	NPC_ID_WRIST_3 = 400064,
	//====================\/
	NPC_ID_BOOTS_1 = 400056,
	NPC_ID_BOOTS_2 = 400057,
	NPC_ID_BOOTS_3 = 400058,
};
class Mall_Vendor_Off_Set : public CreatureScript
{
public:
	Mall_Vendor_Off_Set() : CreatureScript("Mall_Vendor_Off_Set") { }

	bool OnGossipHello(Player* player, Creature* creature)
	{
		{
			player->PrepareQuestMenu(creature->GetGUID());
			player->SendPreparedQuest(creature->GetGUID());
		}
		player->ADD_GOSSIP_ITEM(TABARD_ICON, OFF_SET_MENU_1, GOSSIP_SENDER_MAIN, OFF_SET_WAIST);
		player->ADD_GOSSIP_ITEM(TABARD_ICON, OFF_SET_MENU_2, GOSSIP_SENDER_MAIN, OFF_SET_WRIST);
		player->ADD_GOSSIP_ITEM(TABARD_ICON, OFF_SET_MENU_3, GOSSIP_SENDER_MAIN, OFF_SET_BOOTS);
		player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
		return true;
	}
	
bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
	{
		WorldSession * PGS = player->GetSession();

		player->PlayerTalkClass->ClearMenus();
		{
			switch(action)
			{

		case OPTION_MENU_BACK:
		{
			player->PrepareQuestMenu(creature->GetGUID());
			player->SendPreparedQuest(creature->GetGUID());
		}
		player->ADD_GOSSIP_ITEM(TABARD_ICON, OFF_SET_MENU_1, GOSSIP_SENDER_MAIN, OFF_SET_WAIST);
		player->ADD_GOSSIP_ITEM(TABARD_ICON, OFF_SET_MENU_2, GOSSIP_SENDER_MAIN, OFF_SET_WRIST);
		player->ADD_GOSSIP_ITEM(TABARD_ICON, OFF_SET_MENU_3, GOSSIP_SENDER_MAIN, OFF_SET_BOOTS);
		player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
			break;
		//========================================================================================\/
		case OFF_SET_WAIST:
			player->ADD_GOSSIP_ITEM(CHAT_ICON, OPTION_BACK, GOSSIP_SENDER_MAIN, OPTION_MENU_BACK);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, Waist_Menu_1, GOSSIP_SENDER_MAIN, SUB_OFF_SET_WAIST_1);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, Waist_Menu_2, GOSSIP_SENDER_MAIN, SUB_OFF_SET_WAIST_2);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, Waist_Menu_3, GOSSIP_SENDER_MAIN, SUB_OFF_SET_WAIST_3);
		player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
		break;
		//========================================================================================\/
		case OFF_SET_WRIST:
			player->ADD_GOSSIP_ITEM(CHAT_ICON, OPTION_BACK, GOSSIP_SENDER_MAIN, OPTION_MENU_BACK);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, Wrist_Menu_1, GOSSIP_SENDER_MAIN, SUB_OFF_SET_WRIST_1);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, Wrist_Menu_2, GOSSIP_SENDER_MAIN, SUB_OFF_SET_WRIST_2);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, Wrist_Menu_3, GOSSIP_SENDER_MAIN, SUB_OFF_SET_WRIST_3);
		player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
		break;
		//========================================================================================\/
		case OFF_SET_BOOTS:
			player->ADD_GOSSIP_ITEM(CHAT_ICON, OPTION_BACK, GOSSIP_SENDER_MAIN, OPTION_MENU_BACK);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, Boots_Menu_1, GOSSIP_SENDER_MAIN, SUB_OFF_SET_BOOTS_1);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, Boots_Menu_2, GOSSIP_SENDER_MAIN, SUB_OFF_SET_BOOTS_2);
			player->ADD_GOSSIP_ITEM(VENDOR_ICON, Boots_Menu_3, GOSSIP_SENDER_MAIN, SUB_OFF_SET_BOOTS_3);
		player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
		break;
		//========================================================================================\/
		case SUB_OFF_SET_WAIST_1:
				PGS->SendListInventory(creature->GetGUID(), NPC_ID_WAIST_1);
		break;
		//========================================================================================\/
		case SUB_OFF_SET_WAIST_2:
				PGS->SendListInventory(creature->GetGUID(), NPC_ID_WAIST_2);
		break;
		//========================================================================================\/
		case SUB_OFF_SET_WAIST_3:
				PGS->SendListInventory(creature->GetGUID(), NPC_ID_WAIST_3);
		break;
		//========================================================================================\/
		case SUB_OFF_SET_WRIST_1:
				PGS->SendListInventory(creature->GetGUID(), NPC_ID_WRIST_1);
		break;
		//========================================================================================\/
		case SUB_OFF_SET_WRIST_2:
				PGS->SendListInventory(creature->GetGUID(), NPC_ID_WRIST_2);
		break;
		//========================================================================================\/
		case SUB_OFF_SET_WRIST_3:
				PGS->SendListInventory(creature->GetGUID(), NPC_ID_WRIST_3);
		break;
		//========================================================================================\/
		case SUB_OFF_SET_BOOTS_1:
				PGS->SendListInventory(creature->GetGUID(), NPC_ID_BOOTS_1);
		break;
		//========================================================================================\/
		case SUB_OFF_SET_BOOTS_2:
				PGS->SendListInventory(creature->GetGUID(), NPC_ID_BOOTS_2);
		break;
		//========================================================================================\/
		case SUB_OFF_SET_BOOTS_3:
				PGS->SendListInventory(creature->GetGUID(), NPC_ID_BOOTS_3);
		break;
		//========================================================================================\/
			}
		}
		return true;
	}
};

void AddSC_mall_vendors()
{
	new Mall_Vendor_Miscellious();
	new Mall_Vendor_Weapons_1H();
	new Mall_Vendor_Weapons_2H();
	new Mall_Vendor_OFF_HAND(); 
	new Mall_Vendor_RANGED(); 
	new Mall_Vendor_Off_Set();
	new Mall_Vendor_Set();
	new Mall_Vendor_Gems();
	new Mall_Vendor_Glyphs();
	new Mall_Vendor_Mount();
}