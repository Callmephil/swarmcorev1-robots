#include "GameObject.h"
#include "Battleground.h"
#include "Group.h"
#include "Chat.h"
#include "Extension.h"

enum PortesFFA {
	UpdateFFAID			= 80020,
	DoorFFASortie		= 80021,
	DoorFFAEntree		= 80022,
	BuffFFA				= 80023,
	PorteCouloirID		= 50001,
	PorteCathedraleID 	= 50004,
	PorteAutelID		= 50006,
	PorteCouloirBlocageID		= 50007,
	PorteCathedraleBlocageID 	= 50008,
	PorteAutelBlocageID			= 50009
};
const float FFA_Entree_Monastere[4][4] = {
    {865.96f,1322.21f,18.0f,0.06f}, 
    {985.37f,1454.77f,29.15f,6.26f}, 
    {985.37f,1343.5f,29.15f,6.26f},  
    {1151.0f,1438.45f,30.3f,3.23f}      
};
const float FFA_Entree_Maraudon[4][4] = {
    {-1278.12f,2912.4f,87.84f,5.0f}, 
    {-1185.2f,2875.27f,85.77f,1.85f}, 
    {-1246.4f,3027.06f,88.99f,1.66f},
    {-1246.4f,3027.06f,88.99f,1.66f}
};
const float FFA_Sortie_Monastere[3][4] = {
    {200.99f,-240.60f,19.2f,4.67f},
    {174.89f,-288.77f,19.2f,1.57f}, 
    {154.63f,-261.04f,19.2f,6.28f}
};
const float FFA_Sortie_Maraudon[3][4] = {
    {-1458.1f,2922.58f,94.1f,0.7f},
    {-1420.6f,2915.2f,93.82f,1.52f}, 
    {-1395.62f,2922.95f,93.93f,2.86f}
};

class UpdateFFAZone : public CreatureScript
{
    public:
    UpdateFFAZone() : CreatureScript("UpdateFFAZone") {}
	
    CreatureAI* GetAI(Creature *pCreature) const
    {
        return new UpdateFFAZoneAI (pCreature);
    }
    struct UpdateFFAZoneAI : public ScriptedAI
    {
        UpdateFFAZoneAI(Creature* pCreature) : ScriptedAI(pCreature) {}

		GameObject *PorteCouloir;
		uint32 timer_PorteCouloir;
		GameObject *PorteCathedrale;
		uint32 timer_PorteCathedrale;
		GameObject *PorteAutel;
		uint32 timer_PorteAutel;
		uint32 spawn_timer;
		uint32 timer_PorteBlocage;
		bool FFAactivate;
		//bool blocage;
		void Reset()
		{
			me->SetVisible(false);
			spawn_timer=5000;
			timer_PorteBlocage=0;
			FFAactivate=false;
			//blocage=false;
		}


		void UpdateAI(const uint32 uiDiff)
		{
			/*if(timer_PorteBlocage<uiDiff&&blocage) {
				if(PorteCouloir) 
					me->SummonGameObject(PorteCouloirBlocageID, PorteCouloir->GetPositionX(), PorteCouloir->GetPositionY(), PorteCouloir->GetPositionZ(), PorteCouloir->GetOrientation(), 0.0f, 0.0f, 0.0f, 0.0f, 0);
				if(PorteCathedrale) 
					me->SummonGameObject(PorteCathedraleBlocageID, PorteCathedrale->GetPositionX(), PorteCathedrale->GetPositionY(), PorteCathedrale->GetPositionZ(), PorteCathedrale->GetOrientation(), 0.0f, 0.0f, 0.0f, 0.0f, 0);
				if(PorteAutel) 
					me->SummonGameObject(PorteAutelBlocageID, PorteAutel->GetPositionX(), PorteAutel->GetPositionY(), PorteAutel->GetPositionZ(), PorteAutel->GetOrientation(), 0.0f, 0.0f, 0.0f, 0.0f, 0);
				blocage=false;
			}
			else timer_PorteBlocage-=uiDiff;*/
			if(spawn_timer<uiDiff&&!FFAactivate) {
				timer_PorteCouloir=0;
				timer_PorteCathedrale=0;
				timer_PorteAutel=0;
				PorteCouloir = me->FindNearestGameObject(PorteCouloirID, 20.0f);
				PorteCathedrale = me->FindNearestGameObject(PorteCathedraleID, 20.0f);
				PorteAutel = me->FindNearestGameObject(PorteAutelID, 20.0f);
				if(PorteCouloir) PorteCouloir->SetGoState(GO_STATE_ACTIVE_ALTERNATIVE);
				if(PorteCathedrale) PorteCathedrale->SetGoState(GO_STATE_ACTIVE_ALTERNATIVE);
				if(PorteAutel) PorteAutel->SetGoState(GO_STATE_ACTIVE_ALTERNATIVE);
				FFAactivate=true;
			}
			else spawn_timer-=uiDiff;
			
			if(FFAactivate) {
				if(timer_PorteCouloir<uiDiff) {
					if(PorteCouloir) PorteCouloir->SetGoState(GO_STATE_ACTIVE_ALTERNATIVE);
					if(GameObject* gob=me->FindNearestGameObject(PorteCouloirBlocageID, 20.0f))
						gob->Delete();
				}
				else timer_PorteCouloir-=uiDiff;
				
				if(timer_PorteCathedrale<uiDiff) {
					if(PorteCathedrale) PorteCathedrale->SetGoState(GO_STATE_ACTIVE_ALTERNATIVE);
					if(GameObject* gob=me->FindNearestGameObject(PorteCathedraleBlocageID, 20.0f))
						gob->Delete();
				}
				else timer_PorteCathedrale-=uiDiff;
				
				if(timer_PorteAutel<uiDiff) {
					if(PorteAutel) PorteAutel->SetGoState(GO_STATE_ACTIVE_ALTERNATIVE);
					if(GameObject* gob=me->FindNearestGameObject(PorteAutelBlocageID, 20.0f))
						gob->Delete();
				}
				else timer_PorteAutel-=uiDiff;
			}
		}

		void SetData(uint32 type,uint32 data)
		{
			if(FFAactivate) {
				switch(type)
				{
					case 0:
						timer_PorteCouloir=20000;
						if(PorteCouloir) {
							PorteCouloir->SetGoState(GO_STATE_READY);
							/*timer_PorteBlocage=2000;
							blocage=true;*/
	
						}
					break;
					case 1:
						timer_PorteCathedrale=20000;
						if(PorteCathedrale) {
							PorteCathedrale->SetGoState(GO_STATE_READY);
							/*timer_PorteBlocage=2000;
							blocage=true;*/
						}
					break;
					case 2:
						timer_PorteAutel=20000;
						if(PorteAutel) {
							PorteAutel->SetGoState(GO_STATE_READY);
							/*timer_PorteBlocage=2000;
							blocage=true;*/
						}
					break;
				}
			}
		}
    };
};
enum ffa_map {
	FFA_MONASTERE=0,
	FFA_MARAUDON=0
};
class DoorFFA : public CreatureScript
{
    public:
    DoorFFA() : CreatureScript("DoorFFA") {}
	
    CreatureAI* GetAI(Creature *pCreature) const
    {
        return new DoorFFAAI (pCreature);
    }
    struct DoorFFAAI : public ScriptedAI
    {
        DoorFFAAI(Creature* pCreature) : ScriptedAI(pCreature) {}

		uint32 timer;
		
		void Reset()
		{
			me->SetVisible(false);
			timer=500;
		}
		void EntreePlayer(Player *pPlayer)
		{
			if(!pPlayer->getExtension()->Kazooie_IsInFFAZone()) {
				if(pPlayer->GetZoneId()==2100||pPlayer->GetZoneId()==405) if(pPlayer->GetGroup()) {
					pPlayer->GetGroup()->RemoveMember(pPlayer->GetGUID());
					ChatHandler chH = ChatHandler(pPlayer->GetSession());
					std::string msg;
					msg="Vous avez quitt� votre groupe, il vous est interdit de grouper dans cette zone FFA.";
					chH.SendSysMessage(msg.c_str());
				}
				if(pPlayer->GetGroup()) {
					if(pPlayer->GetGroup()->GetGroupFFASpot()>0) {
						pPlayer->getExtension()->Kazooie_SetInFFAZone(true);
						pPlayer->UpdatePvPState(true);
						if(pPlayer->GetZoneId()==2100||pPlayer->GetZoneId()==405)
							pPlayer->TeleportTo(1,FFA_Sortie_Maraudon[pPlayer->GetGroup()->GetGroupFFASpot()-1][0],FFA_Sortie_Maraudon[pPlayer->GetGroup()->GetGroupFFASpot()-1][1],FFA_Sortie_Maraudon[pPlayer->GetGroup()->GetGroupFFASpot()-1][2],FFA_Sortie_Maraudon[pPlayer->GetGroup()->GetGroupFFASpot()-1][3]);
						else if(pPlayer->GetZoneId()==796)
							pPlayer->TeleportTo(189,FFA_Sortie_Monastere[pPlayer->GetGroup()->GetGroupFFASpot()-1][0],FFA_Sortie_Monastere[pPlayer->GetGroup()->GetGroupFFASpot()-1][1],FFA_Sortie_Monastere[pPlayer->GetGroup()->GetGroupFFASpot()-1][2],FFA_Sortie_Monastere[pPlayer->GetGroup()->GetGroupFFASpot()-1][3]);
					}
					else {
						pPlayer->getExtension()->Kazooie_SetInFFAZone(true);
						uint8 random=0;
						if(pPlayer->GetZoneId()==2100||pPlayer->GetZoneId()==405) {
							random=urand(0,2);
							pPlayer->TeleportTo(1,FFA_Entree_Maraudon[random][0],FFA_Entree_Maraudon[random][1],FFA_Entree_Maraudon[random][2],FFA_Entree_Maraudon[random][3]);
						}
						else if(pPlayer->GetZoneId()==796) {
							random=urand(0,3);
							pPlayer->TeleportTo(189,FFA_Entree_Monastere[random][0],FFA_Entree_Monastere[random][1],FFA_Entree_Monastere[random][2],FFA_Entree_Monastere[random][3]);
						}
						pPlayer->UpdatePvPState(true);
							if(pPlayer->GetGroup()->GetLeaderGUID()==pPlayer->GetGUID())
								pPlayer->GetGroup()->SetGroupFFASpot(random+1);
					}
				}
				else {
					pPlayer->getExtension()->Kazooie_SetInFFAZone(true);
					pPlayer->UpdatePvPState(true);
					if(pPlayer->GetZoneId()==2100||pPlayer->GetZoneId()==405) {
						uint8 random=urand(0,2);
						pPlayer->TeleportTo(1,FFA_Entree_Maraudon[random][0],FFA_Entree_Maraudon[random][1],FFA_Entree_Maraudon[random][2],FFA_Entree_Maraudon[random][3]);
					}
					else if(pPlayer->GetZoneId()==796) {
						uint8 random=urand(0,3);
						pPlayer->TeleportTo(189,FFA_Entree_Monastere[random][0],FFA_Entree_Monastere[random][1],FFA_Entree_Monastere[random][2],FFA_Entree_Monastere[random][3]);
					}
				}
			}
		}
		void SortiePlayer(Player *pPlayer)
		{
			if(pPlayer->IsInCombat()) return;
			if(pPlayer->getExtension()->Kazooie_IsInFFAZone()) {
				pPlayer->getExtension()->Kazooie_SetInFFAZone(false);
				pPlayer->UpdatePvPState(true);
				pPlayer->getExtension()->Kazooie_ResetFFAScore();
				if(pPlayer->GetZoneId()==2100||pPlayer->GetZoneId()==405) {
					uint8 random=urand(0,2);
					pPlayer->TeleportTo(1,FFA_Sortie_Maraudon[random][0],FFA_Sortie_Maraudon[random][1],FFA_Sortie_Maraudon[random][2],FFA_Sortie_Maraudon[random][3]);
				}
				else if(pPlayer->GetZoneId()==796) {
					uint8 random=urand(0,2);
					pPlayer->TeleportTo(189,FFA_Sortie_Monastere[random][0],FFA_Sortie_Monastere[random][1],FFA_Sortie_Monastere[random][2],FFA_Sortie_Monastere[random][3]);
				}
			}
		}
		void UpdateAI(const uint32 uiDiff)
		{
			if(timer<uiDiff)
			{
				Map::PlayerList const &PlList = me->GetMap()->GetPlayers();
				for (Map::PlayerList::const_iterator i = PlList.begin(); i != PlList.end(); ++i)
				{
					Player* pPlayer = i->GetSource();
					if(me->GetDistance2d(pPlayer)>4.0f) continue;
						if(me->GetEntry()==DoorFFASortie) {
							if(me->GetOrientation()<3.14f/4||me->GetOrientation()>3.14f*7/4) if(pPlayer->GetPositionX()<me->GetPositionX()) {
								SortiePlayer(pPlayer);
								return;
							}
							if(me->GetOrientation()>3.14f/4&&me->GetOrientation()<3.14f*3/4) if(pPlayer->GetPositionY()<me->GetPositionY()) {
								SortiePlayer(pPlayer);
								return;
							}
							if(me->GetOrientation()>3.14f*3/4&&me->GetOrientation()<3.14f*5/4) if(pPlayer->GetPositionX()>me->GetPositionX()) {
								SortiePlayer(pPlayer);
								return;
							}
							if(me->GetOrientation()>3.14f*5/4&&me->GetOrientation()<3.14f*7/4) if(pPlayer->GetPositionY()>me->GetPositionY()) {
								SortiePlayer(pPlayer);
								return;
							}
						}
						else if(me->GetEntry()==DoorFFAEntree) {
							if(me->GetOrientation()<3.14f/4||me->GetOrientation()>3.14f*7/4) if(pPlayer->GetPositionX()<me->GetPositionX()) {
								EntreePlayer(pPlayer);
								return;
							}
							if(me->GetOrientation()>3.14f/4&&me->GetOrientation()<3.14f*3/4) if(pPlayer->GetPositionY()<me->GetPositionY()) {
								EntreePlayer(pPlayer);
								return;
							}
							if(me->GetOrientation()>3.14f*3/4&&me->GetOrientation()<3.14f*5/4) if(pPlayer->GetPositionX()>me->GetPositionX()) {
								EntreePlayer(pPlayer);
								return;
							}
							if(me->GetOrientation()>3.14f*5/4&&me->GetOrientation()<3.14f*7/4) if(pPlayer->GetPositionY()>me->GetPositionY()) {
								EntreePlayer(pPlayer);
								return;
							}
						}
					
				}
				timer=250;
			}
			else timer-=uiDiff;
		}
    };
};

class PortalFFA : public CreatureScript
{
    public:
    PortalFFA() : CreatureScript("PortalFFA") {}
	
    CreatureAI* GetAI(Creature *pCreature) const
    {
        return new PortalFFAAI (pCreature);
    }
    struct PortalFFAAI : public ScriptedAI
    {
        PortalFFAAI(Creature* pCreature) : ScriptedAI(pCreature) {}

		uint32 timer;
		
		void Reset()
		{
			me->SetVisible(false);
			timer=500;
		}
		void TeleportPlayer(Player *pPlayer)
		{
			if(pPlayer->IsGameMaster()) return;
			if(pPlayer->IsInCombat()) return;
			if(me->GetEntry()==80025)
				pPlayer->TeleportTo(1,-1251.76f,2892.9f,87.56f,2.1f);
			else if(me->GetEntry()==80026)
				pPlayer->TeleportTo(1,-1301.67f,2992.66f,73.58f,5.88f);
			else if(me->GetEntry()==80027)
				pPlayer->TeleportTo(1,-1250.0f,2958.1f,64.22f,1.75f);
			else if(me->GetEntry()==80028)
				pPlayer->TeleportTo(1,-1226.4f,3042.83f,89.3f,4.53f);
					
		
			if(!pPlayer->getExtension()->Kazooie_IsInFFAZone()) {
				if(pPlayer->GetZoneId()==2100||pPlayer->GetZoneId()==405) if(pPlayer->GetGroup()) {
					pPlayer->GetGroup()->RemoveMember(pPlayer->GetGUID());
					ChatHandler chH = ChatHandler(pPlayer->GetSession());
					std::string msg;
					msg="Vous avez quitt� votre groupe, il vous est interdit de grouper dans cette zone FFA.";
					chH.SendSysMessage(msg.c_str());
				}
				if(pPlayer->GetGroup()) {
					if(pPlayer->GetGroup()->GetGroupFFASpot()>0) {
						pPlayer->getExtension()->Kazooie_SetInFFAZone(true);
						pPlayer->UpdatePvPState(true);
						if(pPlayer->GetZoneId()==2100||pPlayer->GetZoneId()==405)
							pPlayer->TeleportTo(1,FFA_Sortie_Maraudon[pPlayer->GetGroup()->GetGroupFFASpot()-1][0],FFA_Sortie_Maraudon[pPlayer->GetGroup()->GetGroupFFASpot()-1][1],FFA_Sortie_Maraudon[pPlayer->GetGroup()->GetGroupFFASpot()-1][2],FFA_Sortie_Maraudon[pPlayer->GetGroup()->GetGroupFFASpot()-1][3]);
						else if(pPlayer->GetZoneId()==796)
							pPlayer->TeleportTo(189,FFA_Sortie_Monastere[pPlayer->GetGroup()->GetGroupFFASpot()-1][0],FFA_Sortie_Monastere[pPlayer->GetGroup()->GetGroupFFASpot()-1][1],FFA_Sortie_Monastere[pPlayer->GetGroup()->GetGroupFFASpot()-1][2],FFA_Sortie_Monastere[pPlayer->GetGroup()->GetGroupFFASpot()-1][3]);
					}
					else {
						pPlayer->getExtension()->Kazooie_SetInFFAZone(true);
						uint8 random=0;
						if(pPlayer->GetZoneId()==2100||pPlayer->GetZoneId()==405) {
							random=urand(0,2);
							pPlayer->TeleportTo(1,FFA_Entree_Maraudon[random][0],FFA_Entree_Maraudon[random][1],FFA_Entree_Maraudon[random][2],FFA_Entree_Maraudon[random][3]);
						}
						else if(pPlayer->GetZoneId()==796) {
							random=urand(0,3);
							pPlayer->TeleportTo(189,FFA_Entree_Monastere[random][0],FFA_Entree_Monastere[random][1],FFA_Entree_Monastere[random][2],FFA_Entree_Monastere[random][3]);
						}
						pPlayer->UpdatePvPState(true);
							if(pPlayer->GetGroup()->GetLeaderGUID()==pPlayer->GetGUID())
								pPlayer->GetGroup()->SetGroupFFASpot(random+1);
					}
				}
				else {
					pPlayer->getExtension()->Kazooie_SetInFFAZone(true);
					pPlayer->UpdatePvPState(true);
					if(pPlayer->GetZoneId()==2100||pPlayer->GetZoneId()==405) {
						uint8 random=urand(0,2);
						pPlayer->TeleportTo(1,FFA_Entree_Maraudon[random][0],FFA_Entree_Maraudon[random][1],FFA_Entree_Maraudon[random][2],FFA_Entree_Maraudon[random][3]);
					}
					else if(pPlayer->GetZoneId()==796||pPlayer->GetZoneId()==405) {
						uint8 random=urand(0,3);
						pPlayer->TeleportTo(189,FFA_Entree_Monastere[random][0],FFA_Entree_Monastere[random][1],FFA_Entree_Monastere[random][2],FFA_Entree_Monastere[random][3]);
					}
				}
			}
		}

		void UpdateAI(const uint32 uiDiff)
		{
			if(timer<uiDiff)
			{
				Map::PlayerList const &PlList = me->GetMap()->GetPlayers();
				for (Map::PlayerList::const_iterator i = PlList.begin(); i != PlList.end(); ++i)
				{
					Player* pPlayer = i->GetSource();
					if(me->GetDistance2d(pPlayer)>4.0f) continue;
							if(me->GetOrientation()<3.14f/4||me->GetOrientation()>3.14f*7/4) if(pPlayer->GetPositionX()<me->GetPositionX()) {
								TeleportPlayer(pPlayer);
								return;
							}
							if(me->GetOrientation()>3.14f/4&&me->GetOrientation()<3.14f*3/4) if(pPlayer->GetPositionY()<me->GetPositionY()) {
								TeleportPlayer(pPlayer);
								return;
							}
							if(me->GetOrientation()>3.14f*3/4&&me->GetOrientation()<3.14f*5/4) if(pPlayer->GetPositionX()>me->GetPositionX()) {
								TeleportPlayer(pPlayer);
								return;
							}
							if(me->GetOrientation()>3.14f*5/4&&me->GetOrientation()<3.14f*7/4) if(pPlayer->GetPositionY()>me->GetPositionY()) {
								TeleportPlayer(pPlayer);
								return;
							}
						
				}
				timer=250;
			}
			else timer-=uiDiff;
		}
    };
};

class BuffSpawnFFA : public CreatureScript
{
    public:
    BuffSpawnFFA() : CreatureScript("BuffSpawnFFA") {}
	
    CreatureAI* GetAI(Creature *pCreature) const
    {
        return new BuffSpawnFFAAI (pCreature);
    }
    struct BuffSpawnFFAAI : public ScriptedAI
    {
        BuffSpawnFFAAI(Creature* pCreature) : ScriptedAI(pCreature) {}

		uint32 timer_cooldown;
		bool buff_recharge;
		GameObject* Buff_Recuperate;
		GameObject* Buff_Berserk;
		GameObject* Buff_Speed;
		GameObject* Buff_Spawn;
		uint32 gobID;
		
		void Reset()
		{
			me->SetVisible(false);
			buff_recharge=false;
			timer_cooldown=0;
			if(GameObject* gob = me->GetMap()->AddObject(0,BG_OBJECTID_REGENBUFF_ENTRY, me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(), me->GetOrientation(),0.0f, 0.0f, 0.0f, 0.0f,RESPAWN_ONE_DAY))
				Buff_Recuperate=gob;
			if(GameObject* gob = me->GetMap()->AddObject(0,BG_OBJECTID_BERSERKERBUFF_ENTRY, me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(), me->GetOrientation(),0.0f, 0.0f, 0.0f, 0.0f,RESPAWN_ONE_DAY))
				Buff_Berserk=gob;
			if(GameObject* gob = me->GetMap()->AddObject(0,BG_OBJECTID_SPEEDBUFF_ENTRY, me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(), me->GetOrientation(),0.0f, 0.0f, 0.0f, 0.0f,RESPAWN_ONE_DAY))
				Buff_Speed=gob;
			if(GameObject* gob = SpawnBuff())
				Buff_Spawn=gob;
		}
		GameObject* SpawnBuff()
		{
			if(Buff_Recuperate) {
				Buff_Recuperate->SetLootState(GO_JUST_DEACTIVATED);
			}
			if(Buff_Speed) {
				Buff_Speed->SetLootState(GO_JUST_DEACTIVATED);
			}
			if(Buff_Berserk) {
				Buff_Berserk->SetLootState(GO_JUST_DEACTIVATED);
			}
			uint8 choix=urand(0,2);
			switch(choix)
			{
				case 0:
					if(Buff_Recuperate) {
						Buff_Recuperate->SetLootState(GO_READY);
						Buff_Recuperate->SetRespawnTime(RESPAWN_IMMEDIATELY);
						return Buff_Recuperate;
					}
				break;
				case 1:
					if(Buff_Speed) {
						Buff_Speed->SetLootState(GO_READY);
						Buff_Speed->SetRespawnTime(RESPAWN_IMMEDIATELY);
						return Buff_Speed;
					}
				break;
				case 2:
					if(Buff_Berserk) {
						Buff_Berserk->SetLootState(GO_READY);
						Buff_Berserk->SetRespawnTime(RESPAWN_IMMEDIATELY);
						return Buff_Berserk;
					}
				break;
			}
			return NULL;
		}
		void UpdateAI(const uint32 uiDiff)
		{
			if(Buff_Spawn) {
				if (Buff_Spawn->getLootState()==GO_JUST_DEACTIVATED&&!buff_recharge) {
					timer_cooldown=60000;
					buff_recharge=true;
				}
				if(buff_recharge) {
					if(timer_cooldown<uiDiff)
					{
						Buff_Spawn=SpawnBuff();
						buff_recharge=false;
					}
					else timer_cooldown-=uiDiff;
				}
			}
		}
    };
};

class EyeSpawnFFA : public CreatureScript
{
    public:
    EyeSpawnFFA() : CreatureScript("EyeSpawnFFA") {}
	
    CreatureAI* GetAI(Creature *pCreature) const
    {
        return new EyeSpawnFFAAI (pCreature);
    }
    struct EyeSpawnFFAAI : public ScriptedAI
    {
        EyeSpawnFFAAI(Creature* pCreature) : ScriptedAI(pCreature) {}

		uint32 timer_cooldown;
		bool buff_recharge;
		GameObject* Buff_Eye;
		uint32 gobID;
		
		void Reset()
		{
			me->SetVisible(false);
			buff_recharge=false;
			timer_cooldown=0;
			if(GameObject* gob = me->GetMap()->AddObject(0,184663, me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(), me->GetOrientation(),0.0f, 0.0f, 0.0f, 0.0f,RESPAWN_ONE_DAY))
				Buff_Eye=gob;
			Buff_Eye->SetLootState(GO_READY);
			Buff_Eye->SetRespawnTime(RESPAWN_IMMEDIATELY);
		}
		void UpdateAI(const uint32 uiDiff)
		{
			if(Buff_Eye) {
				if (Buff_Eye->getLootState()==GO_JUST_DEACTIVATED&&!buff_recharge) {
					timer_cooldown=60000;
					buff_recharge=true;
				}
				if(buff_recharge) {
					if(timer_cooldown<uiDiff)
					{
						if(Buff_Eye) {
							Buff_Eye->SetLootState(GO_READY);
							Buff_Eye->SetRespawnTime(RESPAWN_IMMEDIATELY);
						}
						buff_recharge=false;
					}
					else timer_cooldown-=uiDiff;
				}
			}
		}
    };
};

class PorteFFALevier : public GameObjectScript
{
public:
    PorteFFALevier() : GameObjectScript("PorteFFALevier") { }
    bool OnGossipHello(Player* player, GameObject* go)
    {
		Creature* pUpdate;
		std::list<Creature*> npcList;
		player->GetCreatureListWithEntryInGrid(npcList,UpdateFFAID, 20.0f);
		if (npcList.empty()) {
			sLog->outError(LOG_FILTER_GENERAL,"ZoneFFA: Erreur Levier, trigger introuvable");
			return true;
		}
		for (std::list<Creature*>::const_iterator itr = npcList.begin(); itr != npcList.end(); ++itr)
			pUpdate=(*itr);
        if(!pUpdate) {
			sLog->outError(LOG_FILTER_GENERAL,"ZoneFFA: Erreur Levier, trigger introuvable");
			return true;
		}
			if (GameObject* pTrigger = player->FindNearestGameObject(PorteCouloirID, 20.0f))
				pUpdate->AI()->SetData(0,0);
			if (GameObject* pTrigger = player->FindNearestGameObject(PorteCathedraleID, 20.0f))
				pUpdate->AI()->SetData(1,0);
			if (GameObject* pTrigger = player->FindNearestGameObject(PorteAutelID, 20.0f))
				pUpdate->AI()->SetData(2,0);
		
        return false;
    }
};

void AddSC_ZoneFFA()
{
    new UpdateFFAZone();
    new PorteFFALevier();
    new BuffSpawnFFA();
    new DoorFFA();
    new EyeSpawnFFA();
    new PortalFFA();
}