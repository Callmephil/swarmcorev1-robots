#include "Extension.h"
#include "Chat.h"
#include "ObjectMgr.h"
#include "ScriptMgr.h"
#include "ArenaTeam.h"
#include "ArenaTeamMgr.h"
#include "World.h"
#include "QueryResult.h"
#include "Language.h"
#include "Battleground.h"
#include "Pet.h"
#define GOSSIP_ITEM_STABLE "Stable"
#define GOSSIP_ITEM_NEWPET "New Pet"
#define GOSSIP_ITEM_BOAR "Boar"
#define GOSSIP_ITEM_SERPENT "Serpent"
#define GOSSIP_ITEM_SCRAB "Scrab"
#define GOSSIP_ITEM_LION "Lion"
#define GOSSIP_ITEM_WOLF "Wolf"
#define GOSSIP_ITEM_RAVAGER "Ravenger"
 
#define GOSSIP_ITEM_UNTRAINEPET "Restart Pet"

class Npc_Beastmaster : public CreatureScript
{
public:
        Npc_Beastmaster() : CreatureScript("Npc_Beastmaster") { }

void CreatePet(Player *player, Creature * m_creature, uint32 entry) {

        
      if(player->getClass() != CLASS_HUNTER) {
            m_creature->MonsterWhisper("You are not a Hunter!", player->GetGUID());
            player->PlayerTalkClass->SendCloseGossip();
            return;
        
     }
        if(player->GetPet()) {
            m_creature->MonsterWhisper("First you must drop your Pet!", player->GetGUID());
            return;
        }
		
        Creature *creatureTarget = m_creature->SummonCreature(entry, player->GetPositionX()+4, player->GetPositionY(), player->GetPositionZ()+2, 0/*player->GetOrientation()*/, TEMPSUMMON_CORPSE_TIMED_DESPAWN, 60000);
    if(!creatureTarget) return;
        
        Pet* pet = player->CreateTamedPetFrom(creatureTarget, 0);
        if(!pet) return;
 
        // kill original creature
        creatureTarget->setDeathState(JUST_DIED);
        creatureTarget->RemoveCorpse();
        creatureTarget->SetHealth(0);                       // just for nice GM-mode view
 
    //    pet->SetPower(POWER_HAPPINESS, 1048000);
 
        //pet->SetUInt32Value(UNIT_FIELD_PETEXPERIENCE,0);
        //pet->SetUInt32Value(UNIT_FIELD_PETNEXTLEVELEXP, uint32((Trinity::XP::xp_to_level(70))/4));
 
        // prepare visual effect for levelup
        pet->SetUInt32Value(UNIT_FIELD_LEVEL, player->getLevel() - 1);
        pet->GetMap()->AddToMap((Creature*)pet);
        // visual effect for levelup
        pet->SetUInt32Value(UNIT_FIELD_LEVEL, player->getLevel());
 
        
       /* if(!pet->InitStatsForLevel(player->getLevel()))
            sLog->outError ("Pet Create fail: no init stats for entry %u", entry);*/
 
        pet->UpdateAllStats();
        
        // caster have pet now
        player->SetMinion(pet, true);
 
        pet->SavePetToDB(PET_SAVE_AS_CURRENT);
        pet->InitTalentForLevel();
        player->PetSpellInitialize();
        
        //end
        //player->PlayerTalkClass->CloseGossip();
    //    m_creature->MonsterWhisper("Hmm pas mal votre nouveau compagnon! Quel nom lui avez vous donnez?", player->GetGUID());
               return;
    }
 
 
bool OnGossipHello(Player *player, Creature * m_creature)
{
       //bool EnableHunterSpells = ConfigMgr::GetBoolDefault("BeastMaster.EnableHunterSpells", true);

      if(player->getClass() != CLASS_HUNTER)
        {
            m_creature->MonsterWhisper("You are not a Hunter!", player->GetGUID());
            return true;
        }
        player->ADD_GOSSIP_ITEM(4, "Get a New Pet.", GOSSIP_SENDER_MAIN, 30);


        if (player->CanTameExoticPets())
        {
            player->ADD_GOSSIP_ITEM(4, "Get a New Exotic Pet.", GOSSIP_SENDER_MAIN, 50);
        }
        player->ADD_GOSSIP_ITEM(5, "Close Beastmaster Window", GOSSIP_SENDER_MAIN, 150);
        player->SEND_GOSSIP_MENU(1, m_creature->GetGUID());		 
        return true;			  
    }
 
bool OnGossipSelect(Player *player, Creature * m_creature, uint32 sender, uint32 action)
    {
                player->PlayerTalkClass->ClearMenus();
        switch (action)
        {

        case 100:
           if(player->getClass() != CLASS_HUNTER)
        {
            m_creature->MonsterWhisper("You are not a Hunter!", player->GetGUID());
            return true;
        }
        player->ADD_GOSSIP_ITEM(4, "Get a New Pet.", GOSSIP_SENDER_MAIN, 30);
        if (player->CanTameExoticPets())
        {
            player->ADD_GOSSIP_ITEM(4, "Get a New Exotic Pet.", GOSSIP_SENDER_MAIN, 50);
        }
        player->ADD_GOSSIP_ITEM(2, "Take me to the Stable.", GOSSIP_SENDER_MAIN, GOSSIP_OPTION_STABLEPET);
        player->ADD_GOSSIP_ITEM(6, "Sell me some Food for my Pet.", GOSSIP_SENDER_MAIN, GOSSIP_OPTION_VENDOR);
                player->ADD_GOSSIP_ITEM(5, "Close Beastmaster Window.", GOSSIP_SENDER_MAIN, 150);
        player->SEND_GOSSIP_MENU(1, m_creature->GetGUID());
                break;

                case 150:
                        player->CLOSE_GOSSIP_MENU();
                break;
        
        
        
        
      
           /* case GOSSIP_OPTION_STABLEPET:
            player->GetSession()->SendStablePet(m_creature->GetGUID());
            break; 
           
		   case GOSSIP_OPTION_VENDOR:
            player->GetSession()->SendListInventory(m_creature->GetGUID());
            break; */
////////////////////////////////////PET EXOTIQUUE///////////////////////////////////////////            
case 50:  
player->ADD_GOSSIP_ITEM(2, "-- Main Menu", GOSSIP_SENDER_MAIN, 100);
player->ADD_GOSSIP_ITEM(5, "Chimaera.", GOSSIP_SENDER_MAIN, 51);
player->ADD_GOSSIP_ITEM(5, "Core Hound.", GOSSIP_SENDER_MAIN, 52);
player->ADD_GOSSIP_ITEM(5, "Devilsaur.", GOSSIP_SENDER_MAIN, 53);
player->ADD_GOSSIP_ITEM(5, "Rhino.", GOSSIP_SENDER_MAIN, 54);
player->ADD_GOSSIP_ITEM(5, "Silithid.", GOSSIP_SENDER_MAIN, 55);
player->ADD_GOSSIP_ITEM(5, "Worm.", GOSSIP_SENDER_MAIN, 56);  
player->ADD_GOSSIP_ITEM(5, "Loque'nahak.", GOSSIP_SENDER_MAIN, 57);
player->ADD_GOSSIP_ITEM(5, "Skoll.", GOSSIP_SENDER_MAIN, 58);
player->ADD_GOSSIP_ITEM(5, "Gondria.", GOSSIP_SENDER_MAIN, 59);
player->ADD_GOSSIP_ITEM(6, "Arcturis.", GOSSIP_SENDER_MAIN, 60);
player->SEND_GOSSIP_MENU(1, m_creature->GetGUID());
break;
			
			case 51: //chimera
            CreatePet(player, m_creature, 21879);
			player->CLOSE_GOSSIP_MENU();
            break;
            
			case 52: //core hound
            CreatePet(player, m_creature, 21108);
			player->CLOSE_GOSSIP_MENU();
            break;
            
			case 53: //Devilsaur
            CreatePet(player, m_creature, 20931);
			player->CLOSE_GOSSIP_MENU();
            break;
            
			case 54: //rhino
            CreatePet(player, m_creature, 30445);
			player->CLOSE_GOSSIP_MENU();
            break;
            
			case 55: //silithid
            CreatePet(player, m_creature, 5460);
			player->CLOSE_GOSSIP_MENU();
            break;
            
			case 56: //Worm
            CreatePet(player, m_creature, 30148);
			player->CLOSE_GOSSIP_MENU();
            break;
            
			case 57: //Loque'nahak
            CreatePet(player, m_creature, 32517);
			player->CLOSE_GOSSIP_MENU();
			break;
           
  		    case 58: //Skoll
			CreatePet(player, m_creature, 35189);
			player->CLOSE_GOSSIP_MENU();
			break;
            
			case 59: //Gondria																
            CreatePet(player, m_creature, 33776);
			player->CLOSE_GOSSIP_MENU();			 
			break;	
            
			case 60: //Arcturis																
            CreatePet(player, m_creature, 38453);
			player->CLOSE_GOSSIP_MENU();			 
			break;																		
/////////////////////////////////////////////////////////////////////////////////////////////////           
			case 30:
            player->ADD_GOSSIP_ITEM(2, "-- Main Menu", GOSSIP_SENDER_MAIN, 100);
            player->ADD_GOSSIP_ITEM(5, "Bat.", GOSSIP_SENDER_MAIN, 18);
			player->ADD_GOSSIP_ITEM(5, "Bear.", GOSSIP_SENDER_MAIN, 1);
			player->ADD_GOSSIP_ITEM(5, "Boar.", GOSSIP_SENDER_MAIN, 2);
			player->ADD_GOSSIP_ITEM(5, "Cat.", GOSSIP_SENDER_MAIN, 4);
			player->ADD_GOSSIP_ITEM(5, "Carrion Bird.", GOSSIP_SENDER_MAIN, 5);
			player->ADD_GOSSIP_ITEM(5, "Crab.", GOSSIP_SENDER_MAIN, 6);
			player->ADD_GOSSIP_ITEM(5, "Crocolisk.", GOSSIP_SENDER_MAIN, 7);
			player->ADD_GOSSIP_ITEM(5, "Dragonhawk.", GOSSIP_SENDER_MAIN, 17);
			player->ADD_GOSSIP_ITEM(5, "Gorilla.", GOSSIP_SENDER_MAIN, 8);
			player->ADD_GOSSIP_ITEM(5, "Hyena.", GOSSIP_SENDER_MAIN, 9);
			player->ADD_GOSSIP_ITEM(5, "Moth.", GOSSIP_SENDER_MAIN, 10);
			player->ADD_GOSSIP_ITEM(5, "Owl.", GOSSIP_SENDER_MAIN, 11);
			player->ADD_GOSSIP_ITEM(0, "[Next page] ->", GOSSIP_SENDER_MAIN, 31);
            player->SEND_GOSSIP_MENU(1, m_creature->GetGUID());
                break;
        
			case 31:  
            player->ADD_GOSSIP_ITEM(2, "-- Main Menu", GOSSIP_SENDER_MAIN, 30);
            player->ADD_GOSSIP_ITEM(4, "-- Back", GOSSIP_SENDER_MAIN, 30);
			player->ADD_GOSSIP_ITEM(5, "Raptor.", GOSSIP_SENDER_MAIN, 20);
			player->ADD_GOSSIP_ITEM(5, "Ravager.", GOSSIP_SENDER_MAIN, 19);
			player->ADD_GOSSIP_ITEM(5, "Strider.", GOSSIP_SENDER_MAIN, 13);
			player->ADD_GOSSIP_ITEM(5, "Scorpid.", GOSSIP_SENDER_MAIN, 414);
			player->ADD_GOSSIP_ITEM(5, "Spider.", GOSSIP_SENDER_MAIN, 16);
			player->ADD_GOSSIP_ITEM(5, "Serpent.", GOSSIP_SENDER_MAIN, 21);  
			player->ADD_GOSSIP_ITEM(5, "Wasp.", GOSSIP_SENDER_MAIN, 93);

            player->ADD_GOSSIP_ITEM(5, "Nether Ray", GOSSIP_SENDER_MAIN, 200);
            player->ADD_GOSSIP_ITEM(5, "Swiftwing Shredder", GOSSIP_SENDER_MAIN, 201);  
            player->ADD_GOSSIP_ITEM(5, "Sporewing", GOSSIP_SENDER_MAIN, 202);
            player->ADD_GOSSIP_ITEM(5, "Turtle", GOSSIP_SENDER_MAIN, 203);  
            player->ADD_GOSSIP_ITEM(5, "Warp Stalker", GOSSIP_SENDER_MAIN, 204);
            player->ADD_GOSSIP_ITEM(5, "Wolf", GOSSIP_SENDER_MAIN, 205);
			
            player->SEND_GOSSIP_MENU(1, m_creature->GetGUID());
                break;
			
			case 16: //Spider
            CreatePet(player, m_creature, 2349);
			player->CLOSE_GOSSIP_MENU();			 
            break;
            
			case 17: //Dragonhawk
            CreatePet(player, m_creature, 27946);     
			player->CLOSE_GOSSIP_MENU();			                  
            break;
            
			case 18: //Bat
	        CreatePet(player, m_creature, 28233);
			player->CLOSE_GOSSIP_MENU();			 
            break;
           
   		    case 19: //Ravager
            CreatePet(player, m_creature, 17199);
			player->CLOSE_GOSSIP_MENU();			 
            break;
            
			case 20: //Raptor
			CreatePet(player, m_creature, 14821);
			player->CLOSE_GOSSIP_MENU();			 
            break;
            
			case 21: //Serpent
			CreatePet(player, m_creature, 28358);
			player->CLOSE_GOSSIP_MENU();			 
			break;
            
			case 1: //bear
			CreatePet(player, m_creature, 29319);
			player->CLOSE_GOSSIP_MENU();			 
			break;
           
		   case 2: //Boar
			CreatePet(player, m_creature, 29996);
			player->CLOSE_GOSSIP_MENU();			 
            break;
           
			case 93: //Bug
			CreatePet(player, m_creature, 28085);
			player->CLOSE_GOSSIP_MENU();			 
            break;
           
		    case 4: //cat
			CreatePet(player, m_creature, 28097);
			player->CLOSE_GOSSIP_MENU();			 
			break;
            
			case 5: //carrion
			CreatePet(player, m_creature, 26838);
			player->CLOSE_GOSSIP_MENU();			 
			break;
            
			case 6: //crab
			CreatePet(player, m_creature, 24478);
			player->CLOSE_GOSSIP_MENU();			 
			break;   
           
		   case 7: //crocolisk
			CreatePet(player, m_creature, 1417);
			player->CLOSE_GOSSIP_MENU();			 
			break;  
            
			case 8: //gorilla
			CreatePet(player, m_creature, 28213);
			player->CLOSE_GOSSIP_MENU();			 
			break;
            
			case 9: //hynea
			CreatePet(player, m_creature, 13036);
			player->CLOSE_GOSSIP_MENU();			 
			break;
			
            case 10: //Moth
			CreatePet(player, m_creature, 27421);
			player->CLOSE_GOSSIP_MENU();			 
			break;
           
		    case 11: //owl
			CreatePet(player, m_creature, 23136);
			player->CLOSE_GOSSIP_MENU();			 
			break;
            
			case 13: //strider
			CreatePet(player, m_creature, 22807);
			player->CLOSE_GOSSIP_MENU();			 
			break;
            
			case 414: //scorpid
			CreatePet(player, m_creature, 9698);
			player->CLOSE_GOSSIP_MENU();			 
			break;
			
			case 200://Raoie du neant
			CreatePet(player, m_creature, 21901);
			player->CLOSE_GOSSIP_MENU();			 
			break;
			
			case 201://Swiftwing Shredder - Sporewing - Turtle - Warp Stalker
			CreatePet(player, m_creature, 20673);
			player->CLOSE_GOSSIP_MENU();			 
			break;
			
			case 202://Sporail�e
			CreatePet(player, m_creature, 18280);
			player->CLOSE_GOSSIP_MENU();			 
			break;
			
			case 203://Tortue
			CreatePet(player, m_creature, 6352);
			player->CLOSE_GOSSIP_MENU();			 
			break;
			
			case 204://Trackeur dim 
			CreatePet(player, m_creature, 23163);
			player->CLOSE_GOSSIP_MENU();			 
			break;
			
			case 205://Louep horde/ally
			if (player->GetTeam() == HORDE)	
			{
			CreatePet(player, m_creature, 17280);
			player->CLOSE_GOSSIP_MENU();	
			}
			else
			{
			CreatePet(player, m_creature, 10077);
			player->CLOSE_GOSSIP_MENU();	
			}			
			break;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        }
        return true;
    }
};
 
void AddSC_Npc_Beastmaster()
{
    new Npc_Beastmaster();
}