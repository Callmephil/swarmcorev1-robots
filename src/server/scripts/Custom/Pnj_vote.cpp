#include "Extension.h"

#define token1 "Buy 25 Champion's Seal (40 pts vote)"
#define token2 "Buy 100 Champion's Seal (150 pts vote)"
#define token3 "Buy 200 Champion's Seal (280 pts vote)"
#define token4 "Buy Customization (10 pts vote)"
#define token5 "Buy Race Change (25 pts vote)"
#define token6 "Buy Faction Change (50 pts vote)"
#define token7 "Turn Honor into Champion's Seal (free)"
#define token8 "Turn Conquest into Champion's Seal (free)"

#define changename "Change Name"
#define changerace "Change Race"
#define changefaction "Change Faction"

#define retour "Retour"

class Pnj_vote : public CreatureScript
{
    public:

        Pnj_vote()
            : CreatureScript("Pnj_vote")
        {
        }
                bool OnGossipHello(Player* player, Creature* creature)
                {
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, token1, GOSSIP_SENDER_MAIN, 1001);
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, token2, GOSSIP_SENDER_MAIN, 1002);
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, token3, GOSSIP_SENDER_MAIN, 1003);
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, token4, GOSSIP_SENDER_MAIN, 1004);
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, token5, GOSSIP_SENDER_MAIN, 1005);
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, token6, GOSSIP_SENDER_MAIN, 1006);
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, 	token7, GOSSIP_SENDER_MAIN, 1007);
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, 	token8, GOSSIP_SENDER_MAIN, 1008);
                    player->PlayerTalkClass->SendGossipMenu(80000, creature->GetGUID());
                    return true;
                }

                void SendDefaultMenu_tele(Player* player, Creature* creature, uint32 uiAction)
                {
                        // Not allow in combat
                        if (player->IsInCombat())
                        {
                            creature->MonsterSay("Vous etes en combat !", LANG_UNIVERSAL, player->GetGUID());
                            return;
                        }
						ChatHandler handler = ChatHandler(player->GetSession());
						switch(uiAction) {
							case 1001:
							{
								uint32 cost=40;
								uint32 reward=25;
								
								uint32 points=player->getExtension()->Kazooie_GetVotePoints();
								if(points<cost)
									handler.PSendSysMessage("You don't have enought vote points!");
								else {
									player->getExtension()->Kazooie_DeleteVotePoints(cost);
									player->ModifyCurrency(241,reward,true,true,false);
									player->CastSpell(player,36937,true);
								}
							}
							break;
							case 1002:
							{
								uint32 cost=150;
								uint32 reward=100;
							
								uint32 points=player->getExtension()->Kazooie_GetVotePoints();
								if(points<cost)
									handler.PSendSysMessage("You don't have enought vote points!");
								else {
									player->getExtension()->Kazooie_DeleteVotePoints(cost);
									player->ModifyCurrency(241,reward,true,true,false);
									player->CastSpell(player,36937,true);
								}
							}
							break;
							case 1003:
							{
								uint32 cost=280;
								uint32 reward=200;
							
								uint32 points=player->getExtension()->Kazooie_GetVotePoints();
								if(points<cost)
									handler.PSendSysMessage("You don't have enought vote points!");
								else {
									player->getExtension()->Kazooie_DeleteVotePoints(cost);
									player->ModifyCurrency(241,reward,true,true,false);
									player->CastSpell(player,36937,true);
								}
							}
							break;
							case 1004:
							{
								uint32 cost=10;
								uint32 points=player->getExtension()->Kazooie_GetVotePoints();
								if(points<cost)
									handler.PSendSysMessage("You don't have enought vote points!");
								else {
									player->getExtension()->Kazooie_DeleteVotePoints(cost);
									player->SetAtLoginFlag(AT_LOGIN_CUSTOMIZE);
									player->CastSpell(player,36937,true);
								}
							}
							break;
							case 1005:
							{
								uint32 cost=25;
								uint32 points=player->getExtension()->Kazooie_GetVotePoints();
								if(points<cost)
									handler.PSendSysMessage("You don't have enought vote points!");
								else {
									player->getExtension()->Kazooie_DeleteVotePoints(cost);
									player->SetAtLoginFlag(AT_LOGIN_CHANGE_RACE);
									player->CastSpell(player,36937,true);
								}
							}
							break;
							case 1006:
							{
								uint32 cost=50;
								uint32 points=player->getExtension()->Kazooie_GetVotePoints();
								if(points<cost)
									handler.PSendSysMessage("You don't have enought vote points!");
								else {
									player->getExtension()->Kazooie_DeleteVotePoints(cost);
									player->SetAtLoginFlag(AT_LOGIN_CHANGE_FACTION);
									player->CastSpell(player,36937,true);
								}
							}
							break;
							case 1007:
							{
								int32 seal=player->GetCurrency(CURRENCY_TYPE_HONOR_POINTS, true)/10;
								if(seal>0) {
									
									player->ModifyCurrency(CURRENCY_TYPE_HONOR_POINTS,-seal*10*CURRENCY_PRECISION,true,true,false);
									player->ModifyCurrency(241,seal,true,true,false);
									player->CastSpell(player,36937,true);
								}
							}
							break;
							case 1008:
							{
								int32 seal=player->GetCurrency(CURRENCY_TYPE_CONQUEST_POINTS, true)/5;
								if(seal>0) {
									player->ModifyCurrency(CURRENCY_TYPE_CONQUEST_POINTS,-seal*5*CURRENCY_PRECISION,true,true,false);
									player->ModifyCurrency(241,seal,true,true,false);
									player->CastSpell(player,36937,true);
								}
							}
							break;
						}
						if(uiAction==1000) {
							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, token1, GOSSIP_SENDER_MAIN, 1001);
							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, token2, GOSSIP_SENDER_MAIN, 1002);
							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, token3, GOSSIP_SENDER_MAIN, 1003);
							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, token4, GOSSIP_SENDER_MAIN, 1004);
							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, token5, GOSSIP_SENDER_MAIN, 1005);
							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, token6, GOSSIP_SENDER_MAIN, 1006);
							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, 	token7, GOSSIP_SENDER_MAIN, 1007);
							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, 	token8, GOSSIP_SENDER_MAIN, 1008);
							player->PlayerTalkClass->SendGossipMenu(80000, creature->GetGUID());
						}
						player->CLOSE_GOSSIP_MENU();
                }

                bool OnGossipSelect(Player* player, Creature* creature, uint32 uiSender, uint32 uiAction)
                {
                        player->PlayerTalkClass->ClearMenus();
                        // Main menu
                        if (uiSender == GOSSIP_SENDER_MAIN)
                                SendDefaultMenu_tele(player, creature, uiAction);
                        return true;
                }
};

class Pnj_vote_Moonlight : public CreatureScript
{
    public:

        Pnj_vote_Moonlight()
            : CreatureScript("Pnj_vote_Moonlight")
        {
        }
                bool OnGossipHello(Player* player, Creature* creature)
                {
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, token1, GOSSIP_SENDER_MAIN, 1001);
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, token2, GOSSIP_SENDER_MAIN, 1002);
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, token3, GOSSIP_SENDER_MAIN, 1003);
					
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, 	token7, GOSSIP_SENDER_MAIN, 1007);
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, 	token8, GOSSIP_SENDER_MAIN, 1008);
                    player->PlayerTalkClass->SendGossipMenu(80000, creature->GetGUID());
                    return true;
                }

                void SendDefaultMenu_tele(Player* player, Creature* creature, uint32 uiAction)
                {
                        // Not allow in combat
                        if (player->IsInCombat())
                        {
                            creature->MonsterSay("Vous etes en combat !", LANG_UNIVERSAL, player->GetGUID());
                            return;
                        }
						ChatHandler handler = ChatHandler(player->GetSession());
						switch(uiAction) {
							case 1001:
							{
								uint32 cost=40;
								uint32 reward=25;
								
								uint32 points=player->getExtension()->Kazooie_GetVotePoints();
								if(points<cost)
									handler.PSendSysMessage("You don't have enought vote points!");
								else {
									player->getExtension()->Kazooie_DeleteVotePoints(cost);
									player->ModifyCurrency(241,reward,true,true,false);
									player->CastSpell(player,36937,true);
								}
							}
							break;
							case 1002:
							{
								uint32 cost=150;
								uint32 reward=100;
							
								uint32 points=player->getExtension()->Kazooie_GetVotePoints();
								if(points<cost)
									handler.PSendSysMessage("You don't have enought vote points!");
								else {
									player->getExtension()->Kazooie_DeleteVotePoints(cost);
									player->ModifyCurrency(241,reward,true,true,false);
									player->CastSpell(player,36937,true);
								}
							}
							break;
							case 1003:
							{
								uint32 cost=280;
								uint32 reward=200;
							
								uint32 points=player->getExtension()->Kazooie_GetVotePoints();
								if(points<cost)
									handler.PSendSysMessage("You don't have enought vote points!");
								else {
									player->getExtension()->Kazooie_DeleteVotePoints(cost);
									player->ModifyCurrency(241,reward,true,true,false);
									player->CastSpell(player,36937,true);
								}
							}
							break;
							
							case 1007:
							{
								int32 seal=player->GetCurrency(CURRENCY_TYPE_HONOR_POINTS, true)/10;
								if(seal>0) {
									
									player->ModifyCurrency(CURRENCY_TYPE_HONOR_POINTS,-seal*10*CURRENCY_PRECISION,true,true,false);
									player->ModifyCurrency(241,seal,true,true,false);
									player->CastSpell(player,36937,true);
								}
							}
							break;
							case 1008:
							{
								int32 seal=player->GetCurrency(CURRENCY_TYPE_CONQUEST_POINTS, true)/5;
								if(seal>0) {
									player->ModifyCurrency(CURRENCY_TYPE_CONQUEST_POINTS,-seal*5*CURRENCY_PRECISION,true,true,false);
									player->ModifyCurrency(241,seal,true,true,false);
									player->CastSpell(player,36937,true);
								}
							}
							break;
						}
						if(uiAction==1000) {
							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, token1, GOSSIP_SENDER_MAIN, 1001);
							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, token2, GOSSIP_SENDER_MAIN, 1002);
							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, token3, GOSSIP_SENDER_MAIN, 1003);
							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, token4, GOSSIP_SENDER_MAIN, 1004);
							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, token5, GOSSIP_SENDER_MAIN, 1005);
							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, token6, GOSSIP_SENDER_MAIN, 1006);
							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, 	token7, GOSSIP_SENDER_MAIN, 1007);
							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, 	token8, GOSSIP_SENDER_MAIN, 1008);
							player->PlayerTalkClass->SendGossipMenu(80000, creature->GetGUID());
						}
						player->CLOSE_GOSSIP_MENU();
                }

                bool OnGossipSelect(Player* player, Creature* creature, uint32 uiSender, uint32 uiAction)
                {
                        player->PlayerTalkClass->ClearMenus();
                        // Main menu
                        if (uiSender == GOSSIP_SENDER_MAIN)
                                SendDefaultMenu_tele(player, creature, uiAction);
                        return true;
                }
};

void AddSC_Pnj_vote()
{
    new Pnj_vote();
	new Pnj_vote_Moonlight();
}