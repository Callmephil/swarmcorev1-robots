//#include "ScriptPCH.h"
//#include "gamePCH.h"
#include "Extension.h"
#include "Chat.h"

using namespace std;

enum eMenus
{

	NVM_EXIT                        = 1000,
	MY_CITIES_MENU                  = 1,
	MY_TRAINER						= 2,
	PLAYER_MENU                     = 3,
	MY_SHOP_MENU                 	= 4,
	MY_TRAINER_MENU2				= 5, // Unused
	CUSTOM_AREAS_MENU               = 6,
	MY_TRAINER_MENU					= 7,
	CUSTOM_LOCATION_PORT   			= 8,
	A_CITIES_MENU                   = 9,
	H_CITIES_MENU                   = 10,
	DARNASSUS_PORT                  = 11,
	THE_EXODAR_PORT                 = 12,
	STORMWIND_PORT                  = 13,
	IRONFORGE_PORT                  = 14,
	ORGRIMMAR_PORT                  = 15,
	THUNDER_BLUFF_PORT              = 16,
	UNDERCITY_PORT                  = 17,
	SILVERMOON_CITY_PORT    		= 18,
	MY_LOCATIONS_MENU               = 19, 
	SERVICES_MENU                   = 20,
	SERVICES_MENU2                  = 22,
	DALARAN_PORT                    = 23,
	MY_SHOP_MENU2		            = 24,
	MY_SHOP_PORT		            = 25,
	MY_SHOP_PORT2		            = 26,
	MY_SHOP_PORT3		            = 27,
	MY_SHOP_PORT4		            = 28,
	MY_SHOP_PORT5		            = 29,
	MY_SHOP_PORT6		            = 30,
	MY_SHOP_PORT7		            = 31,
	PLAYER_MENU2                    = 32,
	SAVE_MENU      					= 33,
	HEAL_MENU      					= 34,
	SHATTRATH_CITY_PORT             = 35,
	TITREA_MENU						= 36,
	TITREH_MENU						= 37,
	TITRE_MENU						= 38,
	BUFF_MENU      					= 39, //Unused
	NPC_TRAIN     				    = 40, //UnUsed
	SKILLS_MENU						= 41,
	CLASSEMENT_MENU					= 42,
	REP_MENU					    = 50,
	MYREP_MENU						= 51,
	MYREP_MENU2                     = 52,
	MYREP_MENU3						= 53,
	MYREP_MENU4						= 54,
	MYREP_MENU5						= 55,
	MYREP_MENU6						= 56,
	ZONE_FFA				        = 100,
	MORPH_MENU						= 800,
	VIP_MENU						= 900,
	CUSTOMIZE_MENU					= 901,
	ACTION_RENAME					= 902,
	ACTION_CUSTOMIZE				= 903,
	ACTION_CHANGE_FACTION			= 904,
	ACTION_CHANGE_RACE				= 905
};

enum MiscSpells
{
	SPELL_FULL_HEAL             = 25840,
	SPELL_ANTI_HEAL_DEBUFF      = 45523,
};

enum BuffSpells
{
	SPELL_POWER_WORD_FORTITUDE          = 48162,
	SPELL_PRAYER_OF_SPIRIT              = 48074,
	SPELL_SHADOW_PROTECTION             = 48170,
	SPELL_GREATER_BLESSING_OF_KINGS     = 43223,
	SPELL_GREATER_BLESSING_OF_MIGHT     = 48934,
	SPELL_GREATER_BLESSING_OF_WISDOM    = 48938,
	SPELL_GREATER_BLESSING_OF_SANCTUARY = 25899,
	SPELL_ARCANE_INTELLECT              = 36880,
	SPELL_DAMPEN_MAGIC                  = 43015,
	SPELL_AMPLIFY_MAGIC                 = 43017,
	SPELL_GIFT_OF_THE_WILD              = 69381,
	SPELL_THORNS                        = 467,
	SPELL_BRILLIANCE_INTELLECT          = 69994,
	SPELL_INFINITE_REPLENISHMENT        = 61782,
	SPELL_INNER_FIRE                    = 48168,
	SPELL_WATER_BREATHING               = 40621,
};

enum Action
{
	// Buff Actions
	ACTION_POWER_WORD_FORTITUDE          = 506,
	ACTION_PRAYER_OF_SPIRIT              = 507,
	ACTION_SHADOW_PROTECTION             = 508,
	ACTION_GREATER_BLESSING_OF_KINGS     = 509,
	ACTION_GREATER_BLESSING_OF_MIGHT     = 510,
	ACTION_GREATER_BLESSING_OF_WISDOM    = 511,
	ACTION_GREATER_BLESSING_OF_SANCTUARY = 512,
	ACTION_ARCANE_INTELLECT              = 513,
	ACTION_DAMPEN_MAGIC                  = 514,
	ACTION_AMPLIFY_MAGIC                 = 515,
	ACTION_GIFT_OF_THE_WILD              = 516,
	ACTION_THORNS                        = 517,
	ACTION_WATER_BREATHING               = 521,
};
enum Titles
{
	//Token ID!
	TITLE_TOKEN = 241, //Changer moi	
};

enum VIP
{
	VIP_TOKEN = 38312,
};
enum eIcons
{
	CHAT_ICON               = 0,
	VENDOR_ICON             = 1,
	FLIGHT_ICON             = 2,
	TRAINER_ICON            = 3,
	GEAR_ICON               = 4,
	GEAR_ICON_2             = 5,
	BANK_ICON               = 6,
	CHAT_DOTS_ICON          = 7,
	TABARD_ICON             = 8,
	SWORDS_ICON             = 9,
	GOLD_DOT_ICON           = 10
};

enum SpellI
{
	SpellI_INVISI = 55848
};

/* Gossip Colors */

#define Deja_le_titre "You already learn his title !"	
#define No_Token "Token Can be Earned after a Duel or in FFA Area And by converting Voting Point !"	

#define TEXT_CUSTOM_RED1                      "|cff990033"
#define TEXT_CUSTOM_RED2                      "|cff000033"
#define TEXT_CUSTOM_RED3                      "|cff873600"
#define TEXT_CUSTOM_RED4                      "|cff873600"

#define TEXT_CUSTOM_ORANGE                      "|cff873600"
#define TEXT_CUSTOM_RED                         "|cffA40000"
#define TEXT_CUSTOM_BLUE                        "|cff00479E"
#define TEXT_CUSTOM_GREEN                       "|cff065600"
#define TEXT_CUSTOM_PURPLE                      "|cff5A005B"
#define TEXT_CUSTOM_GREY                        "|cff515151"
#define TEXT_CUSTOM_CYAN                        "|cff005853"
#define TEXT_CUSTOM_BROWN                       "|cff584200"
/* Gossip Strings */
#define IN_COMBAT  								"Oww Master i hate the blood, call me back when you have finish"
#define SERVICES_GOSSIP                        	"Port ->" 
#define SERVICES_GOSSIP2                        "Options ->" 
#define MESSAGE_CHARACTER_SAVE_TO_DB 			"Succes ! player saved"
#define SAVE									"Save characters"
#define No_ZONE									"My,My Energie is low i can't do more in this area"
/* Add your own locations */
#define CUSTOM_AREAS_GOSSIP                     "The Mall ->" //Add your own custom title
#define FFA										"FFA Area"
#define MY_SHOP_PORT4_GOSSIP          			"Duel Area" //Add your own custom location
/* UNUSED
#define MY_SHOP_PORT_GOSSIP          			"Glyphes" //Add your own custom location
#define MY_SHOP_PORT2_GOSSIP           			"Armures & Armes" //Add your own custom location
#define MY_SHOP_PORT3_GOSSIP          			"Montures & Pet chasseur" //Add your own custom location
#define MY_SHOP_PORT5_GOSSIP          			"Gemmes & Enchant" //Add your own custom location
#define MY_SHOP_PORT6_GOSSIP          			"Metiers" //Add your own custom location
#define MY_SHOP_PORT7_GOSSIP          			"Personnalisation" //Add your own custom location
*/
// vendor
#define MY_SHOP_GOSSIP                			"Vendor"
//#define MY_SHOP_GOSSIP2 						"Raccourcis vers ->"
#define EMOTE_COOLDOWN 							"I can't do that now!..."
#define MY_TRAINER_GOSSIP                		"I would like to reset my talents"
#define MY_TRAINER_GOSSIP2                		"I would like to learn my secondary spec"
#define TEXTE_2 								"Faisons des affaires."
// Reputation
//Not working 
/* UNUSED
#define REP_GOSSIP 								"Apprendre mes reputations"
#define MYREP_GOSSIP							"Classique"
#define MYREP_GOSSIP2							"The burning Crusade"
#define MYREP_GOSSIP3							"The Wrath of the Lich King"
#define MYREP_GOSSIP4							"Capitales"
#define MYREP_GOSSIP5							"sous-rep"
#define MYREP_GOSSIP6							"sous-rep"
*/

//OPTION//
#define NVM_GOSSIP  							"Nevermind!"
#define GO_BACK_GOSSIP2                         "www.Swarm-Servers.com" //N'oubliez pas de votez pour nous!"	
#define GO_BACK_GOSSIP                          "Back"
#define CITIES_GOSSIP                           "City"
/* Alliance Cities */
#define DARNASSUS_GOSSIP                		"Darnassus"
#define IRONFORGE_GOSSIP                		"Ironforge"
#define STORMWIND_GOSSIP                        "Stormwind"
#define THE_EXODAR_GOSSIP                       "Exodar"
/* Horde Cities */
#define ORGRIMMAR_GOSSIP                        "Orgrimmar"
#define SILVERMOON_CITY_GOSSIP         			"Silvermoon"
#define THUNDER_BLUFF_GOSSIP            		"Thunder Bluff"
#define UNDERCITY_GOSSIP                        "Undercity"
#define DALARAN_GOSSIP                          "Dalaran"
#define SHATTRATH_CITY_GOSSIP           		"Shattrath"

#define Characters_Option_Gossip				"(New) [Characters Option]"

class teleporter : public CreatureScript
{
public:
	teleporter()
		: CreatureScript("teleporter")
	{}

	bool OnGossipHello(Player* player, Creature* creature)
	{
		if (player->IsInCombat())
		{
			creature->DespawnOrUnsummon(0);
			creature->MonsterWhisper(IN_COMBAT, player->GetGUID());
		}


		if (player->getLevel() >= 80)
		{
			creature->AddAura(SpellI_INVISI,creature);
			{
				if (player->GetAreaId() == 2270) //FFA
				{
					player->CLOSE_GOSSIP_MENU();
					ChatHandler(player->GetSession()).PSendSysMessage(No_ZONE);
					player->GetSession()->SendNotification(No_ZONE);
					player->GetSession()->SendListInventory(creature->GetGUID());
					return true;
				}else{
					{
						player->PrepareQuestMenu(creature->GetGUID());
						player->SendPreparedQuest(creature->GetGUID());
					}
					player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_BLUE SERVICES_GOSSIP, GOSSIP_SENDER_MAIN, SERVICES_MENU);
					player->ADD_GOSSIP_ITEM(SWORDS_ICON, TEXT_CUSTOM_GREEN SERVICES_GOSSIP2, GOSSIP_SENDER_MAIN, SERVICES_MENU2);
					player->ADD_GOSSIP_ITEM(VENDOR_ICON, TEXT_CUSTOM_BROWN MY_SHOP_GOSSIP, GOSSIP_SENDER_MAIN, MY_SHOP_MENU);
					/*		if (player->HasItemCount(VIP_TOKEN, 1, false ))
					{*/
					player->ADD_GOSSIP_ITEM(5, Characters_Option_Gossip, GOSSIP_SENDER_MAIN, VIP_MENU);
					//	}
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, TEXT_CUSTOM_RED GO_BACK_GOSSIP2, GOSSIP_SENDER_MAIN, PLAYER_MENU2);
					player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				}
				player->PlayerTalkClass->ClearMenus();

				if (player->GetZoneId() == 3277) //
				{
					player->CLOSE_GOSSIP_MENU();
					ChatHandler(player->GetSession()).PSendSysMessage(No_ZONE);
					player->GetSession()->SendListInventory(creature->GetGUID());
					return true;
				}else{
					{
						player->PrepareQuestMenu(creature->GetGUID());
						player->SendPreparedQuest(creature->GetGUID());
					}
					player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_BLUE SERVICES_GOSSIP, GOSSIP_SENDER_MAIN, SERVICES_MENU);
					player->ADD_GOSSIP_ITEM(SWORDS_ICON, TEXT_CUSTOM_GREEN SERVICES_GOSSIP2, GOSSIP_SENDER_MAIN, SERVICES_MENU2);
					player->ADD_GOSSIP_ITEM(VENDOR_ICON, TEXT_CUSTOM_BROWN MY_SHOP_GOSSIP, GOSSIP_SENDER_MAIN, MY_SHOP_MENU);
					//if (player->HasItemCount( VIP_TOKEN, 1, false )){
					player->ADD_GOSSIP_ITEM(5, Characters_Option_Gossip, GOSSIP_SENDER_MAIN, VIP_MENU);
					//	}
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, TEXT_CUSTOM_RED GO_BACK_GOSSIP2, GOSSIP_SENDER_MAIN, PLAYER_MENU2);
					player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				}
				player->PlayerTalkClass->ClearMenus();

				if (player->GetZoneId() == 3358) //
				{
					player->CLOSE_GOSSIP_MENU();
					ChatHandler(player->GetSession()).PSendSysMessage(No_ZONE);
					player->GetSession()->SendListInventory(creature->GetGUID());
					return true;
				}else{
					{
						player->PrepareQuestMenu(creature->GetGUID());
						player->SendPreparedQuest(creature->GetGUID());
					}
					player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_BLUE SERVICES_GOSSIP, GOSSIP_SENDER_MAIN, SERVICES_MENU);
					player->ADD_GOSSIP_ITEM(SWORDS_ICON, TEXT_CUSTOM_GREEN SERVICES_GOSSIP2, GOSSIP_SENDER_MAIN, SERVICES_MENU2);
					player->ADD_GOSSIP_ITEM(VENDOR_ICON, TEXT_CUSTOM_BROWN MY_SHOP_GOSSIP, GOSSIP_SENDER_MAIN, MY_SHOP_MENU);
					/*	if (player->HasItemCount( VIP_TOKEN, 1, false ))
					{*/
					player->ADD_GOSSIP_ITEM(5, Characters_Option_Gossip, GOSSIP_SENDER_MAIN, VIP_MENU);
					//	}
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, TEXT_CUSTOM_RED GO_BACK_GOSSIP2, GOSSIP_SENDER_MAIN, PLAYER_MENU2);
					player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				}
				player->PlayerTalkClass->ClearMenus();

				if (player->GetZoneId() == 3820) //
				{
					player->CLOSE_GOSSIP_MENU();
					ChatHandler(player->GetSession()).PSendSysMessage(No_ZONE);
					player->GetSession()->SendListInventory(creature->GetGUID());
					return true;
				}else{
					////////////////////MENU/////////////////////
					{
						player->PrepareQuestMenu(creature->GetGUID());
						player->SendPreparedQuest(creature->GetGUID());
					}
					player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_BLUE SERVICES_GOSSIP, GOSSIP_SENDER_MAIN, SERVICES_MENU);
					player->ADD_GOSSIP_ITEM(SWORDS_ICON, TEXT_CUSTOM_GREEN SERVICES_GOSSIP2, GOSSIP_SENDER_MAIN, SERVICES_MENU2);
					player->ADD_GOSSIP_ITEM(VENDOR_ICON, TEXT_CUSTOM_BROWN MY_SHOP_GOSSIP, GOSSIP_SENDER_MAIN, MY_SHOP_MENU);
					//	if (player->HasItemCount( VIP_TOKEN, 1, false )){
					player->ADD_GOSSIP_ITEM(5, Characters_Option_Gossip, GOSSIP_SENDER_MAIN, VIP_MENU);
					//}
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, TEXT_CUSTOM_RED GO_BACK_GOSSIP2, GOSSIP_SENDER_MAIN, PLAYER_MENU2);
					player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				}
			}
		}
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 Sender, uint32 Actions)
	{
		player->PlayerTalkClass->ClearMenus();

		if (player->IsInCombat())
		{

			player->CLOSE_GOSSIP_MENU();
			creature->MonsterWhisper(IN_COMBAT, player->GetGUID());

			return true;
		}

		if(Sender == GOSSIP_SENDER_MAIN)
		{
			std::string name;
			switch(Actions)
			{
				break;
				// Reputation
		/*	case REP_MENU: //Unused
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, PLAYER_MENU);
				player->ADD_GOSSIP_ITEM(TRAINER_ICON,MYREP_GOSSIP, GOSSIP_SENDER_MAIN, MYREP_MENU);
				player->ADD_GOSSIP_ITEM(TRAINER_ICON,MYREP_GOSSIP2, GOSSIP_SENDER_MAIN, MYREP_MENU2);
				player->ADD_GOSSIP_ITEM(TRAINER_ICON,MYREP_GOSSIP3, GOSSIP_SENDER_MAIN, MYREP_MENU3);
				player->ADD_GOSSIP_ITEM(TRAINER_ICON,MYREP_GOSSIP4, GOSSIP_SENDER_MAIN, MYREP_MENU4);
				//player->ADD_GOSSIP_ITEM(TRAINER_ICON, TEXT_CUSTOM_PURPLE MYREP_GOSSIP5, GOSSIP_SENDER_MAIN, MYREP_MENU5);
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, TEXT_CUSTOM_RED NVM_GOSSIP, GOSSIP_SENDER_MAIN, NVM_EXIT);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;*/
				//OPTIONS MENU//
				///////////////////////////////////////////MENU DES OPTIONS JOUEUR///////////////////////////////////////////////////	
			case SERVICES_MENU2:
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, PLAYER_MENU);
				//player->ADD_GOSSIP_ITEM(GEAR_ICON, "[Heal Spell] (1 min CD)", GOSSIP_SENDER_MAIN, HEAL_MENU); //Unused
				//player->ADD_GOSSIP_ITEM(GEAR_ICON, "[Buff]", GOSSIP_SENDER_MAIN, BUFF_MENU); //Unused
				player->ADD_GOSSIP_ITEM(GEAR_ICON, "[The Titles]", GOSSIP_SENDER_MAIN, TITRE_MENU);
				player->ADD_GOSSIP_ITEM(TRAINER_ICON, "[Spells & Talents]", GOSSIP_SENDER_MAIN, MY_TRAINER);
				//player->ADD_GOSSIP_ITEM(SWORDS_ICON, TEXT_CUSTOM_GREEN REP_GOSSIP, GOSSIP_SENDER_MAIN, REP_MENU); //Unused
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, TEXT_CUSTOM_RED NVM_GOSSIP, GOSSIP_SENDER_MAIN, NVM_EXIT);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;
				////////////////////////////////////////////////////////////DOUBLE SPE ET RESET TALENT///////////////////////////////		
			case MY_TRAINER:
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, SERVICES_MENU2);
				player->ADD_GOSSIP_ITEM(TRAINER_ICON,  MY_TRAINER_GOSSIP, GOSSIP_SENDER_MAIN, MY_TRAINER_MENU);
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, TEXT_CUSTOM_RED NVM_GOSSIP, GOSSIP_SENDER_MAIN, NVM_EXIT);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;
				/////////////////////////////////////////////////////////////TELEPORT CAPITALE ET MENU SHOP/////////////////////////////			
				//Teleportation H/A
			case SERVICES_MENU:
				if(player->GetTeam() == ALLIANCE){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, PLAYER_MENU);
					player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_CYAN CITIES_GOSSIP, GOSSIP_SENDER_MAIN, A_CITIES_MENU);
					player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_GREEN CUSTOM_AREAS_GOSSIP, GOSSIP_SENDER_MAIN, CUSTOM_AREAS_MENU);
					player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_PURPLE MY_SHOP_PORT4_GOSSIP, GOSSIP_SENDER_MAIN, MY_SHOP_PORT4);
					player->ADD_GOSSIP_ITEM(FLIGHT_ICON, FFA, GOSSIP_SENDER_MAIN, ZONE_FFA);
					//player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_PURPLE MY_SHOP_GOSSIP2, GOSSIP_SENDER_MAIN, MY_SHOP_MENU2);
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, TEXT_CUSTOM_RED NVM_GOSSIP, GOSSIP_SENDER_MAIN, NVM_EXIT);
					player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				}
				else
				{
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, PLAYER_MENU);
					player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_CYAN CITIES_GOSSIP, GOSSIP_SENDER_MAIN, H_CITIES_MENU);
					player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_GREEN CUSTOM_AREAS_GOSSIP, GOSSIP_SENDER_MAIN, CUSTOM_AREAS_MENU);
					player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_PURPLE MY_SHOP_PORT4_GOSSIP, GOSSIP_SENDER_MAIN, MY_SHOP_PORT4);
					player->ADD_GOSSIP_ITEM(FLIGHT_ICON, FFA, GOSSIP_SENDER_MAIN, ZONE_FFA);
					//player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_PURPLE MY_SHOP_GOSSIP2, GOSSIP_SENDER_MAIN, MY_SHOP_MENU2);
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, TEXT_CUSTOM_RED NVM_GOSSIP, GOSSIP_SENDER_MAIN, NVM_EXIT);
					player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				}
				break;
				////////////////////////////////////////TP Area shop/////////////////////////////////////////
			case CUSTOM_AREAS_MENU: 
				//player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, SERVICES_MENU);
				player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_BLUE ">>" , GOSSIP_SENDER_MAIN, CUSTOM_LOCATION_PORT);
				//player->ADD_GOSSIP_ITEM_EXTENDED(FLIGHT_ICON, TEXT_CUSTOM_BLUE CUSTOM_LOCATION_GOSSIP, GOSSIP_SENDER_MAIN, CUSTOM_LOCATION_PORT,"Etes vous sur de vouloir etre teleporter?",0,false);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;
				/////////////////////////////////////////////////////////Tele Ally/////////////////////////////////////////////////   
			case A_CITIES_MENU:
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, SERVICES_MENU);
				player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_BLUE DALARAN_GOSSIP, GOSSIP_SENDER_MAIN, DALARAN_PORT);
				player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_BLUE DARNASSUS_GOSSIP, GOSSIP_SENDER_MAIN, DARNASSUS_PORT);
				player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_BLUE IRONFORGE_GOSSIP, GOSSIP_SENDER_MAIN, IRONFORGE_PORT);
				player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_BLUE SHATTRATH_CITY_GOSSIP, GOSSIP_SENDER_MAIN, SHATTRATH_CITY_PORT);
				player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_BLUE STORMWIND_GOSSIP, GOSSIP_SENDER_MAIN, STORMWIND_PORT);
				player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_BLUE THE_EXODAR_GOSSIP, GOSSIP_SENDER_MAIN, THE_EXODAR_PORT);
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, TEXT_CUSTOM_RED NVM_GOSSIP, GOSSIP_SENDER_MAIN, NVM_EXIT);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;
				////////////////////////////////////////////////////Tele Horde/////////////////////////////////////////////////////
			case H_CITIES_MENU:
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, SERVICES_MENU);
				player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_ORANGE DALARAN_GOSSIP, GOSSIP_SENDER_MAIN, DALARAN_PORT);
				player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_ORANGE ORGRIMMAR_GOSSIP, GOSSIP_SENDER_MAIN, ORGRIMMAR_PORT);
				player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_ORANGE SHATTRATH_CITY_GOSSIP, GOSSIP_SENDER_MAIN, SHATTRATH_CITY_PORT);
				player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_ORANGE SILVERMOON_CITY_GOSSIP, GOSSIP_SENDER_MAIN, SILVERMOON_CITY_PORT);
				player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_ORANGE THUNDER_BLUFF_GOSSIP, GOSSIP_SENDER_MAIN, THUNDER_BLUFF_PORT);
				player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_ORANGE UNDERCITY_GOSSIP, GOSSIP_SENDER_MAIN, UNDERCITY_PORT);
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, TEXT_CUSTOM_RED NVM_GOSSIP, GOSSIP_SENDER_MAIN, NVM_EXIT);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;
				////////////////////////////////////////////Tele Raccourcis///////////////////////////////////////////////////////	
			case MY_SHOP_MENU2:
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, SERVICES_MENU);
		/*		player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_ORANGE MY_SHOP_PORT_GOSSIP, GOSSIP_SENDER_MAIN, MY_SHOP_PORT);
				player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_ORANGE MY_SHOP_PORT2_GOSSIP, GOSSIP_SENDER_MAIN, MY_SHOP_PORT2);
				player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_PURPLE MY_SHOP_PORT3_GOSSIP, GOSSIP_SENDER_MAIN, MY_SHOP_PORT3);
				player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_PURPLE MY_SHOP_PORT5_GOSSIP, GOSSIP_SENDER_MAIN, MY_SHOP_PORT5);
				player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_PURPLE MY_SHOP_PORT6_GOSSIP, GOSSIP_SENDER_MAIN, MY_SHOP_PORT6);
				player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_PURPLE MY_SHOP_PORT7_GOSSIP, GOSSIP_SENDER_MAIN, MY_SHOP_PORT7);*/
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, TEXT_CUSTOM_RED NVM_GOSSIP, GOSSIP_SENDER_MAIN, NVM_EXIT);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;
				/////////////////////////////////////////////////////Retour Menu 1//////////////////////////////////////////////////	
			case PLAYER_MENU:
				{
					player->PrepareQuestMenu(creature->GetGUID());
					player->SendPreparedQuest(creature->GetGUID());
				}
				player->ADD_GOSSIP_ITEM(FLIGHT_ICON, TEXT_CUSTOM_BLUE SERVICES_GOSSIP, GOSSIP_SENDER_MAIN, SERVICES_MENU);
				//player->ADD_GOSSIP_ITEM(TRAINER_ICON, TEXT_CUSTOM_GREEN REP_GOSSIP, GOSSIP_SENDER_MAIN, REP_MENU);
				player->ADD_GOSSIP_ITEM(SWORDS_ICON, TEXT_CUSTOM_GREEN SERVICES_GOSSIP2, GOSSIP_SENDER_MAIN, SERVICES_MENU2);
				player->ADD_GOSSIP_ITEM(VENDOR_ICON, TEXT_CUSTOM_BROWN MY_SHOP_GOSSIP, GOSSIP_SENDER_MAIN, MY_SHOP_MENU);
				player->ADD_GOSSIP_ITEM(5, Characters_Option_Gossip, GOSSIP_SENDER_MAIN, VIP_MENU);
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, TEXT_CUSTOM_RED GO_BACK_GOSSIP2, GOSSIP_SENDER_MAIN, PLAYER_MENU2);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;
				//////////////////BETA///////////////////////REPUTATION///////////////////BETA///////////////////////////////  
			case MYREP_MENU:
				player->SetReputation(1106,429999);//Argent Crusade
				player->SetReputation(529,429999);//Argent Dawn
				player->SetReputation(1012,429999);//Ashtongue Deathsworn
				player->SetReputation(21,429999);//Booty Bay
				player->SetReputation(910,429999);//Brood of Nozdormu
				player->SetReputation(609,429999);//Cenarion Circle
				player->SetReputation(87,429999);//Bloodsail Buccaneers
				player->SetReputation(942,429999);//Cenarion Expedition
				player->SetReputation(909,429999);//Darkmoon Faire
				player->SetReputation(577,429999);//Everlook
				player->SetReputation(1104,429999);//Frenzyheart Tribe
				player->SetReputation(909,429999);//Darkmoon Faire
				player->SetReputation(369,429999);//Gadgetzan
				player->SetReputation(92,429999);//Gelkis Clan Centaur
				player->SetReputation(749,429999);//Hydraxian Waterlords
				player->SetReputation(989,429999);//Keepers of Time
				player->SetReputation(1090,429999);//Kirin Tor
				player->SetReputation(1098,429999);//Knights of the Ebon Blade
				player->SetReputation(1011,429999);//Lower City
				player->SetReputation(93,429999);//Magram Clan Centaur
				player->SetReputation(1015,429999);//Netherwing
				player->SetReputation(1038,429999);//Ogri'la
				player->SetReputation(470,429999);//Ratchet
				player->SetReputation(349,429999);//Ravenholdt
				player->SetReputation(1031,429999);//Sha'tari Skyguard
				player->SetReputation(1077,429999);//Shattered Sun Offensive
				player->SetReputation(809,429999);//Shen'dralar
				player->SetReputation(970,429999);//Sporeggar
				player->SetReputation(70,429999);//Syndicate
				player->SetReputation(932,429999);//The Aldor
				player->SetReputation(1073,429999);//The Kalu'ak 
				player->SaveToDB();
				break;

			case MYREP_MENU2:
				//player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, SERVICES_MENU);
				player->learnDefaultSpells();
				player->CLOSE_GOSSIP_MENU();
				/*player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, TEXT_CUSTOM_RED GO_BACK_GOSSIP2, GOSSIP_SENDER_MAIN, PLAYER_MENU2);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;

				case MYREP_MENU3:
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, SERVICES_MENU);
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, TEXT_CUSTOM_RED GO_BACK_GOSSIP2, GOSSIP_SENDER_MAIN, PLAYER_MENU2);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;*/

			case MYREP_MENU4:
				if(player->GetTeam() == ALLIANCE){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, PLAYER_MENU);
					player->SetReputation(69,429999);//Darnassus
					player->SetReputation(930,429999);//Exodar
					player->SetReputation(54,429999);//gnomeregan
					player->SetReputation(47,429999);//Forgefer
					player->SetReputation(72,429999);//Hurlevent
					/*player->SetReputation(609,429999);//Cenarion Circle
					player->SetReputation(87,429999);//Bloodsail Buccaneers
					player->SetReputation(942,429999);//Cenarion Expedition*/
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, TEXT_CUSTOM_RED NVM_GOSSIP, GOSSIP_SENDER_MAIN, NVM_EXIT);
					player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				}
				else
				{
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, PLAYER_MENU);
					player->SetReputation(530,429999);//trolls sombrelance
					player->SetReputation(76,429999);//orgrimmar
					player->SetReputation(911,429999);//Lune-d'argent
					player->SetReputation(81,429999);//Les pitons du tonerre
					player->SetReputation(68,429999);//fossoyeuse
					/*player->SetReputation(609,429999);//Cenarion Circle
					player->SetReputation(87,429999);//Bloodsail Buccaneers
					player->SetReputation(942,429999);//Cenarion Expedition*/
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, TEXT_CUSTOM_RED NVM_GOSSIP, GOSSIP_SENDER_MAIN, NVM_EXIT);
					player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				}
				break;
				//////////////////////////////////////////////OPTION////////////////////////////////////////////////////////				
				///////////////HEAL////////////
			case HEAL_MENU:
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, SERVICES_MENU2);
				if (player->HasAura(SPELL_ANTI_HEAL_DEBUFF))
				{
					player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
					creature->MonsterWhisper(EMOTE_COOLDOWN,player->GetGUID());
				}
				else
				{
					player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
					player->CastSpell(player, SPELL_FULL_HEAL, true);
					player->CastSpell(player, SPELL_ANTI_HEAL_DEBUFF, true);
				}
				//player->RegenerateAll();
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;
				/////////////TRAINER/////////////
				/////////RESET/////////
			case MY_TRAINER_MENU:
				player->ResetTalents(true);
				player->SendTalentsInfoData(false);
				player->MonsterTextEmote("Your talents has been reset!", 0, true);
				player->SaveToDB();
				player->PlayerTalkClass->SendCloseGossip();
				break;

				//////////////////////////////////////////////////////////BUFF MENU///////////////////////////////////////////////////	
			case BUFF_MENU:
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, SERVICES_MENU2);
				player->ADD_GOSSIP_ITEM(GEAR_ICON, TEXT_CUSTOM_RED "Full", GOSSIP_SENDER_MAIN, 505);
				player->ADD_GOSSIP_ITEM(GEAR_ICON, "Benedictions des Rois", GOSSIP_SENDER_MAIN, ACTION_GREATER_BLESSING_OF_KINGS);
				player->ADD_GOSSIP_ITEM(GEAR_ICON, "Benedictions de Puissances", GOSSIP_SENDER_MAIN, ACTION_GREATER_BLESSING_OF_MIGHT);
				player->ADD_GOSSIP_ITEM(GEAR_ICON, "Benedictions de Sagesses", GOSSIP_SENDER_MAIN, ACTION_GREATER_BLESSING_OF_WISDOM);
				player->ADD_GOSSIP_ITEM(GEAR_ICON, "Benedictions du Sanctuaire", GOSSIP_SENDER_MAIN, ACTION_GREATER_BLESSING_OF_SANCTUARY);
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, NVM_GOSSIP, GOSSIP_SENDER_MAIN, NVM_EXIT);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;
				/////////////////////////////////////////////////////ACTION/////////////////////////////////////
			case 505:
				player->CastSpell(player, SPELL_POWER_WORD_FORTITUDE, true),
					player->CastSpell(player, SPELL_PRAYER_OF_SPIRIT, true),
					player->CastSpell(player, SPELL_SHADOW_PROTECTION, true),
					player->CastSpell(player, SPELL_ARCANE_INTELLECT, true),
					player->CastSpell(player, SPELL_DAMPEN_MAGIC, true),
					player->CastSpell(player, SPELL_AMPLIFY_MAGIC, true),
					player->CastSpell(player, SPELL_GIFT_OF_THE_WILD, true),
					player->CastSpell(player, SPELL_THORNS, true),
					player->CastSpell(player, SPELL_WATER_BREATHING, true);
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, SERVICES_MENU2);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;

			case ACTION_GREATER_BLESSING_OF_KINGS:
				player->CastSpell(player, SPELL_GREATER_BLESSING_OF_KINGS, true);
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, SERVICES_MENU2);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;

			case ACTION_GREATER_BLESSING_OF_MIGHT:
				player->CastSpell(player, SPELL_GREATER_BLESSING_OF_MIGHT, true);
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, SERVICES_MENU2);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;

			case ACTION_GREATER_BLESSING_OF_WISDOM:
				player->CastSpell(player, SPELL_GREATER_BLESSING_OF_WISDOM, true);
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, SERVICES_MENU2);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;

			case ACTION_GREATER_BLESSING_OF_SANCTUARY:
				player->CastSpell(player, SPELL_GREATER_BLESSING_OF_SANCTUARY, true);
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, SERVICES_MENU2);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;
				////////////////////////////////////////////////////////////TELEPORTATION////////////////////////////////////////////////////////////	
			case CUSTOM_LOCATION_PORT: //Area shop
				player->TeleportTo(0, -10522.872070f, -461.047119f, 47.179840f, 0.877093f);
				creature->DespawnOrUnsummon(0);
				player->PlayerTalkClass->SendCloseGossip();
				break;
				
			case MY_SHOP_PORT4://Area Duels
				player->TeleportTo(0, -5267.946777f, -1445.919678f, 513.261414f, 5.88716f);
				creature->DespawnOrUnsummon(0);
				player->PlayerTalkClass->SendCloseGossip();
				break;

			case ZONE_FFA: // FFA Area
				switch (urand(2000 , 2004))
				{
				case 2000:
					player->CastSpell(player,32943);
					player->TeleportTo(0, 2679.245117f, -5310.412109f, 154.188019f, 5.342051f);
					creature->DespawnOrUnsummon(0);
					player->PlayerTalkClass->SendCloseGossip();
					return false;
					break;

				case 2001:	
					player->CastSpell(player,32943);
					player->TeleportTo(0, 2844.855469f, -5382.154297f, 168.586639f, 3.527779f);
					creature->DespawnOrUnsummon(0);
					player->PlayerTalkClass->SendCloseGossip();
					return false;
					break;

				case 2002:
					player->CastSpell(player,32943);
					player->TeleportTo(0, 2749.658691f, -5511.711426f, 166.427567f, 2.330049f);
					creature->DespawnOrUnsummon(0);
					player->PlayerTalkClass->SendCloseGossip();
					return false;
					break;

				case 2003:
					player->CastSpell(player,32943);
					player->TeleportTo(0, 2710.634521f, -5419.008789f, 159.359879f, 2.546025f);
					creature->DespawnOrUnsummon(0);
					player->PlayerTalkClass->SendCloseGossip();
					return false;
					break;

				case 2004:
					player->CastSpell(player,32943);
					player->TeleportTo(0, 2614.830811f, -5375.675781f, 175.868729f, 5.491267f);
					creature->DespawnOrUnsummon(0);
					player->PlayerTalkClass->SendCloseGossip();
					return false;
					break;
				}
				//------------------------------------------------------------------------------//
			case DALARAN_PORT:
				player->TeleportTo(571, 5804.149902f, 624.770996f,648.747009f, 1.640000f);
				creature->DespawnOrUnsummon(0);
				player->PlayerTalkClass->SendCloseGossip();
				break;

			case SHATTRATH_CITY_PORT:
				player->TeleportTo(530, -1833.544312f, 5312.579590f, -9.536835f, 1.349267f);
				creature->DespawnOrUnsummon(0);
				player->PlayerTalkClass->SendCloseGossip();
				break;

			case DARNASSUS_PORT:
				player->TeleportTo(1, 9949.559570f, 2284.20996f, 1342.969482f, 1.595870f);
				creature->DespawnOrUnsummon(0);
				player->PlayerTalkClass->SendCloseGossip();
				break;

			case IRONFORGE_PORT:
				player->TeleportTo(0, -4918.879883f, -940.406006f, 504.854126f, 5.423470f);
				creature->DespawnOrUnsummon(0);
				player->PlayerTalkClass->SendCloseGossip();
				break;

			case STORMWIND_PORT:
				player->TeleportTo(0, -8833.379883f, 628.627991f, 95.826599f, 1.065350f);
				creature->DespawnOrUnsummon(0);
				player->PlayerTalkClass->SendCloseGossip();
				break;

			case THE_EXODAR_PORT:
				player->TeleportTo(530, -3965.699951f, -11653.599609f, -137.184998f, 0.852154f);
				creature->DespawnOrUnsummon(0);
				player->PlayerTalkClass->SendCloseGossip();
				break;

			case ORGRIMMAR_PORT:
				player->TeleportTo(1, 1629.359985f, -4373.390137f, 33.097401f, 3.548390f);
				creature->DespawnOrUnsummon(0);
				player->PlayerTalkClass->SendCloseGossip();
				break;

			case SILVERMOON_CITY_PORT:
				player->TeleportTo(530, 9482.883789f, -7278.637207f, 18.480778f, 6.040224f);
				creature->DespawnOrUnsummon(0);
				player->PlayerTalkClass->SendCloseGossip();
				break;

			case THUNDER_BLUFF_PORT:
				player->TeleportTo(1, -1277.369995f, 124.804001f, 134.094009f, 5.222740f);
				creature->DespawnOrUnsummon(0);
				player->PlayerTalkClass->SendCloseGossip();
				break;

			case UNDERCITY_PORT:
				player->TeleportTo(0, 1584.069946f, 241.987000f, -51.075413f, 1.296860f);
				creature->DespawnOrUnsummon(0);
				player->PlayerTalkClass->SendCloseGossip();
				break;
/* UNUSED
				case MY_SHOP_PORT: //glyphe
				player->TeleportTo(0, -10481.900391f, -425.226654f, 45.024639f, 1.757160f);
				creature->DespawnOrUnsummon(0);
				player->PlayerTalkClass->SendCloseGossip();
				break;

				case MY_SHOP_PORT2://Armures & armes
				player->TeleportTo(0, -10542.234375f, -438.107605f, 53.746941f, 1.680976f);
				creature->DespawnOrUnsummon(0);
				player->PlayerTalkClass->SendCloseGossip();
				break;

				case MY_SHOP_PORT3://Monture & pets chasseurs
				player->TeleportTo(0, -10524.994141f, -438.468109f, 50.711845f, 4.994551f);
				creature->DespawnOrUnsummon(0);
				player->PlayerTalkClass->SendCloseGossip();
				
				case MY_SHOP_PORT5:
				player->TeleportTo(0, -10521.006836f, -432.751709f, 50.037071f, 1.703752f);
				creature->DespawnOrUnsummon(0);
				player->PlayerTalkClass->SendCloseGossip();
				break;

			case MY_SHOP_PORT6:
				player->TeleportTo(0, -10533.506836f, -436.990540f, 52.033730f, 1.434280f);
				creature->DespawnOrUnsummon(0);
				player->PlayerTalkClass->SendCloseGossip();
				break;

			case MY_SHOP_PORT7:
				player->TeleportTo(0, -10533.506836f, -436.990540f, 52.033730f, 1.434280f);
				creature->DespawnOrUnsummon(0);
				player->PlayerTalkClass->SendCloseGossip();
				break;
				break;
				*/
				///////////////////////////////////////////////RETOUR ET AUTRES TRUCKS DU GENRES///////////////////////////////	
				////////EXIT
			case NVM_EXIT:
				player->PlayerTalkClass->SendCloseGossip();
				break;
				/////////////////////////////////SAVE/////////////////////////////////////
			case SAVE_MENU:
				player->CLOSE_GOSSIP_MENU();
				player->SaveToDB();
				creature->MonsterWhisper(MESSAGE_CHARACTER_SAVE_TO_DB, player->GetGUID());
				break;
				////////////////////////////////SHOP/////////////////////////////////////			
			case MY_SHOP_MENU:
				player->GetSession()->SendListInventory(creature->GetGUID());
				break;	
				///////////////////////////////////////////////////////TITRE/////////////////////////////////////////////////
				////////////////////MENU
			case TITRE_MENU:
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, SERVICES_MENU2);
				player->ADD_GOSSIP_ITEM( 3, "General (15 Token)", GOSSIP_SENDER_MAIN, 300);
				player->ADD_GOSSIP_ITEM( 3, "Cities (20 Token)", GOSSIP_SENDER_MAIN, 307);
				player->ADD_GOSSIP_ITEM( 8, "Event (30 Token)", GOSSIP_SENDER_MAIN, 301);
				player->ADD_GOSSIP_ITEM( 1, "Dungeons & Raids (65 Token)", GOSSIP_SENDER_MAIN, 302);
				player->ADD_GOSSIP_ITEM( 6, "Reputations (70 Token)", GOSSIP_SENDER_MAIN, 303);
				player->ADD_GOSSIP_ITEM( 9, "PvP (80 Token)", GOSSIP_SENDER_MAIN, 304);
				player->ADD_GOSSIP_ITEM( 10, "Others (100 Token)", GOSSIP_SENDER_MAIN, 305);
				player->ADD_GOSSIP_ITEM( 5, "The Out Of Prices", GOSSIP_SENDER_MAIN, 306);
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, NVM_GOSSIP, GOSSIP_SENDER_MAIN, NVM_EXIT);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;	
				
			case 300: //General
				name = player->GetName();
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
				player->ADD_GOSSIP_ITEM( 1,name+ " the Explorer"   				    , GOSSIP_SENDER_MAIN, 308);
				player->ADD_GOSSIP_ITEM( 1,"Chef "+name , GOSSIP_SENDER_MAIN, 309);
				player->ADD_GOSSIP_ITEM( 1,"Salty "+name , GOSSIP_SENDER_MAIN, 310);
				player->ADD_GOSSIP_ITEM( 1,"Loremaster " +name , GOSSIP_SENDER_MAIN, 311);
				player->ADD_GOSSIP_ITEM( 1,name+ " The Seeker"                        	, GOSSIP_SENDER_MAIN, 312);
				player->ADD_GOSSIP_ITEM( 1,name+ " The Patient"                     	, GOSSIP_SENDER_MAIN, 313);
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, NVM_GOSSIP, GOSSIP_SENDER_MAIN, NVM_EXIT);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;
				
			case 307:
				name = player->GetName();
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
				if(player->GetTeam() == HORDE)
				{
				player->ADD_GOSSIP_ITEM( 1,name+ " of Orgrimmar"   				    , GOSSIP_SENDER_MAIN, 371);
				player->ADD_GOSSIP_ITEM( 1,name+ " of Sen'jin"   				    , GOSSIP_SENDER_MAIN, 372);
				player->ADD_GOSSIP_ITEM( 1,name+ " of Silvermoon"   				    , GOSSIP_SENDER_MAIN, 373);
				player->ADD_GOSSIP_ITEM( 1,name+ " of Thunder Bluff"   				    , GOSSIP_SENDER_MAIN, 374);
				player->ADD_GOSSIP_ITEM( 1,name+ " of the Undercity"   				    , GOSSIP_SENDER_MAIN, 375);
				}
				else
				{
				player->ADD_GOSSIP_ITEM( 1,name+ " of the Exodar"   				    , GOSSIP_SENDER_MAIN, 366);
				player->ADD_GOSSIP_ITEM( 1,name+ " of Darnassus"   				    , GOSSIP_SENDER_MAIN, 367);
				player->ADD_GOSSIP_ITEM( 1,name+ " of Ironforge"   				    , GOSSIP_SENDER_MAIN, 368);
				player->ADD_GOSSIP_ITEM( 1,name+ " of Stormwind"   				    , GOSSIP_SENDER_MAIN, 369);
				player->ADD_GOSSIP_ITEM( 1,name+ " of Gnomeregan"   				    , GOSSIP_SENDER_MAIN, 370);
				}
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, NVM_GOSSIP, GOSSIP_SENDER_MAIN, NVM_EXIT);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
			break;

			case 301: //Event
				name = player->GetName();
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
				player->ADD_GOSSIP_ITEM( 1, "Elder " +name	, GOSSIP_SENDER_MAIN, 315);
				player->ADD_GOSSIP_ITEM( 1,name+" The Love Fool" , GOSSIP_SENDER_MAIN, 316);
				player->ADD_GOSSIP_ITEM( 1,name+ " The Noble"                         , GOSSIP_SENDER_MAIN, 317);
				player->ADD_GOSSIP_ITEM( 1,"Brewmaster " +name  , GOSSIP_SENDER_MAIN, 318);
				player->ADD_GOSSIP_ITEM( 1,name+ " the Hallowed"                      , GOSSIP_SENDER_MAIN, 319);
				player->ADD_GOSSIP_ITEM( 1,name+ " The Pilgrim"       , GOSSIP_SENDER_MAIN, 320);
				player->ADD_GOSSIP_ITEM( 1,"Merrymaker " +name                        , GOSSIP_SENDER_MAIN, 321);
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, NVM_GOSSIP, GOSSIP_SENDER_MAIN, NVM_EXIT);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;

			case 302: //Dungeon & Raid
				name = player->GetName();
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
				player->ADD_GOSSIP_ITEM( 1, name+ " Jenkins"      						, GOSSIP_SENDER_MAIN, 322);
				player->ADD_GOSSIP_ITEM( 1, "Scarab Lord " +name                      , GOSSIP_SENDER_MAIN, 323);
				player->ADD_GOSSIP_ITEM( 1, name +", Champion of the Frozen Wastes"   	, GOSSIP_SENDER_MAIN, 324);
				player->ADD_GOSSIP_ITEM( 1,name+ " the Undying"                       , GOSSIP_SENDER_MAIN, 325);
				player->ADD_GOSSIP_ITEM( 1,name+ " the Immortal"                      , GOSSIP_SENDER_MAIN, 326);
				player->ADD_GOSSIP_ITEM( 1,name+ " the Kingslayer"                  , GOSSIP_SENDER_MAIN, 327);
				player->ADD_GOSSIP_ITEM( 1, "Twilight Vanquisher " +name             , GOSSIP_SENDER_MAIN, 328);
				player->ADD_GOSSIP_ITEM( 1, "Starcaller "  +name                      , GOSSIP_SENDER_MAIN, 329);
				player->ADD_GOSSIP_ITEM( 1, name+" the Astral Walker"                 , GOSSIP_SENDER_MAIN, 330);
				player->ADD_GOSSIP_ITEM( 1, name+", Herald of the Titans"              , GOSSIP_SENDER_MAIN, 331);
				player->ADD_GOSSIP_ITEM( 1, name+", Champion of Ulduar"                , GOSSIP_SENDER_MAIN, 332);
				player->ADD_GOSSIP_ITEM( 1, name+", Conqueror of Ulduar"               , GOSSIP_SENDER_MAIN, 333);
				player->ADD_GOSSIP_ITEM( 1, name+", Bane of the Fallen King"           , GOSSIP_SENDER_MAIN, 334);
				player->ADD_GOSSIP_ITEM( 1, name+" the Light of Dawn"                 , GOSSIP_SENDER_MAIN, 335);
				player->ADD_GOSSIP_ITEM( 1, name+" the Kingslayer"                    , GOSSIP_SENDER_MAIN, 336);
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, NVM_GOSSIP, GOSSIP_SENDER_MAIN, NVM_EXIT);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;

			case 303: //Reputation
				name = player->GetName();
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
				player->ADD_GOSSIP_ITEM( 1, name+" of the Shattered Sun"              , GOSSIP_SENDER_MAIN, 337);
				player->ADD_GOSSIP_ITEM( 1, "Ambassador " +name                       , GOSSIP_SENDER_MAIN, 338);
				player->ADD_GOSSIP_ITEM( 1, "Bloodsail Admiral "+name       			, GOSSIP_SENDER_MAIN, 339);
				player->ADD_GOSSIP_ITEM( 1, name+ " the Diplomat"                      , GOSSIP_SENDER_MAIN, 340);
				player->ADD_GOSSIP_ITEM( 1, name+", Guardian of Cenarius"              , GOSSIP_SENDER_MAIN, 341);
				player->ADD_GOSSIP_ITEM( 1, name+", Herald of the Titans"               , GOSSIP_SENDER_MAIN, 342);
				player->ADD_GOSSIP_ITEM( 1, name+" of the Ashen Verdict"              , GOSSIP_SENDER_MAIN, 343);
				player->ADD_GOSSIP_ITEM( 1, name+" the Exalted"                       , GOSSIP_SENDER_MAIN, 344);
				player->ADD_GOSSIP_ITEM( 1, name+" the Insane"                        , GOSSIP_SENDER_MAIN, 345);
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, NVM_GOSSIP, GOSSIP_SENDER_MAIN, NVM_EXIT);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;

			case 304: //PvP & Arena
				name = player->GetName();
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
				player->ADD_GOSSIP_ITEM( 1, "Battlemaster " +name             , GOSSIP_SENDER_MAIN, 346);
				/*	player->ADD_GOSSIP_ITEM( 0, "Maitre de l'arene"              , GOSSIP_SENDER_MAIN, 347);
				player->ADD_GOSSIP_ITEM( 0, "Duelliste"                   , GOSSIP_SENDER_MAIN, 348);
				player->ADD_GOSSIP_ITEM( 0, "Rival"                     , GOSSIP_SENDER_MAIN, 349);
				player->ADD_GOSSIP_ITEM( 0, "Competiteur"                , GOSSIP_SENDER_MAIN, 350);
				player->ADD_GOSSIP_ITEM( 0, "Vainqueur"                , GOSSIP_SENDER_MAIN, 351);*/
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, NVM_GOSSIP, GOSSIP_SENDER_MAIN, NVM_EXIT);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;

			case 305: //Other
				name = player->GetName();
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
				player->ADD_GOSSIP_ITEM( 1, "Crusader " +name                          , GOSSIP_SENDER_MAIN, 352);
				player->ADD_GOSSIP_ITEM( 1, "Archmage " +name                         , GOSSIP_SENDER_MAIN, 353);
				player->ADD_GOSSIP_ITEM( 1, name+", Champion of the Naaru"             , GOSSIP_SENDER_MAIN, 354);
				player->ADD_GOSSIP_ITEM( 1, name+", Hand of A'dal"                     , GOSSIP_SENDER_MAIN, 355);
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, NVM_GOSSIP, GOSSIP_SENDER_MAIN, NVM_EXIT);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;

			case 306: //Les hors de prix
				name = player->GetName();
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
				player->ADD_GOSSIP_ITEM( 1, name+" the Malefic (500 Token)", GOSSIP_SENDER_MAIN, 357);
				player->ADD_GOSSIP_ITEM( 1, name+" the Magic Seeker (500 Token)", GOSSIP_SENDER_MAIN, 358);
				player->ADD_GOSSIP_ITEM( 1, "Prophet (600 Token) "+name, GOSSIP_SENDER_MAIN, 359);
				player->ADD_GOSSIP_ITEM( 1, "Assassin (650 Token) "+name , GOSSIP_SENDER_MAIN, 360);
				player->ADD_GOSSIP_ITEM( 1, name+ ", Death's Demise(700 Token)", GOSSIP_SENDER_MAIN, 361);
				player->ADD_GOSSIP_ITEM( 1, "Vanquisher (800 Token) "+name, GOSSIP_SENDER_MAIN, 362);
				player->ADD_GOSSIP_ITEM( 1, name+" of the Ten Storms (850 Token)", GOSSIP_SENDER_MAIN, 363);
				player->ADD_GOSSIP_ITEM( 1, name+" of the Emerald Dream (850 Token)", GOSSIP_SENDER_MAIN, 364);
				player->ADD_GOSSIP_ITEM( 1, name+" The Supreme (1000 Token)", GOSSIP_SENDER_MAIN, 365);
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, NVM_GOSSIP, GOSSIP_SENDER_MAIN, NVM_EXIT);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;
				//////////////////////////////////////////////////////////////////////////////////ACTION/////////////////////////////////////////////////////////////////////
				/////////////GENERAL
			case 308://The Explorer
				if (player->HasTitle(sCharTitlesStore.LookupEntry(78))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 15)){
						player->ModifyCurrency(TITLE_TOKEN, -15, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(78));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}else{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 309://Chef
				if (player->HasTitle(sCharTitlesStore.LookupEntry(84))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 15))
					{
						player->ModifyCurrency(TITLE_TOKEN, -15, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(84));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 310://Salty
				if (player->HasTitle(sCharTitlesStore.LookupEntry(83))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 15))
					{
						player->ModifyCurrency(TITLE_TOKEN, -15, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(83));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 311://Loremaster
				if (player->HasTitle(sCharTitlesStore.LookupEntry(125))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 15))
					{
						player->ModifyCurrency(TITLE_TOKEN, -15, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(125));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 312://The Seeker
				if (player->HasTitle(sCharTitlesStore.LookupEntry(81))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 15))
					{
						player->ModifyCurrency(TITLE_TOKEN, -15, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(81));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 313://The Patient
				if (player->HasTitle(sCharTitlesStore.LookupEntry(172))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 15))
					{
						player->ModifyCurrency(TITLE_TOKEN, -15, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(172));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;
				///////////////////////////////////////////////////////////////////////////////ACTION/////////////////////////////////////////////////////////////////////
				/////////////EVENT
			case 315://Elder
				if (player->HasTitle(sCharTitlesStore.LookupEntry(74))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 30))
					{
						player->ModifyCurrency(TITLE_TOKEN, -30, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(74));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(2, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 316://The Love Fool
				if (player->HasTitle(sCharTitlesStore.LookupEntry(135))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 30))
					{
						player->ModifyCurrency(TITLE_TOKEN, -30, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(135));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(2, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 317://The Noble
				if (player->HasTitle(sCharTitlesStore.LookupEntry(155))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 30))
					{
						player->ModifyCurrency(TITLE_TOKEN, -30, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(155));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(2, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 318://Brewmaster
				if (player->HasTitle(sCharTitlesStore.LookupEntry(133))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 30))
					{
						player->ModifyCurrency(TITLE_TOKEN, -30, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(133));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(2, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 319://The Hallowed
				if (player->HasTitle(sCharTitlesStore.LookupEntry(124))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 30))
					{
						player->ModifyCurrency(TITLE_TOKEN, -30, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(124));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(2, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 320://Pilgrim
				if (player->HasTitle(sCharTitlesStore.LookupEntry(168))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 30))
					{
						player->ModifyCurrency(TITLE_TOKEN, -30, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(168));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(2, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 321://Merrymaker
				if (player->HasTitle(sCharTitlesStore.LookupEntry(134))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 30))
					{
						player->ModifyCurrency(TITLE_TOKEN, -30, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(134));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(2, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;
				//////////////////////////////////////////////////////////////////////////////////ACTION/////////////////////////////////////////////////////////////////////
				//////////////// Donjons & raids
			case 322://Jenkins
				if (player->HasTitle(sCharTitlesStore.LookupEntry(143))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 65))
					{
						player->ModifyCurrency(TITLE_TOKEN, -65, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(143));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 323://Seigneur scarabee
				if (player->HasTitle(sCharTitlesStore.LookupEntry(46))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 65))
					{
						player->ModifyCurrency(TITLE_TOKEN, -65, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(46));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 324://champion des terres gelees
				if (player->HasTitle(sCharTitlesStore.LookupEntry(129))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 65))
					{
						player->ModifyCurrency(TITLE_TOKEN, -65, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(129));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 325://l'increvable
				if (player->HasTitle(sCharTitlesStore.LookupEntry(142))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 65))
					{
						player->ModifyCurrency(TITLE_TOKEN, -65, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(142));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 326://l'immortel
				if (player->HasTitle(sCharTitlesStore.LookupEntry(141))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 65))
					{
						player->ModifyCurrency(TITLE_TOKEN, -65, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(141));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 327://du crepuscule
				if (player->HasTitle(sCharTitlesStore.LookupEntry(140))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 65))
					{
						player->ModifyCurrency(TITLE_TOKEN, -65, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(140));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 328://Vainqueur du crepuscule
				if (player->HasTitle(sCharTitlesStore.LookupEntry(121))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 65))
					{
						player->ModifyCurrency(TITLE_TOKEN, -65, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(121));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 329://Mande-etoile
				if (player->HasTitle(sCharTitlesStore.LookupEntry(164))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 65))
					{
						player->ModifyCurrency(TITLE_TOKEN, -65, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(164));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 330://le marcheur astral
				if (player->HasTitle(sCharTitlesStore.LookupEntry(165))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 65))
					{
						player->ModifyCurrency(TITLE_TOKEN, -65, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(165));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 331://heraut des titans
				if (player->HasTitle(sCharTitlesStore.LookupEntry(166))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 65))
					{
						player->ModifyCurrency(TITLE_TOKEN, -65, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(166));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 332://champion d'Ulduar
				if (player->HasTitle(sCharTitlesStore.LookupEntry(161))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 65))
					{
						player->ModifyCurrency(TITLE_TOKEN, -65, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(161));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 333://conquerant d'Ulduar
				if (player->HasTitle(sCharTitlesStore.LookupEntry(160))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 65))
					{
						player->ModifyCurrency(TITLE_TOKEN, -65, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(160));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 334://tourment du roi dechu
				if (player->HasTitle(sCharTitlesStore.LookupEntry(174))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 65))
					{
						player->ModifyCurrency(TITLE_TOKEN, -65, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(174));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 335://lumiere de l'aube
				if (player->HasTitle(sCharTitlesStore.LookupEntry(173))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 65))
					{
						player->ModifyCurrency(TITLE_TOKEN, -65, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(173));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 336://le regicide
				if (player->HasTitle(sCharTitlesStore.LookupEntry(175))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 65))
					{
						player->ModifyCurrency(TITLE_TOKEN, -65, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(175));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;
				////////////////////////////////////////////////////////////////ACTION////////////////////////////////////////////////////////////////////////
				//Reputation
			case 337://du Soleil brise
				if (player->HasTitle(sCharTitlesStore.LookupEntry(63))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 70))
					{
						player->ModifyCurrency(TITLE_TOKEN, -70, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(63));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 338://Ambassadeur
				if (player->HasTitle(sCharTitlesStore.LookupEntry(130))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 70))
					{
						player->ModifyCurrency(TITLE_TOKEN, -70, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(130));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 339://Amiral de la Voile sanglante
				if (player->HasTitle(sCharTitlesStore.LookupEntry(144))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 70))
					{
						player->ModifyCurrency(TITLE_TOKEN, -70, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(144));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 340://le diplomate
				if (player->HasTitle(sCharTitlesStore.LookupEntry(79))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 70))
					{
						player->ModifyCurrency(TITLE_TOKEN, -70, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(79));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 341://gardien de Cenarius
				if (player->HasTitle(sCharTitlesStore.LookupEntry(132))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 70))
					{
						player->ModifyCurrency(TITLE_TOKEN, -70, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(132));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 342://champion d'argent
				if (player->HasTitle(sCharTitlesStore.LookupEntry(131))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 70))
					{
						player->ModifyCurrency(TITLE_TOKEN, -70, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(131));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 343://du Verdict des cendres
				if (player->HasTitle(sCharTitlesStore.LookupEntry(176))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 70))
					{
						player->ModifyCurrency(TITLE_TOKEN, -70, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(176));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 344://l'exalte
				if (player->HasTitle(sCharTitlesStore.LookupEntry(77))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 70))
					{
						player->ModifyCurrency(TITLE_TOKEN, -70, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(77));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 345://le grand malade
				if (player->HasTitle(sCharTitlesStore.LookupEntry(145))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 70))
					{
						player->ModifyCurrency(TITLE_TOKEN, -70, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(145));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;
				//////////////////////////////////////////////////////////////////////////////////ACTION/////////////////////////////////////////////////////////////////////
				//PvP & Arena
			case 346://Maitre de guerre 
				if (player->HasTitle(sCharTitlesStore.LookupEntry(72))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 80))
					{
						player->ModifyCurrency(TITLE_TOKEN, -80, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(72));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 347://Maitre de l'arene
				if (player->HasTitle(sCharTitlesStore.LookupEntry(82))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 80))
					{
						player->ModifyCurrency(TITLE_TOKEN, -80, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(82));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 348://Duelliste
				if (player->HasTitle(sCharTitlesStore.LookupEntry(43))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 80))
					{
						player->ModifyCurrency(TITLE_TOKEN, -80, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(43));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 349://Rival
				if (player->HasTitle(sCharTitlesStore.LookupEntry(44))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 80))
					{
						player->ModifyCurrency(TITLE_TOKEN, -80, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(44));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 350://Competiteur
				if (player->HasTitle(sCharTitlesStore.LookupEntry(45))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 80))
					{
						player->ModifyCurrency(TITLE_TOKEN, -80, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(45));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 351://Vainqueur
				if (player->HasTitle(sCharTitlesStore.LookupEntry(163))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 80))
					{
						player->ModifyCurrency(TITLE_TOKEN, -80, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(163));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;
				//////////////////////////////////////////////////////////////////////////////////ACTION/////////////////////////////////////////////////////////////////////
				//Other
			case 352://Croise
				if (player->HasTitle(sCharTitlesStore.LookupEntry(156))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 100))
					{
						player->ModifyCurrency(TITLE_TOKEN, -100, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(156));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 353://Archimage
				if (player->HasTitle(sCharTitlesStore.LookupEntry(93))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 100))
					{
						player->ModifyCurrency(TITLE_TOKEN, -100, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(93));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 354://champion des naaru
				if (player->HasTitle(sCharTitlesStore.LookupEntry(53))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 100))
					{
						player->ModifyCurrency(TITLE_TOKEN, -100, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(53));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 355://Main d'A'dal
				if (player->HasTitle(sCharTitlesStore.LookupEntry(64))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 100))
					{
						player->ModifyCurrency(TITLE_TOKEN, -100, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(64));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;//////////////////////////////////////////////////////////////////////////////////ACTION/////////////////////////////////////////////////////////////////////
				//Hors de prix
			case 357://Malesfique
				if (player->HasTitle(sCharTitlesStore.LookupEntry(90))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 500))
					{
						player->ModifyCurrency(TITLE_TOKEN, -500, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(90));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 358://le cherche-magie
				if (player->HasTitle(sCharTitlesStore.LookupEntry(120))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 500))
					{
						player->ModifyCurrency(TITLE_TOKEN, -500, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(120));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 359://Prophete
				if (player->HasTitle(sCharTitlesStore.LookupEntry(89))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 600))
					{
						player->ModifyCurrency(TITLE_TOKEN, -600, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(89));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 360://Assassin
				if (player->HasTitle(sCharTitlesStore.LookupEntry(95))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 650))
					{
						player->ModifyCurrency(TITLE_TOKEN, -650, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(95));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 361://faucheur de la Mort l'infaillible vainqueur
				if (player->HasTitle(sCharTitlesStore.LookupEntry(158))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 700))
					{
						player->ModifyCurrency(TITLE_TOKEN, -700, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(158));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 362://l'infaillible vainqueur
				if (player->HasTitle(sCharTitlesStore.LookupEntry(128))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasItemCount( TITLE_TOKEN, 750, false ))
					{
						player->DestroyItemCount(TITLE_TOKEN, -750, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(128));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 363://des dix temp�tes
				if (player->HasTitle(sCharTitlesStore.LookupEntry(86))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 850))
					{
						player->ModifyCurrency(TITLE_TOKEN, -850, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(86));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break; 

			case 364://Du reve d'�meraude
				if (player->HasTitle(sCharTitlesStore.LookupEntry(87))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 850))
					{
						player->ModifyCurrency(TITLE_TOKEN, -850, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(87));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 365://le supreme
				if (player->HasTitle(sCharTitlesStore.LookupEntry(85))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 1000))
					{
						player->ModifyCurrency(TITLE_TOKEN, -1000, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(85));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 366:// CITIES -- ALLY
				if (player->HasTitle(sCharTitlesStore.LookupEntry(146))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 20))
					{
						player->ModifyCurrency(TITLE_TOKEN, -20, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(146));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 367:// CITIES -- ALLY
				if (player->HasTitle(sCharTitlesStore.LookupEntry(147))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 20))
					{
						player->ModifyCurrency(TITLE_TOKEN, -20, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(147));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 368:// CITIES -- ALLY
				if (player->HasTitle(sCharTitlesStore.LookupEntry(148))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 20))
					{
						player->ModifyCurrency(TITLE_TOKEN, -20, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(148));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;

			case 369:// CITIES -- ALLY
				if (player->HasTitle(sCharTitlesStore.LookupEntry(149))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 20))
					{
						player->ModifyCurrency(TITLE_TOKEN, -20, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(149));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;
				
			case 370:// CITIES -- ALLY
				if (player->HasTitle(sCharTitlesStore.LookupEntry(113))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 20))
					{
						player->ModifyCurrency(TITLE_TOKEN, -20, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(113));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;
				
			case 371:// CITIES -- HORDE
				if (player->HasTitle(sCharTitlesStore.LookupEntry(150))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 20))
					{
						player->ModifyCurrency(TITLE_TOKEN, -20, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(150));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;
			case 372:// CITIES -- HORDE
				if (player->HasTitle(sCharTitlesStore.LookupEntry(151))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 20))
					{
						player->ModifyCurrency(TITLE_TOKEN, -20, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(151));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;
			case 373:// CITIES -- HORDE
				if (player->HasTitle(sCharTitlesStore.LookupEntry(152))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 20))
					{
						player->ModifyCurrency(TITLE_TOKEN, -20, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(152));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;
			case 374:// CITIES -- HORDE
				if (player->HasTitle(sCharTitlesStore.LookupEntry(153))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 20))
					{
						player->ModifyCurrency(TITLE_TOKEN, -20, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(153));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;
			case 375:// CITIES -- HORDE
				if (player->HasTitle(sCharTitlesStore.LookupEntry(154))){
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
					player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					creature->MonsterWhisper(Deja_le_titre,player->GetGUID());
				}else{
					if (player->HasCurrency( TITLE_TOKEN, 20))
					{
						player->ModifyCurrency(TITLE_TOKEN, -20, true, false);
						player->SetTitle(sCharTitlesStore.LookupEntry(154));
						player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, TITRE_MENU);
						player->PlayerTalkClass->SendGossipMenu(TITRE_MENU, creature->GetGUID());
					}
					else
					{
						player->CLOSE_GOSSIP_MENU();
						creature->MonsterWhisper(No_Token, player->GetGUID());
					}
					return false;
				}
				break;
				
				///////////////////////////////////////////SYSTEM D'AIDE JOUEUR/////////////////////////////////////////////
				///////MENU
			case PLAYER_MENU2:
				ChatHandler(player->GetSession()).PSendSysMessage("Support Us ! Vote At www.Swarm-Server.com and gain some awesome stuff !");
				/*	name = player->GetName();
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, SERVICES_MENU);
				player->ADD_GOSSIP_ITEM( 0, "Petit Nouveau ->", GOSSIP_SENDER_MAIN, 6000);
				player->ADD_GOSSIP_ITEM( 0, "Systeme de jeu ->", GOSSIP_SENDER_MAIN, 6001);
				player->ADD_GOSSIP_ITEM( 0, "Idealogie ->", GOSSIP_SENDER_MAIN, 6002);
				player->ADD_GOSSIP_ITEM( 0, "Liens web existant ->", GOSSIP_SENDER_MAIN, 6003);
				player->ADD_GOSSIP_ITEM( 0, "Equipe Lycosa-informatique ->", GOSSIP_SENDER_MAIN, 6004);
				player->ADD_GOSSIP_ITEM( 0, "Aides & supplements ->", GOSSIP_SENDER_MAIN, 6005);*/
				//		player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, NVM_GOSSIP, GOSSIP_SENDER_MAIN, NVM_EXIT);
				player->CLOSE_GOSSIP_MENU();
				//player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;

				/*	case 6000: unused
				break;

				case 6001:
				break;
				case 6002:
				ChatHandler(player->GetSession()).PSendSysMessage("Power-Arena est un serveur a base purement PvP de type : tournament. Cependant nous restons sur le concept du (Jouer pour le plaisir), c'est donc pour cela que le serveur propose diverses modes de jeu unique.");
				break;

				case 6003:
				ChatHandler(player->GetSession()).PSendSysMessage("////////////////////////////_SITE WEB_////////////////////////////");
				ChatHandler(player->GetSession()).PSendSysMessage("|cffFF9933[Site] |cffFFFF33: |cff3399FF http://www.power-arena.fr");
				ChatHandler(player->GetSession()).PSendSysMessage("|cffFF9933[Forum]  |cffFFFF33: |cff3399FF http://forum.power-arena.fr/");
				ChatHandler(player->GetSession()).PSendSysMessage("|cffFF9933[Armurie] |cffFFFF33: |cff3399FF http://tools.power-arena.fr/armurerie/");
				ChatHandler(player->GetSession()).PSendSysMessage("|cffFF9933[Lycosa] |cffFFFF33: |cff3399FF http://www.lycosa-informatique.fr/");
				ChatHandler(player->GetSession()).PSendSysMessage("|cffFF9933[Portail] |cffFFFF33: |cff3399FF http://www.solarian-serveurs.com/");
				break;

				case 6004:
				ChatHandler(player->GetSession()).PSendSysMessage("|cff6699FFL'equipe lycosa-informatique est composee de 5 membres");
				ChatHandler(player->GetSession()).PSendSysMessage("Luke (Byakkos) : |cff99CC66Esclaves");
				ChatHandler(player->GetSession()).PSendSysMessage("Vincent (Lightning)  : |cff99CC66Fondateur & financier de Power-Arena");
				ChatHandler(player->GetSession()).PSendSysMessage("Yohan (Macronix) : |cff99CC66Fondateur du projet Lycosa-informatique & Webmaster");
				ChatHandler(player->GetSession()).PSendSysMessage("Gaetan(Suppositwar) : |cff99CC66Webmaster");
				ChatHandler(player->GetSession()).PSendSysMessage("Philippe : |cff99CC66Developpeur Core/DB & Fondateur de Power-Arena");
				break;
				/*		case 6000://PETIT NOUVEAU MENU
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, SERVICES_MENU);
				player->ADD_GOSSIP_ITEM( 0, "Utilisation ->", GOSSIP_SENDER_MAIN, 6000);
				player->ADD_GOSSIP_ITEM( 0, "Idealogie ->", GOSSIP_SENDER_MAIN, 6001);
				player->ADD_GOSSIP_ITEM( 0, "Systeme de jeu ->", GOSSIP_SENDER_MAIN, 6002);
				player->ADD_GOSSIP_ITEM( 0, "Liens web existant ->", GOSSIP_SENDER_MAIN, 6003);
				player->ADD_GOSSIP_ITEM( 0, "Equipe Lycosa-informatique ->", GOSSIP_SENDER_MAIN, 6004);
				player->ADD_GOSSIP_ITEM( 0, "Aides & supplements ->", GOSSIP_SENDER_MAIN, 6005);
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, NVM_GOSSIP, GOSSIP_SENDER_MAIN, NVM_EXIT);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;
				*/
				///////////////////////////////////////////VIP_MENU//////////////////////////////////////////////
			case VIP_MENU:
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, PLAYER_MENU);
				player->ADD_GOSSIP_ITEM(8, "Personalization  ->", GOSSIP_SENDER_MAIN, CUSTOMIZE_MENU);
				player->ADD_GOSSIP_ITEM(5, "Morph ->", GOSSIP_SENDER_MAIN, MORPH_MENU);
				player->ADD_GOSSIP_ITEM(5, SAVE , GOSSIP_SENDER_MAIN, SAVE_MENU);
				//player->ADD_GOSSIP_ITEM(0, "Boite au letre ->", GOSSIP_SENDER_MAIN, 10000);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

			case CUSTOMIZE_MENU:
				player->PlayerTalkClass->ClearMenus();
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, VIP_MENU);
				player->ADD_GOSSIP_ITEM(5, "Change Name", GOSSIP_SENDER_MAIN, ACTION_RENAME);
				player->ADD_GOSSIP_ITEM(5, "Change Skin", GOSSIP_SENDER_MAIN, ACTION_CUSTOMIZE);
				player->ADD_GOSSIP_ITEM(5, "Change Race", GOSSIP_SENDER_MAIN, ACTION_CHANGE_RACE);
				player->ADD_GOSSIP_ITEM(5, "Change Faction", GOSSIP_SENDER_MAIN, ACTION_CHANGE_FACTION);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

#define Please_Disconnect "Please Re-Connecte to see effect"

			case ACTION_RENAME:
				{
					player->SetAtLoginFlag(AT_LOGIN_RENAME);
					player->PlayDirectSound(13838);
					creature->MonsterWhisper(Please_Disconnect, player->GetGUID());
					player->CLOSE_GOSSIP_MENU();
				}
				break;

			case ACTION_CUSTOMIZE:
				{
					player->SetAtLoginFlag(AT_LOGIN_CUSTOMIZE);
					player->PlayDirectSound(13838);
					creature->MonsterWhisper(Please_Disconnect, player->GetGUID());
					player->CLOSE_GOSSIP_MENU();
				}
				break;

			case ACTION_CHANGE_FACTION:
				{
					player->SetAtLoginFlag(AT_LOGIN_CHANGE_FACTION);
					player->PlayDirectSound(13838);
					creature->MonsterWhisper(Please_Disconnect, player->GetGUID());
					player->CLOSE_GOSSIP_MENU();
				}
				break;

			case ACTION_CHANGE_RACE:
				{
					player->SetAtLoginFlag(AT_LOGIN_CHANGE_RACE);
					player->PlayDirectSound(13838);
					creature->MonsterWhisper(Please_Disconnect, player->GetGUID());
					player->CLOSE_GOSSIP_MENU();
				}
				break;
				////////////////////////////////////////////////////VIP_MORPH///////////////////////////////////

#define Class_Human "Human ->"
#define Class_Gnome "Gnome ->"
#define Class_Nain "Dwarf ->"
#define Class_Elf_de_la_nuit "Night Elf ->"
#define Class_Draenei "Draenei ->"
#define Class_Worgen "Worgen ->"

#define Class_Orc "Orc ->"
#define Class_Undead "Undead ->"
#define Class_Tauren "Tauren ->"
#define Class_Troll "Troll ->"
#define Class_Blood_elf "Blood elf ->"
#define Class_Gobelin	"Gobelin ->"

			case MORPH_MENU:
				if (player->GetTeam() == ALLIANCE)
				{
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, VIP_MENU);
					player->ADD_GOSSIP_ITEM(5, Class_Human, GOSSIP_SENDER_MAIN, 5001);
					player->ADD_GOSSIP_ITEM(5, Class_Gnome, GOSSIP_SENDER_MAIN, 5002);
					player->ADD_GOSSIP_ITEM(5, Class_Nain, GOSSIP_SENDER_MAIN, 5003);
					player->ADD_GOSSIP_ITEM(5, Class_Elf_de_la_nuit, GOSSIP_SENDER_MAIN, 5004);
					player->ADD_GOSSIP_ITEM(5, Class_Draenei, GOSSIP_SENDER_MAIN, 5005);
					player->ADD_GOSSIP_ITEM(5, Class_Worgen, GOSSIP_SENDER_MAIN, 5012);
					player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				}
				else
				{
					player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, VIP_MENU);
					player->ADD_GOSSIP_ITEM(5, Class_Orc, GOSSIP_SENDER_MAIN, 5006);
					player->ADD_GOSSIP_ITEM(5, Class_Undead, GOSSIP_SENDER_MAIN, 5007);
					player->ADD_GOSSIP_ITEM(5, Class_Tauren, GOSSIP_SENDER_MAIN, 5008);
					player->ADD_GOSSIP_ITEM(5, Class_Troll, GOSSIP_SENDER_MAIN, 5009);
					player->ADD_GOSSIP_ITEM(5, Class_Blood_elf, GOSSIP_SENDER_MAIN, 5010);
					player->ADD_GOSSIP_ITEM(5, Class_Gobelin, GOSSIP_SENDER_MAIN, 5011);
					player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				}
				break;
				/////////////////////////////////////////////ACTION/////////////////////////////////////////////////
#define Males "Males ->"
#define FeMales "Females ->"				
			case 5001:
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, VIP_MENU);
				player->ADD_GOSSIP_ITEM(5, Males, GOSSIP_SENDER_MAIN, 5035);
				player->ADD_GOSSIP_ITEM(5, FeMales, GOSSIP_SENDER_MAIN, 5036);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

			case 5002:
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, VIP_MENU);
				player->ADD_GOSSIP_ITEM(5, Males, GOSSIP_SENDER_MAIN, 5013);
				player->ADD_GOSSIP_ITEM(5, FeMales, GOSSIP_SENDER_MAIN, 5014);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

			case 5003:
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, VIP_MENU);
				player->ADD_GOSSIP_ITEM(5, Males, GOSSIP_SENDER_MAIN, 5015);
				player->ADD_GOSSIP_ITEM(5, FeMales, GOSSIP_SENDER_MAIN, 5016);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

			case 5004:
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, VIP_MENU);
				player->ADD_GOSSIP_ITEM(5, Males, GOSSIP_SENDER_MAIN, 5017);
				player->ADD_GOSSIP_ITEM(5, FeMales, GOSSIP_SENDER_MAIN, 5018);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

			case 5005:
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, VIP_MENU);
				player->ADD_GOSSIP_ITEM(5, Males, GOSSIP_SENDER_MAIN, 5019);
				player->ADD_GOSSIP_ITEM(5, FeMales, GOSSIP_SENDER_MAIN, 5020);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

			case 5006:
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, VIP_MENU);
				player->ADD_GOSSIP_ITEM(5, Males, GOSSIP_SENDER_MAIN, 5021);
				player->ADD_GOSSIP_ITEM(5, FeMales, GOSSIP_SENDER_MAIN, 5022);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

			case 5007:
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, VIP_MENU);
				player->ADD_GOSSIP_ITEM(5, Males, GOSSIP_SENDER_MAIN, 5023);
				player->ADD_GOSSIP_ITEM(5, FeMales, GOSSIP_SENDER_MAIN, 5024);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

			case 5008:
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, VIP_MENU);
				player->ADD_GOSSIP_ITEM(5, Males, GOSSIP_SENDER_MAIN, 5025);
				player->ADD_GOSSIP_ITEM(5, FeMales, GOSSIP_SENDER_MAIN, 5026);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

			case 5009:
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, VIP_MENU);
				player->ADD_GOSSIP_ITEM(5, Males, GOSSIP_SENDER_MAIN, 5027);
				player->ADD_GOSSIP_ITEM(5, FeMales, GOSSIP_SENDER_MAIN, 5028);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

			case 5010:
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, VIP_MENU);
				player->ADD_GOSSIP_ITEM(5, Males, GOSSIP_SENDER_MAIN, 5029);
				player->ADD_GOSSIP_ITEM(5, FeMales, GOSSIP_SENDER_MAIN, 5030);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

			case 5011:
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, VIP_MENU);
				player->ADD_GOSSIP_ITEM(5, Males, GOSSIP_SENDER_MAIN, 5031);
				player->ADD_GOSSIP_ITEM(5, FeMales, GOSSIP_SENDER_MAIN, 5032);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;
				
			case 5012:
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, GO_BACK_GOSSIP, GOSSIP_SENDER_MAIN, VIP_MENU);
				player->ADD_GOSSIP_ITEM(5, Males, GOSSIP_SENDER_MAIN, 5033);
				player->ADD_GOSSIP_ITEM(5, FeMales, GOSSIP_SENDER_MAIN, 5034);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

			case 5035://Humain Males
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(19723);
				player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
				break;

			case 5036://Humaine femelle
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(19724);
				player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
				break;

			case 5013://Gnome Mal
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(20580);
				player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
				break;

			case 5014://Gnome femelle
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(20581);
				player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
				break;

			case 5015://nain m
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(20317);
				player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
				break;

			case 5016://nain f
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(37918);
				player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
				break;

			case 5017://elf de la nuit M
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(20318);
				player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
				break;

			case 5018:// Elf de la nuit F
				player->CLOSE_GOSSIP_MENU();
				// player->SetDisplayId(A FAIRE);
				player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
				break;

			case 5019://draenei m
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(37916);
				player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
				break;

			case 5020://draenei f
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(20323);
				player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
				break;
				
			case 5033://Worgen m
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(37915);
				player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
				break;

			case 5034://Worgen f
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(37914);
				player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
				break;
				///////////////////////////////////////////////// Horde
			case 5021:
				player->CLOSE_GOSSIP_MENU();//Orc mal
				player->SetDisplayId(37920); 
				player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
				break;

			case 5022://Orc femelle
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(20316);
				player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
				break;

			case 5023://Undead M
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(37923);
				player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
				break;

			case 5024:// Undead F
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(37924);
				player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
				break;

			case 5025://Tauren Males
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(20585);
				player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
				break;

			case 5026://Tauren Famle
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(20584);
				player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
				break;

			case 5027://Troll Males
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(20321);
				player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
				break;

			case 5028://Troll feMales
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(37922);
				player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
				break;

			case 5029://blood elf Males
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(20578);
				player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
				break;

			case 5030://blood elf feMales
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(20579);
				player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
				break;
				
			case 5031://blood elf Males
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(37927);
				player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
				break;

			case 5032://blood elf feMales
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(37926);
				player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
				break;
				/////////////////////////////////////////////////BOITE AU LETRE/////////////////////////////////	
				/*case 10000: // Money
				creature->MonsterWhisper("EN DEV.", player->GetGUID());
				break;*/

		
			}			
		}
		return true;
	}
};

#define MSG1 100001
#define MSG2 100002
//Zone Duel

class doctor_npc : public CreatureScript
{
public:
	doctor_npc() : CreatureScript ("doctor_npc") {}

	bool OnGossipHello (Player* player, Creature* creature)
	{
		if (player->IsInCombat())
		{

			player->CLOSE_GOSSIP_MENU();
			creature->MonsterWhisper(IN_COMBAT, player->GetGUID());

			return true;
		};

		{
			player->PrepareQuestMenu(creature->GetGUID());
			player->SendPreparedQuest(creature->GetGUID());
		}
		{
			player->ADD_GOSSIP_ITEM(5, "Refull Life", GOSSIP_SENDER_MAIN, 1);
			player->ADD_GOSSIP_ITEM(5, "Refull Mana", GOSSIP_SENDER_MAIN, 2);
			player->ADD_GOSSIP_ITEM(5, "Refull Cooldown", GOSSIP_SENDER_MAIN, 3);
			//	player->ADD_GOSSIP_ITEM(5, "Buffs", GOSSIP_SENDER_MAIN, BUFF_MENU);
		}
		if (player->GetTeam() == ALLIANCE){
			player->ADD_GOSSIP_ITEM(5, "Remove 'Exhaustion'", GOSSIP_SENDER_MAIN, 4);
		}else{
			player->ADD_GOSSIP_ITEM(5, "Remove  'Sated'", GOSSIP_SENDER_MAIN, 5);
		}
		player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, TEXT_CUSTOM_RED GO_BACK_GOSSIP2, GOSSIP_SENDER_MAIN, 0);
		player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 uiSender, uint32 action)
	{
		{
			player->PlayerTalkClass->ClearMenus();
		}
		switch(action)
		{
		case 0:
			{
				{
					player->PrepareQuestMenu(creature->GetGUID());
					player->SendPreparedQuest(creature->GetGUID());
				}
				{
					player->ADD_GOSSIP_ITEM(5, "Refull Life", GOSSIP_SENDER_MAIN, 1);
					player->ADD_GOSSIP_ITEM(5, "Refull Mana", GOSSIP_SENDER_MAIN, 2);
					player->ADD_GOSSIP_ITEM(5, "Refull Cooldown", GOSSIP_SENDER_MAIN, 3);
					//	player->ADD_GOSSIP_ITEM(5, "Buffs", GOSSIP_SENDER_MAIN, BUFF_MENU);
				}
				if (player->GetTeam() == ALLIANCE){
				player->ADD_GOSSIP_ITEM(5, "Remove 'Exhaustion'", GOSSIP_SENDER_MAIN, 4);
				}else{
				player->ADD_GOSSIP_ITEM(5, "Remove  'Sated'", GOSSIP_SENDER_MAIN, 5);
				}
				player->ADD_GOSSIP_ITEM(CHAT_DOTS_ICON, TEXT_CUSTOM_RED GO_BACK_GOSSIP2, GOSSIP_SENDER_MAIN, 0);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;
				return true;
			}				
			//player->PlayerTalkClass->ClearMenus();
		case 1://Restore Health
			player->CLOSE_GOSSIP_MENU();
			player->SetHealth(player->GetMaxHealth());
			player->CastSpell(player,52582,false);
			creature->MonsterWhisper("Life Refulled", player->GetGUID());
			break;
		case 2: //Restore Mana
			player->CLOSE_GOSSIP_MENU();
			player->SetPower(POWER_MANA, player->GetMaxPower(POWER_MANA));
			player->CastSpell(player,52582,false);
			creature->MonsterWhisper("Mana Refulled", player->GetGUID());
			break;
		case 3://Remove Cooldown
			player->CLOSE_GOSSIP_MENU();
			player->RemoveAllSpellCooldown();
			player->CastSpell(player,75459,false);
			creature->MonsterWhisper("Cooldown Refulled", player->GetGUID());
			break;
		case 4://Remove Exhaustion
			player->CLOSE_GOSSIP_MENU();
			player->RemoveAurasDueToSpell(57723);
			player->CastSpell(player,61456,false);
			creature->MonsterWhisper("Done !", player->GetGUID());
			break;
		case 5://Remove Sated
			player->CLOSE_GOSSIP_MENU();
			player->RemoveAurasDueToSpell(57724);
			player->CastSpell(player,61456,false); 
			creature->MonsterWhisper("Done !", player->GetGUID());
			break;
		}
		return true;
	}
};


void AddSC_teleporter()
{
	new teleporter();
	new doctor_npc();
}
