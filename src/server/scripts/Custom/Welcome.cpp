/*
//Trinitycore//
Author : Philippe
Welcome NPC 
Version : 4.3.4

//Spell List % Complete//
Warrior : 100 %
Mage : 100 %
Druid : 100 %
Paladin : 100 %
Warlock : 100 %
Hunter : 100 %
Shaman : 100 %
Rogue : 100 %
Priest : 100 %
DK : 100 %
Mount spells : 100 %
Mastery : 100 %

//Spell Glyph % Complete//
Warrior : 0 %
Mage : 0 %
Druid : 0 %
Paladin : 0 %
Warlock : 0 %
Hunter : 0 %
Shaman : 0 %
Rogue : 0 %
Priest : 0 %
DK : 0 %
*/

#include "Extension.h"

enum eIcons
{
	CHAT_ICON               = 0,
	VENDOR_ICON             = 1,
	FLIGHT_ICON             = 2,
	TRAINER_ICON            = 3,
	GEAR_ICON               = 4,
	GEAR_ICON_2             = 5,
	BANK_ICON               = 6,
	CHAT_DOTS_ICON          = 7,
	TABARD_ICON             = 8,
	SWORDS_ICON             = 9,
	GOLD_DOT_ICON           = 10
};

enum war_spell 
{
	war_spell_Charge = 100,
	war_spell_Rend = 772,
	war_spell_Thunder_Clap = 6343,
	war_spell_Heroic_Strike = 78,
	war_spell_Heroic_Throw =57755,
	war_spell_OverPower = 7384,
	war_spell_Hamstring = 1715,
	war_spell_Retaliation = 20230,
	war_spell_Shattering_Throw = 64382,
	war_spell_Colossus_Smash = 86346,
	war_spell_Victory_Rush = 34428,
	war_spell_Execute = 5308,
	war_spell_Battle_Shout = 6673,
	war_spell_Cleave = 845,
	war_spell_Berserker_Stance =2458,
	war_spell_WhirlWind = 1680,
	war_spell_Pummel = 6552,
	war_spell_Intimidating_Shout = 5246,
	war_spell_Slam = 1464,
	war_spell_Challenging_shout = 1161,
	war_spell_Intercept = 20252,
	war_spell_Demoralizing_Shout = 1160,
	war_spell_Berserker_Rage = 18499,
	war_spell_Inner_rage = 1134,
	war_spell_Recklessness = 1719,
	war_spell_Commanding_Shout = 469,
	war_spell_Enraged_Regeneration = 55694,
	war_spell_Rallying_Cry = 97462,
	war_spell_Heroic_leap = 6544,
	war_spell_Defensive_Stance = 71,
	war_spell_Parry = 3127,
	war_spell_Taunt = 355,
	war_spell_Shield_Block = 2565,
	war_spell_Disarm = 676,
	war_spell_Revenge = 6572,
	war_spell_Shield_Wall = 871,
	war_spell_Stance_Mastery = 12678,
	war_spell_Shell_Reflection = 23920,
	war_spell_Intervene = 3411,
	war_spell_Sunder_Armor = 7386,
	war_spell_Mastery = 87500
};

enum pala_spell
{
	pala_spell_holy_light = 635,
	pala_spell_word_of_glory = 85673,
	pala_spell_redemption = 7328,
	pala_spell_flash_of_light = 19750,
	pala_spell_lay_of_hands = 633,
	pala_spell_Exorcism = 879,
	pala_spell_consecration = 26573,
	pala_spell_holy_wrath = 2812,
	pala_spell_seal_of_insight = 20165,
	pala_spell_cleanse = 4987,
	pala_spell_concentration_aura = 19746,
	pala_spell_divine_plea = 54428,
	pala_spell_divine_light = 82326,
	pala_spell_turn_evil = 10326,
	pala_spell_holy_radiance = 82327,
	pala_spell_seal_of_righteousness = 20154,
	pala_spell_devotion_aura = 465,
	pala_spell_parry  = 82242,
	pala_spell_righteous_fury = 25780,
	pala_spell_hammer_of_justice = 853,
	pala_spell_hand_of_reckoning = 62124,
	pala_spell_hand_of_protection = 1022,
	pala_spell_blessing_of_kings = 20217,
	pala_spell_divine_protection = 498,
	pala_spell_righteous_defense = 31789,
	pala_spell_divine_shield = 642,
	pala_spell_hand_of_freedom = 1044,
	pala_spell_seal_of_justice = 20164,
	pala_spell_hand_of_salvation = 1038,
	pala_spell_resistance_aura = 19891,
	pala_spell_hand_of_sacrifice = 6940,
	pala_spell_guardian_of_ancient_kings = 86150,
	pala_spell_judgement = 20271,
	pala_spell_retribution_aura = 7294,
	pala_spell_seal_of_truth = 31801,
	pala_spell_hammer_of_wrath = 24275,
	pala_spell_rebuke = 96231,
	pala_spell_blessing_of_might = 19740,
	pala_spell_crusader_aura = 32223,
	pala_spell_avenging_wrath = 31884,
	pala_spell_inquisition = 84963
};

enum mage_spell
{
	mage_spell_Conjure_mana_gem = 759,
	mage_spell_mirror_image = 55342,
	mage_spell_arcane_brilliance = 1459,
	mage_spell_mage_armor = 6117,
	mage_spell_spellsteal = 30449,
	mage_spell_ritual_of_refreshment = 43987,
	mage_spell_invisibility = 66,
	mage_spell_time_warp = 80353,
	mage_spell_Arcane_missiles = 5143,
	mage_spell_Counterspell = 2139,
	mage_spell_Evocation = 12051,
	mage_spell_Polymorph = 118,
	mage_spell_Blink = 1953,
	mage_spell_Arcane_Blast = 30451,
	mage_spell_Arcane_Explosion = 1449,
	mage_spell_Remove_curse = 475,
	mage_spell_slow_fall = 130,
	mage_spell_mage_ward = 543,
	mage_spell_conjure_refreshement = 42955,
	mage_spell_mana_shield = 1463,
	mage_spell_fire_blast = 2136,
	mage_spell_scorch = 2948,
	mage_spell_molten_armor = 30482,
	mage_spell_Flamestrike = 2120,
	mage_spell_frostfire_bolt = 44614,
	mage_spell_flame_orb = 82731,
	mage_spell_frostbolt = 116,
	mage_spell_frost_nova = 122,
	mage_spell_Cone_of_cold = 120,
	mage_spell_ice_lance = 30455,
	mage_spell_ice_block = 45438,
	mage_spell_blizzard = 10,
	mage_spell_forst_armor = 7302,
	mage_spell_ring_of_frost = 82676
};

enum dk_spell
{
	dk_spell_Death_Strike = 49998,
	dk_spell_Pestilence = 50842,
	dk_spell_Blood_Presence = 48263,
	dk_spell_blood_boil = 48721,
	dk_spell_strangulate = 47476,
	dk_spell_Blood_Tap = 45529,
	dk_spell_Dark_command = 56222, 
	dk_spell_death_pact = 48743,
	dk_spell_Dark_simulacrum = 77606,
	dk_spell_mind_freeze = 47528,
	dk_spell_chains_of_ice = 45524,
	dk_spell_obliterate = 49020,
	dk_spell_path_of_frost = 3714,
	dk_spell_icebound_fortitude = 48792,
	dk_spell_festering_strike = 85948,
	dk_spell_horn_of_winter = 57330,
	dk_spell_rune_strike = 56815,
	dk_spell_runic_empowerment = 81229,
	dk_spell_empower_rune_weapon = 47568,
	dk_spell_raise_dead = 46584,
	dk_spell_death_of_decay = 43265,
	dk_spell_anti_magic_shell = 48707,
	dk_spell_unholy_presence = 48265,
	dk_spell_raise_ally = 61999,
	dk_spell_army_of_the_dead = 42650,
	dk_spell_outbreak =  77575,
	dk_spell_necrotic_strike = 73975
};

enum druid_spell
{
	druid_spell_moonfire = 8921,
	druid_spell_thorns = 467,
	druid_spell_entanling_roots = 339,
	druid_spell_starfire = 2912,
	druid_spell_teleport_moonglade = 18960,
	druid_spell_insect_swarm = 5570,
	druid_spell_faerie_fire = 770,
	druid_spell_innervate = 29166,
	druid_spell_soothe = 2908,
	druid_spell_hurricane = 16914,
	druid_spell_hibernate = 2637,
	druid_spell_nature_grasp = 16689,
	druid_spell_barkskin = 22812,
	druid_spell_cyclone = 33786,
	druid_spell_wild_mushroom = 88747,
	druid_spell_wild_mushroom_detonate = 88751,
	druid_spell_cat_form = 768,
	druid_spell_claw = 1082,
	druid_spell_ferocious_bite = 22568,
	druid_spell_rake = 1822,
	druid_spell_prowl = 5215,
	druid_spell_bear_form = 5487,
	druid_spell_demoralizing_roar = 99,
	druid_spell_growl = 6795,
	druid_spell_maul = 6807,
	druid_spell_aquatic_form = 1066,
	druid_spell_travel_form = 783,
	druid_spell_swipe = 779,
	druid_spell_enrage = 5229,
	druid_spell_ravage = 6785,
	druid_spell_skill_Bash_cat = 80965,
	druid_spell_skill_bash_bear = 80964,
	druid_spell_faerie_fire_feral = 16857,
	druid_spell_tiger_fury = 5217,
	druid_spell_cower = 8998,
	druid_spell_dash = 1850,
	druid_spell_feline_grace = 20719,
	druid_spell_challenging_roar = 5209,
	druid_spell_bash = 5211,
	druid_spell_pounce = 9005,	
	druid_spell_swipe_feral = 62078,
	druid_spell_savage_defense = 62600,
	druid_spell_shred = 5221,
	druid_spell_frenzied_regeneration = 22842,
	druid_spell_rip = 1079,
	druid_spell_flight_form = 33943,
	druid_spell_maim = 22570,
	druid_spell_lacerate = 33745,
	druid_spell_swift_flight_form = 40120,
	druid_spell_savage_roar = 52610,
	druid_spell_trash = 77758,
	druid_spell_stampeding_roar = 77761,
	druid_spell_stampeding_raor_cat = 77764,
	druid_spell_rejuvenation = 774,
	druid_spell_nourish = 50464,
	druid_spell_regrowth = 8936,
	druid_spell_revive = 50769,
	druid_spell_omen_of_clarity = 16864,
	druid_spell_rebirth = 20484,
	druid_spell_remove_corruption = 2782,
	druid_spell_mark_of_the_wild = 1126,
	druid_spell_lifebloom = 33763,
	druid_spell_tranquility = 740,
	druid_spell_healing_touch = 5185
};

enum shaman_spell
{
	shaman_spell_earth_shock = 8042,
	shaman_spell_searing_totem  = 3599,
	shaman_spell_purge = 370,
	shaman_spell_flame_shock = 8050,
	shaman_spell_wind_shear = 57994,
	shaman_spell_earthbind_totem = 2484,
	shaman_spell_frost_shock = 8056,
	shaman_spell_chain_lightning = 421,
	shaman_spell_fire_nova = 1535,
	shaman_spell_call_of_the_elements = 66842,
	shaman_spell_lava_burst = 51505,
	shaman_spell_magma_totem = 8190,
	shaman_spell_call_of_the_ancestors = 66843,
	shaman_spell_call_of_the_spirits = 66844,
	shaman_spell_stoneclaw_totem = 5730,
	shaman_spell_bind_elemental = 76780,
	shaman_spell_fire_elemental_totem = 2894,
	shaman_spell_hex = 51514,
	shaman_spell_spiritwalker_grace = 79206, 
	shaman_spell_primal_strike = 73899,
	shaman_spell_strenght_of_earth_totem = 8075,
	shaman_spell_lightning_shield = 324,
	shaman_spell_flametongue_weapon = 8024,
	shaman_spell_flametongue_totem = 8227,
	shaman_spell_ghost_Wolf = 2645,
	shaman_spell_water_walking = 546,
	shaman_spell_frostbrand_weapon = 8033,
	shaman_spell_astral_recall = 556,
	shaman_spell_windfury_totem = 8512,
	shaman_spell_windfury_weapon =  8232,
	shaman_spell_far_sight = 6196,
	shaman_spell_grounding_totem = 8177,
	shaman_spell_wrath_of_air_totem = 3738,
	shaman_spell_water_breathing = 131,
	shaman_spell_stoneskin_totem = 8071,
	shaman_spell_earth_elemental_totem = 2062,
	shaman_spell_elemental_resistance_totem = 8184,
	shaman_spell_heroism = 32182,
	shaman_spell_rockbiter_weapon = 8017,
	shaman_spell_unleash_elements = 73680,
	shaman_spell_healing_wave = 331,
	shaman_spell_ancestral_spirit = 2008,
	shaman_spell_cleanse_spirit = 51886,
	shaman_spell_healing_stream_totem = 5394,
	shaman_spell_healing_surge = 8004,
	shaman_spell_water_shield = 52127,
	shaman_spell_reincarnation = 20608,
	shaman_spell_totemic_recall = 36936,
	shaman_spell_chain_heal = 1064,
	shaman_spell_mana_spring_totem = 5675,
	shaman_spell_tremor_totem = 8143,
	shaman_spell_earthliving_weapon = 51730,
	shaman_spell_greater_healing_wave = 77472,
	shaman_spell_totem_of_tranquil_mind = 87718,
	shaman_spell_healing_rain = 73920,
};

enum hunter_spell
{
	hunter_spell_beast_lore = 1462,
	hunter_spell_control_pet = 93321,
	hunter_spell_Dismiss_pet = 2641,
	hunter_spell_feed_pet = 6991,
	hunter_spell_kill_command = 34026,
	hunter_spell_tame_beast = 1515,
	hunter_spell_aspect_of_the_hawk = 13165,
	hunter_spell_eagle_eye = 6197,
	hunter_spell_Mend_pet = 136,
	hunter_spell_call_pet_2 = 83242,
	hunter_spell_aspect_of_the_cheetah = 5118,
	hunter_spell_scare_beast = 1513,
	hunter_spell_window_venom = 82654,
	hunter_spell_call_pet_3 = 83243,
	hunter_spell_aspect_of_the_pack = 13159,
	hunter_spell_call_pet_4 = 83244,
	hunter_spell_aspect_of_the_wild = 20043,
	hunter_spell_master_call = 53271,
	hunter_spell_call_pet_5 = 83245,
	hunter_spell_aspect_of_the_fox = 82661,
	hunter_spell_steady_shot =  56641, 
	hunter_spell_hunter_mark = 1130,
	hunter_spell_multi_shot = 2643,
	hunter_spell_kill_shot = 53351,
	hunter_spell_tranquilizing_shot = 19801,
	hunter_spell_flare = 1543,
	hunter_spell_distracting_shot = 20736,
	hunter_spell_rapid_fire = 3045,
	hunter_spell_raptor_strike = 2973,
	hunter_spell_parry = 82243,
	hunter_spell_serpent_sting = 1978,
	hunter_spell_wing_clip = 2974,
	hunter_spell_disengage = 781,
	hunter_spell_scatter_shot = 19503,
	hunter_spell_immolation_trap = 13795,
	hunter_spell_freezing_trap = 1499,
	hunter_spell_feign_death = 5384,
	hunter_spell_explosive_trap = 13813,
	hunter_spell_ice_trap  = 13809,
	hunter_spell_trap_launcher = 77769,
	hunter_spell_snake_trap = 34600,
	hunter_spell_misdirection = 34477,
	hunter_spell_deterrence = 19263,
	hunter_spell_cobra_shot = 77767,
	hunter_spell_camouflage = 51753
};

enum rogue_spell
{
	rogue_spell_Eviscerate = 2098,
	rogue_spell_Ambush = 8676,
	rogue_spell_Poisons = 2842,
	rogue_spell_Slice_and_Dice =5171,
	rogue_spell_Cheap_Shot = 1833,
	rogue_spell_Kidney_Shot = 408,
	rogue_spell_Expose_Armor = 8647,
	rogue_spell_Dismantle = 51722,
	rogue_spell_Garrote = 703, 
	rogue_spell_Rupture = 1943, 
	rogue_spell_Envenom = 32645,
	rogue_spell_Deadly_Throw = 26679,
	rogue_spell_Evasion = 5277,
	rogue_spell_Parry = 82245,
	rogue_spell_Recuperate = 73651,
	rogue_spell_Kick = 1766,
	rogue_spell_Gouge=  1776,
	rogue_spell_Sprint = 2983,
	rogue_spell_Backstab = 53,
	rogue_spell_Feint = 1966,
	rogue_spell_Shiv = 5938,
	rogue_spell_Fan_of_Knives = 51723,
	rogue_spell_Combat_Readiness = 74001,
	rogue_spell_Stealth = 1784,
	rogue_spell_Pick_Pocket = 921,
	rogue_spell_Sap = 6770,
	rogue_spell_Pick_Lock = 1804,
	rogue_spell_Vanish = 1856,
	rogue_spell_Distract = 1725,
	rogue_spell_Detect_Traps = 2836,
	rogue_spell_Blind = 2094,
	rogue_spell_Disarm_Trap = 1842,
	rogue_spell_Safe_Fall = 1860,
	rogue_spell_Cloak_of_Shadows = 31224, 
	rogue_spell_Tricks_Of_the_Trade = 57934,
	rogue_spell_Redirect = 73981,
	rogue_spell_Smoke_Bomb = 76577

};

enum priest_spell
{
	priest_spell_Power_Word_Shield = 17,
	priest_spell_Inner_Fire = 588,
	priest_spell_Power_word_Fortitude = 21562,
	priest_spell_Dispel_Magic = 527,
	priest_spell_Shackle_Undead = 9484,
	priest_spell_Levitate = 1706,
	priest_spell_Fear_Ward = 6346, 
	priest_spell_Mana_Burn = 8129,
	priest_spell_Mass_Dispel = 32375,
	priest_spell_Inner_Will = 73413,
	priest_spell_Flash_Heal = 2061,
	priest_spell_Renew = 139,
	priest_spell_Resurrection = 2006,
	priest_spell_Heal = 2050,
	priest_spell_Holy_Fire = 14914,
	priest_spell_Cure_Disease = 528,
	priest_spell_Grater_Heal = 2060,
	priest_spell_Prayer_Of_Healing = 596,
	priest_spell_Binding_Heal = 32546,
	priest_spell_Holy_Nova = 15237,
	priest_spell_Hymn_Of_Hope = 64901,
	priest_spell_Prayer_of_Mending = 33076,
	priest_spell_Divine_Hymn = 64843,
	priest_spell_Leap_Of_faith = 73325,
	priest_spell_Shadow_Word_Pain = 589,
	priest_spell_Mind_Blast = 8092,
	priest_spell_Psychic_Scream = 8122,
	priest_spell_Fade = 586,
	priest_spell_Devouring_plague = 2944,
	priest_spell_Shadow_Word_Death = 32379,
	priest_spell_Mind_Vision = 2096,
	priest_spell_Mind_Control = 605,
	priest_spell_Shadow_Protection = 27683,
	priest_spell_Mind_Soothe = 453, 
	priest_spell_ShadowFiend = 34433,
	priest_spell_Mind_Sear = 48045,
	priest_spell_Mind_Spike = 73510
};

enum warlock_spell
{
	warlock_spell_Corruption = 172,
	warlock_spell_Life_Tap = 1454,
	warlock_spell_Drain_life = 689,
	warlock_spell_Drain_soul = 1120,
	warlock_spell_Bane_Of_Agony = 980,
	warlock_spell_Fear = 5782,
	warlock_spell_Curse_of_Weakness = 702,
	warlock_spell_Bane_Of_Doom = 603,
	warlock_spell_Curse_Of_Tongues = 1714,
	warlock_spell_Death_Coil = 6789,
	warlock_spell_Howl_Of_Terror = 5484,
	warlock_spell_Curse_of_the_Elements = 1490,
	warlock_spell_Seed_Of_Corruption = 27243,
	warlock_spell_Dark_Intent = 80398,
	warlock_spell_Demon_Armor = 687,
	warlock_spell_Summon_Voidwalker = 697,
	warlock_spell_Create_Healthstone = 6201,
	warlock_spell_Control_Demon = 93375,
	warlock_spell_Soulburn = 74434,
	warlock_spell_Healt_Funnel = 755,
	warlock_spell_Soul_Harvest = 79268,
	warlock_spell_Unending_Breath = 5697,
	warlock_spell_Create_Soulstone = 693,
	warlock_spell_Soul_Link = 19028,
	warlock_spell_Summon_Succubus = 712,
	warlock_spell_eye_of_kilrogg = 126,
	warlock_spell_enslave_demon = 1098,
	warlock_spell_summon_felhunter = 691,
	warlock_spell_Banish = 710,
	warlock_spell_Shadow_Ward = 6229,
	warlock_spell_Ritual_Of_summoning = 698,
	warlock_spell_Summon_Infernal = 1122,
	warlock_spell_Summon_Doomguard = 18540,
	warlock_spell_Fel_armor = 28176,
	warlock_spell_SoulShatter = 29858,
	warlock_spell_Ritual_of_souls = 29893,
	warlock_spell_Demonic_Circle_Summon = 48018,
	warlock_spell_Demonic_Circle_Teleport = 48020,
	warlock_spell_Demon_Soul = 77801,
	warlock_spell_Immolate = 348,
	warlock_spell_Rain_of_Fire = 5740,
	warlock_spell_Searing_Pain = 5676,
	warlock_spell_Soul_Fire = 6353,
	warlock_spell_Hellfire = 1949,
	warlock_spell_Incinerate = 29722,
	warlock_spell_ShadowFlame = 47897,
	warlock_spell_Fel_Flame = 77799
};

enum Specialisazion
{
	Spell_Dual_Wield = 674,
	Spell_Spec_War_Plate = 86526, // War Only
	Spell_Spec_Pal_Plate = 86525, // Pal Only
	Spell_Spec_DK_Plate = 86524, // Dk Only 
	Spell_Spec_Druid_Leather = 86530, // Druid
	Spell_Spec_Rogue_Leather = 86531, // Druid
	Spell_Spec_Hunt_Mail = 86528, // Hunter
	Spell_Spec_Shaman_Mail = 86529, //Shaman
	Spell_netherMancy = 86091, //Warlock
	Spell_Wizardry = 89744, //Mage
	Spell_Mysticism = 89745, //Priest
	Spell_Equip_plate = 750, // Pala-War-DK
	Spell_Equip_Mail = 8737, // Hunt-Shaman
	Spell_Equip_Leather = 9077 // Druid-Rogue
};

enum mount_spell
{
	mount_spell_Master_Riding = 90265,
	mount_spell_Journeyman_Riding = 33392,
	mount_spell_Cold_Weather_Flying = 54197,
	mount_spell_Flight_Master_License = 90267
};


enum spell_mastery
{
	spell_mastery_warrior = 87500,
	spell_mastery_mage = 86467,
	spell_mastery_druid = 87491,
	spell_mastery_dk = 87492,
	spell_mastery_hunter = 87493,
	spell_mastery_paladin = 87494,
	spell_mastery_priest = 87495,
	spell_mastery_rogue = 87496,
	spell_mastery_shaman = 87497,
	spell_mastery_warlock = 87498
};

//////////////////////WELCOME MENU/////////////////////////////////
#define Class_Skills  "I would like to learn my class spell"	   
#define Weapon_Skills "I would like to learn my Weapon skills"
#define Riding_Skills "I would like to learn my Riding skills"
#define nevermind 	  "[nevermind]"							 
#define back 		  "[back]"								
//////////////////////////////////////////////////////////////////

class npc_welcome : public CreatureScript
{
public:
	npc_welcome() : CreatureScript("npc_welcome") { }

	bool OnGossipHello(Player* player, Creature* creature)
	{
		if (player->getLevel() >= 80)
		{
			{
				player->PrepareQuestMenu(creature->GetGUID());
				player->SendPreparedQuest(creature->GetGUID());
			}
		/*	if(player->HasSpell(75906))
			{
			ChatHandler(player->GetSession()).PSendSysMessage("|cFFFFFC00[System]|cFF00FFFF Well, player you have nothing to do here anymore.");
			}
			else
			{*/
			player->ADD_GOSSIP_ITEM(TRAINER_ICON, Class_Skills, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+1);
			player->ADD_GOSSIP_ITEM(TRAINER_ICON, Riding_Skills, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+2);
			//}
			player->SEND_GOSSIP_MENU(1, creature->GetGUID());
		}
		else
		{
			player->PlayerTalkClass->SendCloseGossip();
			return false;
		}
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
	{
		uint32 mount_spells[4] = 
		{
			mount_spell_Flight_Master_License,mount_spell_Journeyman_Riding,mount_spell_Cold_Weather_Flying,mount_spell_Master_Riding
		};

		uint32 war_spell[44] = 
		{
			war_spell_OverPower, war_spell_Battle_Shout, war_spell_Heroic_Strike, war_spell_Charge, war_spell_Rend, war_spell_Thunder_Clap, war_spell_Victory_Rush, war_spell_Hamstring, war_spell_Defensive_Stance, war_spell_Sunder_Armor,war_spell_Taunt,war_spell_Demoralizing_Shout,war_spell_Revenge,war_spell_Shield_Block,war_spell_Disarm,war_spell_Cleave,war_spell_Retaliation,war_spell_Execute,war_spell_Shield_Wall,war_spell_Berserker_Stance,war_spell_Intercept,war_spell_Slam,war_spell_WhirlWind,war_spell_Pummel,war_spell_Slam,war_spell_Recklessness,war_spell_Shell_Reflection,war_spell_Commanding_Shout,war_spell_Intervene,war_spell_Shattering_Throw,war_spell_Enraged_Regeneration,war_spell_Heroic_Throw,war_spell_Intimidating_Shout,war_spell_Parry, war_spell_Colossus_Smash,war_spell_Heroic_leap,war_spell_Berserker_Rage,war_spell_Inner_rage,war_spell_Rallying_Cry,war_spell_Stance_Mastery,war_spell_Mastery,Spell_Spec_War_Plate,war_spell_Challenging_shout,Spell_Equip_plate
		};


		uint32 mage_spell[36] = 
		{
			mage_spell_Conjure_mana_gem,mage_spell_mirror_image,mage_spell_arcane_brilliance,mage_spell_mage_armor,mage_spell_spellsteal,mage_spell_ritual_of_refreshment,mage_spell_invisibility,mage_spell_time_warp,mage_spell_fire_blast,mage_spell_scorch,mage_spell_molten_armor,mage_spell_Flamestrike,mage_spell_frostfire_bolt,mage_spell_flame_orb,mage_spell_frostbolt,mage_spell_frost_nova,mage_spell_Cone_of_cold,mage_spell_ice_lance,mage_spell_ice_block,mage_spell_blizzard,mage_spell_forst_armor,mage_spell_ring_of_frost,mage_spell_Arcane_missiles,mage_spell_Counterspell,mage_spell_Evocation,mage_spell_Polymorph,mage_spell_Blink,mage_spell_Arcane_Blast,mage_spell_Arcane_Explosion,mage_spell_Remove_curse,mage_spell_slow_fall,mage_spell_mage_ward,mage_spell_conjure_refreshement,mage_spell_mana_shield,Spell_Wizardry,spell_mastery_mage
		};

		uint32 pala_spell[44] = 
		{
			pala_spell_holy_light,pala_spell_word_of_glory,pala_spell_redemption,pala_spell_flash_of_light,pala_spell_lay_of_hands,pala_spell_Exorcism,pala_spell_consecration,pala_spell_holy_wrath,pala_spell_seal_of_insight,pala_spell_cleanse,pala_spell_concentration_aura,pala_spell_divine_plea,pala_spell_divine_light,pala_spell_turn_evil,pala_spell_holy_radiance,pala_spell_seal_of_righteousness,pala_spell_devotion_aura,pala_spell_parry,pala_spell_righteous_fury,pala_spell_hammer_of_justice,pala_spell_hand_of_reckoning,pala_spell_hand_of_protection,pala_spell_blessing_of_kings,pala_spell_divine_protection,pala_spell_righteous_defense,pala_spell_divine_shield,pala_spell_hand_of_freedom,pala_spell_seal_of_justice,pala_spell_hand_of_salvation,pala_spell_resistance_aura,pala_spell_hand_of_sacrifice,pala_spell_guardian_of_ancient_kings,pala_spell_judgement,pala_spell_retribution_aura,pala_spell_seal_of_truth,pala_spell_hammer_of_wrath,pala_spell_rebuke,pala_spell_blessing_of_might,pala_spell_crusader_aura,pala_spell_avenging_wrath,pala_spell_inquisition,spell_mastery_paladin,Spell_Equip_plate,Spell_Spec_Pal_Plate
		};

		uint32 dk_spell[29] = 
		{
			dk_spell_Death_Strike,dk_spell_Pestilence,dk_spell_Blood_Presence,dk_spell_blood_boil,dk_spell_strangulate,dk_spell_Blood_Tap,dk_spell_Dark_command,dk_spell_death_pact,dk_spell_Dark_simulacrum,dk_spell_mind_freeze,dk_spell_chains_of_ice,dk_spell_obliterate,dk_spell_path_of_frost,dk_spell_icebound_fortitude,dk_spell_festering_strike,dk_spell_horn_of_winter,dk_spell_rune_strike,dk_spell_runic_empowerment,dk_spell_empower_rune_weapon,dk_spell_raise_dead,dk_spell_death_of_decay,dk_spell_anti_magic_shell,dk_spell_unholy_presence,dk_spell_raise_ally,dk_spell_army_of_the_dead,dk_spell_outbreak,dk_spell_necrotic_strike,spell_mastery_dk,Spell_Spec_DK_Plate
		};

		uint32 druid_spell[68] =
		{
			druid_spell_moonfire,druid_spell_thorns,druid_spell_entanling_roots,druid_spell_starfire,druid_spell_teleport_moonglade,druid_spell_insect_swarm,druid_spell_faerie_fire,druid_spell_innervate,druid_spell_soothe,druid_spell_hurricane,druid_spell_hibernate,druid_spell_nature_grasp,druid_spell_barkskin,druid_spell_cyclone,druid_spell_wild_mushroom,druid_spell_wild_mushroom_detonate,druid_spell_cat_form,druid_spell_claw,druid_spell_ferocious_bite,druid_spell_rake,druid_spell_prowl,druid_spell_bear_form,druid_spell_demoralizing_roar,druid_spell_growl,druid_spell_maul,druid_spell_aquatic_form,druid_spell_travel_form,druid_spell_swipe,druid_spell_enrage,druid_spell_ravage,druid_spell_skill_Bash_cat,druid_spell_skill_bash_bear,druid_spell_faerie_fire_feral,druid_spell_tiger_fury,druid_spell_cower,druid_spell_dash,druid_spell_feline_grace,druid_spell_challenging_roar,druid_spell_bash,druid_spell_pounce,druid_spell_swipe_feral,druid_spell_savage_defense,druid_spell_shred,druid_spell_frenzied_regeneration,druid_spell_rip,druid_spell_flight_form,druid_spell_maim,druid_spell_lacerate,druid_spell_swift_flight_form,druid_spell_savage_roar,druid_spell_trash
			,druid_spell_stampeding_roar,druid_spell_stampeding_raor_cat,druid_spell_rejuvenation,druid_spell_nourish,druid_spell_regrowth,druid_spell_revive,druid_spell_omen_of_clarity,druid_spell_rebirth,
			druid_spell_remove_corruption,druid_spell_mark_of_the_wild,druid_spell_lifebloom,druid_spell_tranquility,druid_spell_healing_touch,Spell_Equip_Leather,spell_mastery_druid,Spell_Spec_Druid_Leather
		};

		uint32 shaman_spell[58] =
		{
			shaman_spell_searing_totem,shaman_spell_earth_shock,shaman_spell_purge,shaman_spell_flame_shock,shaman_spell_wind_shear,shaman_spell_earthbind_totem,shaman_spell_frost_shock,shaman_spell_chain_lightning,shaman_spell_fire_nova,shaman_spell_call_of_the_elements,shaman_spell_lava_burst,shaman_spell_magma_totem,shaman_spell_call_of_the_ancestors,shaman_spell_call_of_the_spirits,shaman_spell_stoneclaw_totem,shaman_spell_bind_elemental,shaman_spell_fire_elemental_totem,shaman_spell_hex,shaman_spell_spiritwalker_grace,shaman_spell_primal_strike,shaman_spell_strenght_of_earth_totem,shaman_spell_lightning_shield,shaman_spell_flametongue_weapon,shaman_spell_flametongue_totem,shaman_spell_ghost_Wolf,shaman_spell_water_walking,shaman_spell_frostbrand_weapon,shaman_spell_astral_recall,shaman_spell_windfury_totem,shaman_spell_windfury_weapon,shaman_spell_far_sight,shaman_spell_grounding_totem,shaman_spell_wrath_of_air_totem,shaman_spell_water_breathing,shaman_spell_stoneskin_totem,shaman_spell_earth_elemental_totem,shaman_spell_elemental_resistance_totem,shaman_spell_heroism,shaman_spell_rockbiter_weapon,shaman_spell_unleash_elements,shaman_spell_healing_wave,shaman_spell_ancestral_spirit,shaman_spell_cleanse_spirit,shaman_spell_healing_stream_totem,shaman_spell_healing_surge,shaman_spell_water_shield,shaman_spell_reincarnation,shaman_spell_totemic_recall,shaman_spell_chain_heal,shaman_spell_mana_spring_totem,shaman_spell_tremor_totem,shaman_spell_earthliving_weapon,shaman_spell_greater_healing_wave,shaman_spell_totem_of_tranquil_mind,shaman_spell_healing_rain,Spell_Spec_Shaman_Mail,spell_mastery_shaman,Spell_Equip_Mail
		};

		uint32 hunter_spell[49] =
		{
			hunter_spell_beast_lore,hunter_spell_control_pet,hunter_spell_Dismiss_pet,hunter_spell_feed_pet,hunter_spell_kill_command,hunter_spell_tame_beast,hunter_spell_aspect_of_the_hawk,hunter_spell_eagle_eye,hunter_spell_Mend_pet,hunter_spell_call_pet_2,hunter_spell_aspect_of_the_cheetah,hunter_spell_scare_beast,hunter_spell_window_venom,hunter_spell_call_pet_3,hunter_spell_aspect_of_the_pack,hunter_spell_call_pet_4,hunter_spell_aspect_of_the_wild,hunter_spell_master_call,hunter_spell_call_pet_5,hunter_spell_aspect_of_the_fox,hunter_spell_steady_shot,hunter_spell_hunter_mark,hunter_spell_multi_shot,hunter_spell_kill_shot,hunter_spell_tranquilizing_shot,hunter_spell_flare,hunter_spell_distracting_shot,hunter_spell_rapid_fire,hunter_spell_raptor_strike,hunter_spell_parry,hunter_spell_serpent_sting,hunter_spell_wing_clip,hunter_spell_disengage,hunter_spell_scatter_shot,hunter_spell_immolation_trap,hunter_spell_freezing_trap,hunter_spell_feign_death,hunter_spell_explosive_trap,hunter_spell_ice_trap,hunter_spell_trap_launcher,hunter_spell_snake_trap,hunter_spell_misdirection,hunter_spell_deterrence,hunter_spell_cobra_shot,hunter_spell_camouflage,Spell_Equip_Mail,Spell_Spec_Hunt_Mail,Spell_Dual_Wield,spell_mastery_hunter
		};

		uint32 rogue_spell[40] =
		{
			rogue_spell_Ambush,rogue_spell_Eviscerate,rogue_spell_Poisons,rogue_spell_Slice_and_Dice,rogue_spell_Cheap_Shot,rogue_spell_Kidney_Shot,rogue_spell_Expose_Armor,rogue_spell_Dismantle,rogue_spell_Garrote,rogue_spell_Rupture,rogue_spell_Envenom,rogue_spell_Deadly_Throw,rogue_spell_Evasion,rogue_spell_Recuperate,rogue_spell_Parry,rogue_spell_Kick,rogue_spell_Gouge,rogue_spell_Sprint,rogue_spell_Backstab,rogue_spell_Feint,rogue_spell_Shiv,rogue_spell_Fan_of_Knives,rogue_spell_Combat_Readiness,rogue_spell_Stealth,rogue_spell_Pick_Pocket,rogue_spell_Sap,rogue_spell_Pick_Lock,rogue_spell_Vanish,rogue_spell_Distract,rogue_spell_Detect_Traps,rogue_spell_Blind,rogue_spell_Disarm_Trap,rogue_spell_Safe_Fall,rogue_spell_Cloak_of_Shadows,rogue_spell_Tricks_Of_the_Trade,rogue_spell_Redirect,rogue_spell_Smoke_Bomb,Spell_Spec_Rogue_Leather,Spell_Equip_Leather,spell_mastery_rogue
		};


		uint32 priest_spell[39] =
		{
			priest_spell_Power_Word_Shield,priest_spell_Inner_Fire,priest_spell_Power_word_Fortitude,priest_spell_Dispel_Magic,priest_spell_Shackle_Undead,priest_spell_Levitate,priest_spell_Fear_Ward, priest_spell_Mana_Burn,priest_spell_Mass_Dispel,priest_spell_Inner_Will,priest_spell_Flash_Heal,priest_spell_Renew,priest_spell_Heal,priest_spell_Holy_Fire,
			priest_spell_Cure_Disease,priest_spell_Grater_Heal,priest_spell_Prayer_Of_Healing,priest_spell_Binding_Heal,priest_spell_Holy_Nova,priest_spell_Hymn_Of_Hope,priest_spell_Prayer_of_Mending,priest_spell_Divine_Hymn,priest_spell_Leap_Of_faith,
			priest_spell_Shadow_Word_Pain,priest_spell_Mind_Blast, priest_spell_Psychic_Scream,priest_spell_Fade,priest_spell_Devouring_plague,priest_spell_Shadow_Word_Death,priest_spell_Mind_Vision,priest_spell_Mind_Control,priest_spell_Shadow_Protection,priest_spell_Mind_Soothe, priest_spell_ShadowFiend,priest_spell_Mind_Sear,priest_spell_Mind_Spike,spell_mastery_priest,Spell_Mysticism,priest_spell_Resurrection

		};

		uint32 warlock_spell[49]=
		{
			warlock_spell_Corruption,warlock_spell_Life_Tap,warlock_spell_Drain_life,warlock_spell_Drain_soul,warlock_spell_Bane_Of_Agony,warlock_spell_Fear,warlock_spell_Curse_of_Weakness,warlock_spell_Bane_Of_Doom,warlock_spell_Curse_Of_Tongues,warlock_spell_Death_Coil,warlock_spell_Howl_Of_Terror,warlock_spell_Curse_of_the_Elements,warlock_spell_Seed_Of_Corruption,warlock_spell_Demon_Armor,warlock_spell_Create_Healthstone,warlock_spell_Control_Demon,warlock_spell_Soulburn,	warlock_spell_Healt_Funnel,warlock_spell_Soul_Harvest,warlock_spell_Unending_Breath,warlock_spell_Soul_Link,warlock_spell_Summon_Succubus,warlock_spell_eye_of_kilrogg,warlock_spell_enslave_demon,warlock_spell_summon_felhunter,warlock_spell_Banish,warlock_spell_Shadow_Ward,warlock_spell_Ritual_Of_summoning,warlock_spell_Summon_Infernal,warlock_spell_Summon_Doomguard,	warlock_spell_Fel_armor,warlock_spell_SoulShatter,warlock_spell_Ritual_of_souls,warlock_spell_Demonic_Circle_Summon,	warlock_spell_Demonic_Circle_Teleport,warlock_spell_Demon_Soul,warlock_spell_Immolate,	warlock_spell_Rain_of_Fire,	warlock_spell_Searing_Pain,	warlock_spell_Soul_Fire,warlock_spell_Hellfire,	warlock_spell_Incinerate,warlock_spell_ShadowFlame,	warlock_spell_Fel_Flame,Spell_netherMancy,spell_mastery_warlock,warlock_spell_Dark_Intent,warlock_spell_Summon_Voidwalker,warlock_spell_Create_Soulstone
		};

		player->PlayerTalkClass->ClearMenus();
		{
			/**DUAL SPEC*////////////////////////////////////////////////////////////
			/**/player->CastSpell(player,63680,true,NULL,NULL,player->GetGUID());///
			/**/player->CastSpell(player,63624,true,NULL,NULL,player->GetGUID());//
			/**///////////////////////////////////////////////////////////////////
			switch(action)
			{
			case GOSSIP_ACTION_INFO_DEF+1:
				//------------------------------------//
				if (player->getClass() == CLASS_WARRIOR)
				{
					if(player->HasSpell(75906))
					{
						player->CLOSE_GOSSIP_MENU();
					}
					else
					{
						for(uint32 i = 0; i < sizeof(war_spell)/sizeof(uint32); i++)
							player->learnSpell(war_spell[i],false);
						player->SaveToDB();
						// player->AddItem(51809, 4);
						ChatHandler(player->GetSession()).PSendSysMessage("Welcome home Warrior!");
					}
					player->CLOSE_GOSSIP_MENU();
				}

				//----------------------------------//
				if (player->getClass() == CLASS_MAGE)

				{
					if(player->HasSpell(75906))
					{
						player->CLOSE_GOSSIP_MENU();
					}
					else
					{
						for(uint32 i = 0; i < sizeof(mage_spell)/sizeof(uint32); i++)
							player->learnSpell(mage_spell[i],false);
						player->SaveToDB();
						// player->AddItem(51809, 4);
						ChatHandler(player->GetSession()).PSendSysMessage("Welcome home Mage!");
					}
					player->CLOSE_GOSSIP_MENU();
				}

				//----------------------------------//
				if (player->getClass() == CLASS_PALADIN)
				{
					if(player->HasSpell(75906))
					{
						player->CLOSE_GOSSIP_MENU();
					}
					else
					{
						for(uint32 i = 0; i < sizeof(pala_spell)/sizeof(uint32); i++)
							player->learnSpell(pala_spell[i],false);
						player->SaveToDB();
						// player->AddItem(51809, 4);
						ChatHandler(player->GetSession()).PSendSysMessage("Welcome home Paladin!");
					}
					player->CLOSE_GOSSIP_MENU();
				}
				//------------------------------------------//
				if (player->getClass() == CLASS_DEATH_KNIGHT)
				{
					if(player->HasSpell(75906))
					{
						player->CLOSE_GOSSIP_MENU();
					}
					else
					{
						for(uint32 i = 0; i < sizeof(dk_spell)/sizeof(uint32); i++)
							player->learnSpell(dk_spell[i],false);
						player->SaveToDB();
						// player->AddItem(51809, 4);
						ChatHandler(player->GetSession()).PSendSysMessage("Welcome home Death Knights!");
					}
					player->CLOSE_GOSSIP_MENU();
				}
				//----------------------------------//
				if (player->getClass() == CLASS_DRUID)
				{
					if(player->HasSpell(75906))
					{
						player->CLOSE_GOSSIP_MENU();
					}
					else
					{
						for(uint32 i = 0; i < sizeof(druid_spell)/sizeof(uint32); i++)
							player->learnSpell(druid_spell[i],false);
						player->SaveToDB();
						// player->AddItem(51809, 4);
						ChatHandler(player->GetSession()).PSendSysMessage("Welcome home Druid!");
					}
					player->CLOSE_GOSSIP_MENU();
				}
				//------------------------------------//
				if (player->getClass() == CLASS_SHAMAN)
				{
					if(player->HasSpell(75906))
					{
						player->CLOSE_GOSSIP_MENU();
					}
					else
					{
						for(uint32 i = 0; i < sizeof(shaman_spell)/sizeof(uint32); i++)
							player->learnSpell(shaman_spell[i],false);
						player->SaveToDB();
						// player->AddItem(51809, 4);
						ChatHandler(player->GetSession()).PSendSysMessage("Welcome home Shaman!");
					}
					player->CLOSE_GOSSIP_MENU();
				}
				//------------------------------------//
				if (player->getClass() == CLASS_HUNTER)
				{
					if(player->HasSpell(75906))
					{
						player->CLOSE_GOSSIP_MENU();
					}
					else
					{
						for(uint32 i = 0; i < sizeof(hunter_spell)/sizeof(uint32); i++)
							player->learnSpell(hunter_spell[i],false);
						player->SaveToDB();
						// player->AddItem(51809, 4);
						ChatHandler(player->GetSession()).PSendSysMessage("Welcome home Hunter!");
					}
					player->CLOSE_GOSSIP_MENU();
				}
				//------------------------------------//
				if (player->getClass() == CLASS_ROGUE)
				{
					if(player->HasSpell(75906))
					{
						player->CLOSE_GOSSIP_MENU();
					}
					else
					{
						for(uint32 i = 0; i < sizeof(rogue_spell)/sizeof(uint32); i++)
							player->learnSpell(rogue_spell[i],false);
						player->SaveToDB();
						// player->AddItem(51809, 4);
						ChatHandler(player->GetSession()).PSendSysMessage("Welcome home Rogue!");
					}
					player->CLOSE_GOSSIP_MENU();
				}
				//------------------------------------//
				if (player->getClass() == CLASS_PRIEST)
				{
					if(player->HasSpell(75906))
					{
						player->CLOSE_GOSSIP_MENU();
					}
					else
					{
						for(uint32 i = 0; i < sizeof(priest_spell)/sizeof(uint32); i++)
							player->learnSpell(priest_spell[i],false);
						player->SaveToDB();
						// player->AddItem(51809, 4);
						ChatHandler(player->GetSession()).PSendSysMessage("Welcome home Priest!");
					}
					player->CLOSE_GOSSIP_MENU();
				}
				//------------------------------------//
				if (player->getClass() == CLASS_WARLOCK)
				{
					if(player->HasSpell(75906))
					{
						player->CLOSE_GOSSIP_MENU();
					}
					else
					{
						for(uint32 i = 0; i < sizeof(warlock_spell)/sizeof(uint32); i++)
							player->learnSpell(warlock_spell[i],false);
						player->SaveToDB();
						// player->AddItem(51809, 4);
						ChatHandler(player->GetSession()).PSendSysMessage("Welcome home Warlock!");
					}
					player->CLOSE_GOSSIP_MENU();
				}


				break;

			case GOSSIP_ACTION_INFO_DEF+2:
				{
					for(uint32 i = 0; i < sizeof(mount_spells)/sizeof(uint32); i++)
						player->learnSpell(mount_spells[i],false);
						if (player->GetTeam() == HORDE)
						{	 
						player->AddItem(46749, 1);
						player->AddItem(44690, 1);						
						}
						else
						{ 
						player->AddItem(44689, 1);
						player->AddItem(45125, 1);
						}
					player->CLOSE_GOSSIP_MENU();
				}
				break;
			}
		}
		return true;
	}

};

void AddSC_npc_welcome()
{
	new npc_welcome();
}