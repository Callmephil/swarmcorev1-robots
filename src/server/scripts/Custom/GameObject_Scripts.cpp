#include "ScriptMgr.h"
#include "GameObjectAI.h"
#include "Extension.h" 


#define RANGE 3.0
//ID
#define BUFF_SPEED 23451
#define BUFF_HEAL 23493
#define BUFF_BERSERK 23505
#define BUFF_Shadow_Sight 34709
//Timer
#define TIMER_RESPAWN 40000


bool despawn = true; /*Set this to true if you want gameobject to despawn */

class gameobject_buffer : public GameObjectScript
{
    public:
        gameobject_buffer() : GameObjectScript("gameobject_buffer") { }

        struct gameobject_bufferrAI : public GameObjectAI
        {
            gameobject_bufferrAI(GameObject* gameObject) : GameObjectAI(gameObject), _checkTimer(1000){ }

			void Reset()
			{
				_checkTimer = 1000;
				respawn = false;
				cansearch = true;
				_respawnTimer = 0;
				
			}

			void SearchInRange()
			{
				Trinity::AnyPlayerInObjectRangeCheck checker(go, RANGE);
				Trinity::PlayerListSearcher<Trinity::AnyPlayerInObjectRangeCheck> searcher(go, players, checker);
				go->VisitNearbyWorldObject(RANGE, searcher);
			}

            void UpdateAI(uint32 diff)
            {
                if(cansearch)
				{
					if (_checkTimer <= diff)
					{
						SearchInRange();
						if(!players.empty())
						{
							for (std::list<Player*>::const_iterator itr = players.begin(); itr != players.end(); ++itr)
							{
								Player * plr = (*itr);
								if (plr->IsInRange(go,5.0, 5.0))
								{
									plr->CastSpell(plr, BUFF_SPEED,true);
								}
							}
							
							if(despawn)
							{
								go->SetPhaseMask(2,true);
							}
							respawn = true;
							_respawnTimer = TIMER_RESPAWN;
							cansearch = false;
							players.clear();
						}
						else 
							_checkTimer = 1000;
					}
					else
						_checkTimer -= diff;
				}

				if(respawn && despawn)
				{
					if(_respawnTimer <= diff)
					{
						go->SetPhaseMask(1, true);
						respawn = false;
						cansearch = true;
					}
					else
						_respawnTimer -= diff;
				}
            }

          public:
              uint32 _checkTimer;
			  bool respawn;
			  bool cansearch;
			  uint32 _respawnTimer;
			  std::list<Player*> players;
        };

        GameObjectAI* GetAI(GameObject* go) const
        {
            return new gameobject_bufferrAI(go);
        }
};

class gameobject_healer : public GameObjectScript
{
    public:
        gameobject_healer() : GameObjectScript("gameobject_healer") { }

        struct gameobject_healerAI : public GameObjectAI
        {
            gameobject_healerAI(GameObject* gameObject) : GameObjectAI(gameObject), _checkTimer(1000){ }

			void Reset()
			{
				_checkTimer = 1000;
				respawn = false;
				cansearch = true;
				_respawnTimer = 0;
				
			}

			void SearchInRange()
			{
				Trinity::AnyPlayerInObjectRangeCheck checker(go, RANGE);
				Trinity::PlayerListSearcher<Trinity::AnyPlayerInObjectRangeCheck> searcher(go, players, checker);
				go->VisitNearbyWorldObject(RANGE, searcher);
			}

            void UpdateAI(uint32 diff)
            {
                if(cansearch)
				{
					if (_checkTimer <= diff)
					{
						SearchInRange();
						if(!players.empty())
						{
							for (std::list<Player*>::const_iterator itr = players.begin(); itr != players.end(); ++itr)
							{
								Player * plr = (*itr);
								if (plr->IsInRange(go,5.0, 5.0))
								{
									plr->CastSpell(plr, BUFF_HEAL,true);
								}
							}
							
							if(despawn)
							{
								go->SetPhaseMask(2,true);
							}
							respawn = true;
							_respawnTimer = TIMER_RESPAWN;
							cansearch = false;
							players.clear();
						}
						else 
							_checkTimer = 1000;
					}
					else
						_checkTimer -= diff;
				}

				if(respawn && despawn)
				{
					if(_respawnTimer <= diff)
					{
						go->SetPhaseMask(1, true);
						respawn = false;
						cansearch = true;
					}
					else
						_respawnTimer -= diff;
				}
            }

          public:
              uint32 _checkTimer;
			  bool respawn;
			  bool cansearch;
			  uint32 _respawnTimer;
			  std::list<Player*> players;
        };

        GameObjectAI* GetAI(GameObject* go) const
        {
            return new gameobject_healerAI(go);
        }
};

class gameobject_berserk : public GameObjectScript
{
    public:
        gameobject_berserk() : GameObjectScript("gameobject_berserk") { }

        struct gameobject_berserkAI : public GameObjectAI
        {
            gameobject_berserkAI(GameObject* gameObject) : GameObjectAI(gameObject), _checkTimer(1000), _respawnTimer(0){ }

			void Reset()
			{
				_checkTimer = 1000;
				respawn = false;
				cansearch = true;
				_respawnTimer = 0;
				
			}

			void SearchInRange()
			{
				Trinity::AnyPlayerInObjectRangeCheck checker(go, RANGE);
				Trinity::PlayerListSearcher<Trinity::AnyPlayerInObjectRangeCheck> searcher(go, players, checker);
				go->VisitNearbyWorldObject(RANGE, searcher);
			}

            void UpdateAI(uint32 diff)
            {
                if(cansearch)
				{
					if (_checkTimer <= diff)
					{
						SearchInRange();
						if(!players.empty())
						{
							for (std::list<Player*>::const_iterator itr = players.begin(); itr != players.end(); ++itr)
							{
								Player * plr = (*itr);
								if (plr->IsInRange(go,5.0, 5.0))
								{
									plr->CastSpell(plr, BUFF_BERSERK,true);
								}
							}
							
							if(despawn)
							{
								go->SetPhaseMask(2,true);
							}
							respawn = true;
							_respawnTimer = TIMER_RESPAWN;
							cansearch = false;
							players.clear();
						}
						else 
							_checkTimer = 1000;
					}
					else
						_checkTimer -= diff;
				}

				if(respawn && despawn)
				{
					if(_respawnTimer <= diff)
					{
						go->SetPhaseMask(1, true);
						respawn = false;
						cansearch = true;
					}
					else
						_respawnTimer -= diff;
				}
            }

          public:
              uint32 _checkTimer;
			  bool respawn;
			  bool cansearch;
			  uint32 _respawnTimer;
			  std::list<Player*> players;
        };

        GameObjectAI* GetAI(GameObject* go) const
        {
            return new gameobject_berserkAI(go);
        }
};


class gameobject_Shadow_Sight : public GameObjectScript
{
    public:
        gameobject_Shadow_Sight() : GameObjectScript("gameobject_Shadow_Sight") { }

        struct gameobject_Shadow_SightAI : public GameObjectAI
        {
            gameobject_Shadow_SightAI(GameObject* gameObject) : GameObjectAI(gameObject), _checkTimer(1000), _respawnTimer(0){ }

			void Reset()
			{
				_checkTimer = 1000;
				respawn = false;
				cansearch = true;
				_respawnTimer = 0;
				
			}

			void SearchInRange()
			{
				Trinity::AnyPlayerInObjectRangeCheck checker(go, RANGE);
				Trinity::PlayerListSearcher<Trinity::AnyPlayerInObjectRangeCheck> searcher(go, players, checker);
				go->VisitNearbyWorldObject(RANGE, searcher);
			}

            void UpdateAI(uint32 diff)
            {
                if(cansearch)
				{
					if (_checkTimer <= diff)
					{
						SearchInRange();
						if(!players.empty())
						{
							for (std::list<Player*>::const_iterator itr = players.begin(); itr != players.end(); ++itr)
							{
								Player * plr = (*itr);
								if (plr->IsInRange(go,5.0, 5.0))
								{
									plr->CastSpell(plr, BUFF_Shadow_Sight,true);
								}
							}
							
							if(despawn)
							{
								go->SetPhaseMask(2,true);
							}
							respawn = true;
							_respawnTimer = TIMER_RESPAWN;
							cansearch = false;
							players.clear();
						}
						else 
							_checkTimer = 1000;
					}
					else
						_checkTimer -= diff;
				}

				if(respawn && despawn)
				{
					if(_respawnTimer <= diff)
					{
						go->SetPhaseMask(1, true);
						respawn = false;
						cansearch = true;
					}
					else
						_respawnTimer -= diff;
				}
            }

          public:
              uint32 _checkTimer;
			  bool respawn;
			  bool cansearch;
			  uint32 _respawnTimer;
			  std::list<Player*> players;
        };

        GameObjectAI* GetAI(GameObject* go) const
        {
            return new gameobject_Shadow_SightAI(go);
        }
};

void AddSC_Buffer()
{
	new gameobject_buffer();
	new gameobject_healer();
	new gameobject_berserk();
	new gameobject_Shadow_Sight();
}