/*#include "gamePCH.h"
*/
#include "Extension.h"
#include "Chat.h"
#include "Player.h"

using namespace std;  

//Je ferait un .H avec toutes les palettes de couleurs pour notre plus grand plaisir !
#define TEXT_CUSTOM_ORANGE                      "|cffFF6600"
#define TEXT_CUSTOM_RED                         "|cffFF0000"
#define TEXT_CUSTOM_BLUE                        "|cff00479E"
#define TEXT_CUSTOM_GREEN                       "|cff99FF00"
#define TEXT_CUSTOM_PURPLE                      "|cff5A005B"
#define TEXT_CUSTOM_GREY                        "|cff515151"
#define TEXT_CUSTOM_CYAN                        "|cff33CCFF"
#define TEXT_CUSTOM_BROWN                       "|cff584200"
#define TEXT_CUSTOM_WHITE                       "|cffFFFFFF"

#define W_MSG_SWARM "Welcome in the Swarm"
#define W_MSG_MOONLIGHT "Moonlight is a PvP International Server (4.3.4a)"
#define join_the_com "You can join the community with the command |cffFFFFFF /join world"
#define vote_for_us "Vote For Us ! Check our website www.Swarm-Servers.com"


class learn_on_login : public PlayerScript
{
public:
	learn_on_login() : PlayerScript("learn_on_login") { }

	void OnLogin(Player* player)
	{

		//Chat MSG
		ChatHandler(player->GetSession()).PSendSysMessage(""TEXT_CUSTOM_WHITE"" "//=============="W_MSG_SWARM"==============\\");
		ChatHandler(player->GetSession()).PSendSysMessage(""TEXT_CUSTOM_GREEN"" W_MSG_MOONLIGHT);
		ChatHandler(player->GetSession()).PSendSysMessage(""TEXT_CUSTOM_CYAN"" join_the_com);
		ChatHandler(player->GetSession()).PSendSysMessage(""TEXT_CUSTOM_ORANGE"" vote_for_us);
		//Screen MSG
		player->GetSession()->SendNotification(""TEXT_CUSTOM_WHITE"" "//==========="W_MSG_SWARM"===========\\");
		player->GetSession()->SendNotification(""TEXT_CUSTOM_CYAN"" join_the_com);
		player->GetSession()->SendNotification(""TEXT_CUSTOM_ORANGE"" vote_for_us);
	}
};

void AddSC_learn_on_login()
{
	new learn_on_login();
}