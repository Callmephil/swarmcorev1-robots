#include "Kazooie.h"
#include "Extension.h"

#define currency_token 241

void RevivePlayer(Player* player)
	{
	player->SetHealth(player->GetMaxHealth());
	if(player->getPowerType() == POWER_MANA)
	player->SetPower(POWER_MANA, player->GetMaxPower(POWER_MANA));
	if(player->getPowerType() == POWER_ENERGY)
	player->SetPower(POWER_ENERGY, player->GetMaxPower(POWER_ENERGY));
	}

class Reset_OnDuelEnd : public PlayerScript
{
public:
	Reset_OnDuelEnd() : PlayerScript("Reset_OnDuelEnd") {}
	void OnDuelEnd(Player *player, Player * plTarget,  DuelCompleteType type)
	{
		
	
		if (player->GetZoneId() == 139 && plTarget->GetZoneId() == 139/* && type == DUEL_WON*/) //Interdit en zone ffa
		{
			return;
		}
		else
		{
		
			player->RemoveAllSpellCooldown();
			plTarget->RemoveAllSpellCooldown();
		
			RevivePlayer(player);
			RevivePlayer(plTarget);
			
			player->CombatStop();
			plTarget->CombatStop();
			
			
            player->InterruptSpell(CURRENT_AUTOREPEAT_SPELL); // break Auto Shot and autohit
            plTarget->InterruptSpell(CURRENT_CHANNELED_SPELL);  // break channeled spells
			
			player->AttackStop();
			plTarget->AttackStop();
		}
	}
};

class Reset_OnDuelStart : public PlayerScript
{
public:
	Reset_OnDuelStart() : PlayerScript("OnDuelStart") {}

	void OnDuelStart(Player *player, Player *plTarget)
	{
		
		RevivePlayer(player);
		RevivePlayer(plTarget);
		player->SetPower(POWER_RAGE, 0);
		plTarget->SetPower(POWER_RAGE, 0);
		player->SetPower(POWER_RUNIC_POWER, 0);
		plTarget->SetPower(POWER_RUNIC_POWER, 0);
	}	
};

class Duel_Token_Reward : public PlayerScript
{
public:
	Duel_Token_Reward() : PlayerScript("Duel_Token_Reward") { }
	
	/*	void Reset()
		{
			DuelTimer = 5000;
		};*/

	void OnDuelEnd(Player *winner, Player *looser, DuelCompleteType type)
	{
		uint64 winnerGUID = winner->GetGUID();
		uint64 looserGUID = looser->GetGUID();

		

		struct Duel
		{
			uint32 DUEL_WON;
			uint64 lastkill;
		};

		static std::map<uint64, Duel> Duel_Data;

		if( winnerGUID == looserGUID || Duel_Data[winnerGUID].lastkill == looserGUID )
			return;

		Duel_Data[winnerGUID].DUEL_WON++;
		Duel_Data[winnerGUID].lastkill = looserGUID;
		Duel_Data[looserGUID].DUEL_WON = 0;
		Duel_Data[looserGUID].lastkill = 2;
		/*{
		sLog->outError(LOG_FILTER_WARDEN, "[Anti-Triche]Triche possible en zone duel, avec le joueur (il a fait plusieur match gagne contre): %u",winner->GetGUID());
		}*/
	//	if(DuelTimer() 0);
	//	{
			if (winner->GetZoneId() == 1 && looser->GetZoneId() == 1 && type == DUEL_WON)
			{
				if (winner->getLevel() >= 1 && looser->getLevel() <= 18 && type == DUEL_WON)
				{
					return;
				}
				if (winner->getLevel() == 80 && looser->getLevel() == 19 && type == DUEL_WON)
				{
					return;
				}
				if (winner->getLevel() == 19 && looser->getLevel() == 80 && type == DUEL_WON)
				{
					return;
				}
				if (looser->GetSession()->GetLatency() > 350 && type == DUEL_WON)
				{
					ChatHandler(winner->GetSession()).PSendSysMessage("|cFFFFFC00[System]|cFF00FFFF  !! ALERT !! One of you has a latency superior to 500");
					ChatHandler(looser->GetSession()).PSendSysMessage("|cFFFFFC00[System]|cFF00FFFF  !! ALERT !! One of you has a latency superior to 500");
					return;
				}
				if (looser->GetHealth() > 10 && type == DUEL_WON)
				{
				ChatHandler(looser->GetSession()).PSendSysMessage("|cFFFFFC00[System]|cFF00FFFF No one killed the other !");
					return;
				}
				if (looser->GetSession()->GetSecurity() >= 2/* && winner->GetSession()->GetSecurity() < 1 */&& type == DUEL_WON)
				{
					ChatHandler(winner->GetSession()).PSendSysMessage("|cFFFFFC00[System]|cFF00FFFF You Can't Claim Reward When you're vs a GameMaster !");
					ChatHandler(looser->GetSession()).PSendSysMessage("|cFFFFFC00[System]|cFF00FFFF You Can't Claim Reward When you're vs a GameMaster !" );
					return;
				}
				if (winner->GetSession()->GetSecurity() >= 2/* && looser->GetSession()->GetSecurity() < 1 */&& type == DUEL_WON)
				{
					ChatHandler(winner->GetSession()).PSendSysMessage("|cFFFFFC00[System]|cFF00FFFF You Can't Claim Reward When you're vs a GameMaster !");
					ChatHandler(looser->GetSession()).PSendSysMessage("|cFFFFFC00[System]|cFF00FFFF You Can't Claim Reward When you're vs a GameMaster !");
					return;
				}
				
					if (winner->GetSession()->GetRemoteAddress() == looser->GetSession()->GetRemoteAddress())
				{
				/*	sLog->outError(LOG_FILTER_WARDEN, "[Systeme Anti-Triche]Triche possible en zone duel, avec le Guid joueur : %u",winner->GetGUID()); 
					sLog->outError(LOG_FILTER_WARDEN, "et l'ID de compte : %u",winner->GetSession()->GetAccountId());*/
					ChatHandler(winner->GetSession()).SendGlobalGMSysMessage("|cFFFFFC00[Cheating System]|cFF00FFFF A player is trying to cheat in duel Area !");
					ChatHandler(winner->GetSession()).PSendSysMessage("|cFFFFFC00[Cheating System]|cFF00FFFF you can't claim reaward vs a same ip !");
					ChatHandler(looser->GetSession()).PSendSysMessage("|cFFFFFC00[Cheating System]|cFF00FFFF you can't claim reaward vs a same ip !");
				}
				else 
				{
				//Simple Reward
					winner->ModifyCurrency(currency_token, 3 , true,true, false);
					winner->ModifyCurrency(currency_token, 1 , true,true, false);
					winner->SaveToDB();
					looser->SaveToDB();
				//Cote Duel
				/*
				if(DuelStat* stat_win=winner->getExtension()->Kazooie_getDuelStat())
					if(DuelStat* stat_loose=looser->getExtension()->Kazooie_getDuelStat()) 
				{
						stat_win->modCote(true,stat_loose);
						stat_loose->modCote(false,stat_win);
				}
				*/
				}
			}
		//}
		
		//}
	}
};


void AddSC_Reset()
{
	new Duel_Token_Reward;
	new Reset_OnDuelEnd;
	new	Reset_OnDuelStart;
}