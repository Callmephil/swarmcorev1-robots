#include "Extension.h"
#define mascotte1 "Teleportation in Shop Zone"
#define mascotte2 "Enroll in Battleground"
#define mascotte3 "Compete in FFA Zone"
#define mascotte4 "Compete in Duel Zone"
#define mascotte5 "Display BG score"
#define mascotte6 "Display Arena score"
#define mascotte7 "Display FFA score"
#define mascotte8 "Display Duel score"
#define mascotte9 "Maximum skills"
#define mascotte10 "Repair"

class Mascotte : public CreatureScript
{
public:

	Mascotte()
		: CreatureScript("Mascotte")
	{
	}
	bool OnGossipHello(Player* pPlayer, Creature* pCreature)
	{
		if(TempSummon* moi=pCreature->ToTempSummon()) {
			if(Unit* owner=moi->GetOwner()) {
				if(owner!=pPlayer) {
					return true;
				}
			}
			else return true;
		}
		else return true;
		pPlayer->ADD_GOSSIP_ITEM(2, mascotte1, GOSSIP_SENDER_MAIN, 1001);
		pPlayer->ADD_GOSSIP_ITEM(5, mascotte3, GOSSIP_SENDER_MAIN, 1003);
		pPlayer->ADD_GOSSIP_ITEM(5, mascotte4, GOSSIP_SENDER_MAIN, 1004);
		pPlayer->ADD_GOSSIP_ITEM(3, mascotte5, GOSSIP_SENDER_MAIN, 1005);
		pPlayer->ADD_GOSSIP_ITEM(3, mascotte6, GOSSIP_SENDER_MAIN, 1006);
		pPlayer->ADD_GOSSIP_ITEM(3, mascotte7, GOSSIP_SENDER_MAIN, 1007);
		pPlayer->ADD_GOSSIP_ITEM(3, mascotte8, GOSSIP_SENDER_MAIN, 1008);
		pPlayer->ADD_GOSSIP_ITEM(4, mascotte9, GOSSIP_SENDER_MAIN, 1009);
		pPlayer->ADD_GOSSIP_ITEM(4, mascotte10, GOSSIP_SENDER_MAIN, 1010);

		pPlayer->PrepareQuestMenu(pCreature->GetGUID());

		pPlayer->PlayerTalkClass->SendGossipMenu(80000, pCreature->GetGUID());
		return true;
	}

	void MascotteSendDefault(Player* pPlayer, Creature* pCreature, uint32 uiAction)
	{
		switch(uiAction) {
		case 1001: 
			if(!pPlayer->InBattleground()) 
				if(!pPlayer->InArena())
					if(!pPlayer->getExtension()->Kazooie_IsInFFAZone()) 
						if (!pPlayer->IsInCombat()) {
							pPlayer->TeleportTo(189,1212.12f,1397.1f,29.02f,3.02f);
						}
						break;
		case 1003:
			pPlayer->TeleportTo(189, 168.1f,-270.66f,18.76f,0.88f);
			break;
		case 1005: //pPlayer->getExtension()->Kazooie_SendBgMsg();
			break;
		case 1006: //pPlayer->getExtension()->Kazooie_SendArenaMsg();
			break;
		case 1007: //pPlayer->getExtension()->Kazooie_SendFFAMsg();
			break;
		case 1008: //pPlayer->getExtension()->Kazooie_SendDuelMsg();
			break;
		case 1009:
			pPlayer->UpdateSkillsToMaxSkillsForLevel();
			pPlayer->CastSpell(pPlayer,36937,true);
			break;
		case 1010:
			// Repair items
			pPlayer->DurabilityRepairAll(false, 0, false);
			pPlayer->CastSpell(pPlayer,36937,true);
			break;
		}
		pPlayer->CLOSE_GOSSIP_MENU();
	}

	bool OnGossipSelect(Player* pPlayer, Creature* pCreature, uint32 uiSender, uint32 uiAction)
	{
		if(TempSummon* moi=pCreature->ToTempSummon()) {
			if(Unit* owner=moi->GetOwner()) {
				if(owner!=pPlayer) 
					return true;
			}
			else return true;
		}
		else return true;
		// Not allow in combat
		if (pPlayer->IsInCombat())
		{
			pCreature->MonsterSay("You are in combat !", LANG_UNIVERSAL, pPlayer->GetGUID());
			return true;
		}
		pPlayer->PlayerTalkClass->ClearMenus();
		// Main menu
		if (uiSender == GOSSIP_SENDER_MAIN)
			MascotteSendDefault(pPlayer, pCreature, uiAction);
		return true;
	}
};
void AddSC_Mascotte()
{
	new Mascotte();
}